#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class CubeEnviroment
{
	public:
		unsigned int id_cubemap;
		unsigned int fbo_positive_x;
		unsigned int fbo_negative_x;
		unsigned int fbo_positive_y;
		unsigned int fbo_negative_y;
		unsigned int fbo_positive_z;
		unsigned int fbo_negative_z;
		unsigned int size;
		glm::vec3 pos;
		CubeEnviroment(unsigned int cubemap_size, glm::vec3 pos);
};

