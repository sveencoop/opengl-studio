#pragma once
#include <glad\glad.h>
#include <vector>
#include "stb_image.h"

class TextureImage;

//TODAS AS FUN��ES ASSOCIADAS A TEXTURA
class texture {
	public:
		static void init();

		static std::vector<unsigned int> texturesCompiled;
		static std::vector<TextureImage*> textureImagesCompiled;
		static void getError(const char* path);

		static unsigned int createFrameBufferTexture(unsigned int width, unsigned height);
		//Juntar os dois abaixo
		static unsigned int *createFrameBufferTexture2(unsigned int width, unsigned height);
		static unsigned int *createFrameBufferTextureDeferred(unsigned int width, unsigned int height, unsigned int fbo);

		static unsigned int createFrameBuffer();
		static unsigned int createDepthMapTexture(unsigned int width, unsigned int height);
		
		//Juntar os dois abaixo
		static void bindFrameBufferColorMap2(unsigned int fbo, unsigned int texture0, unsigned int texture1);
		static void bindFrameBufferColorMap(unsigned int fbo, unsigned int texture);
		static void bindFrameBufferDepthMap(unsigned int fbo, unsigned int texture);

		static unsigned int initCompileCubeMap();
		static void addFaceCubeMap(const char*path, unsigned int CUBE_MAP_DIR);
		static void allocFaceCubeMap(unsigned int CUBE_MAP_DIR, unsigned int cubeMapSize);
		static unsigned int attachFaceCubeMap(unsigned int cubemap, unsigned int CUBE_MAP_DIR, unsigned int cubemap_size);

		static unsigned char *load_image(const char* path);
		static unsigned int compile(const char* path, unsigned int clamp = GL_CLAMP_TO_EDGE);

		static TextureImage* createImage(const char *path);

		static void freeTexturesCompiled();
};

class TextureImage {
private:
	int nrChannels;
	int width;
	int height;
	unsigned char *dataImg;
	const char* path;
public:
	unsigned int id;
	TextureImage();
	~TextureImage();
	const char *getPath();
	void load(const char *path);
	unsigned int flushToGL(unsigned int clamp);
	void flushToGLCubeMap(unsigned int CUBE_MAP_DIR);
	void free();
};