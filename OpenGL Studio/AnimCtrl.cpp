#include "AnimCtrl.h"

AnimCtrl::AnimCtrl(void(*function)(double), bool repeat, double mod) {
	this->function = function;
	this->repeat = repeat;
	this->mod = mod;
	this->lockedTime = mod;
}

void AnimCtrl::setLockedTime(double max) {
	this->lockedTime = max;
}

void AnimCtrl::setMod(double mod) {
	this->mod = mod;
}

void AnimCtrl::start(double initial) {
	this->initial = initial;
	this->running = true;
	this->locked = true;
}

void AnimCtrl::trigger(double time) {
	if (this->running) {
		double value = time - this->initial;
		if (this->locked) {
			if (value >= this->lockedTime) {
				this->locked = false;
			}
		}
		if (value >= this->mod) {
			if (this->repeat) {
				this->initial = this->initial + mod;
				value = time - this->initial;
			} else {
				this->end();
			}
		}
		this->function(value);
	}
}

bool AnimCtrl::isLocked() {
	return this->locked;
}

bool AnimCtrl::isRunning() {
	return this->running;
}

void AnimCtrl::end() {
	this->running = false;
}

AnimCtrl::~AnimCtrl() {
}
