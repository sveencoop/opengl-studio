#pragma once


class Sun {
private:
	unsigned int attr_ambient;
	unsigned int attr_diffuse;
	unsigned int attr_direction;
public:
	void init(); 
	void apply();
	void setPosition(float x, float y, float z);
	void shaderingData(unsigned int shader);
	float* diffuse;
	float* ambient;
	float* direction;
};

