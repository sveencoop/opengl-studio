#include "TextureControl.h"
#include "StringBuilder.h"
#include "InterfaceIO.h"

using namespace GlobalInterfaceIO;

std::vector<unsigned int> texture::texturesCompiled;

//PARA TEXTURAS DO FRAMEBUFFERS
unsigned int texture::createFrameBufferTexture(unsigned int width, unsigned height) {

	unsigned int texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	texture::texturesCompiled.push_back(texture_id);

	return texture_id;
}

//juntar com o debaixo
unsigned int *texture::createFrameBufferTexture2(unsigned int width, unsigned height) {
	unsigned int *texture_id = (unsigned int*)malloc(sizeof(unsigned int)*2);
	glGenTextures(2, texture_id);
	for (int i = 0; i < 2; i++) {
		glBindTexture(GL_TEXTURE_2D, texture_id[i]);

		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		texture::texturesCompiled.push_back(texture_id[i]);
	}
	return texture_id;
}

unsigned int *texture::createFrameBufferTextureDeferred(unsigned int width, unsigned int height, unsigned int fbo) {
	unsigned int *texture_id = (unsigned int*)malloc(sizeof(unsigned int) * 3);
	unsigned int gPosition, gNormal, gColorSpec;

	glGenTextures(1, &gPosition);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);

	// - normal color buffer
	glGenTextures(1, &gNormal);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);

	// - color + specular color buffer
	glGenTextures(1, &gColorSpec);
	glBindTexture(GL_TEXTURE_2D, gColorSpec);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gColorSpec, 0);

	unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	//Diz ao Opengl que o FBO possui os seguintes attachments para desenhar cores
	glDrawBuffers(3, attachments);

	texture_id[0] = gPosition;
	texture_id[1] = gNormal;
	texture_id[2] = gColorSpec;
	
	texture::texturesCompiled.push_back(gPosition);
	texture::texturesCompiled.push_back(gNormal);
	texture::texturesCompiled.push_back(gColorSpec);

	return texture_id;
}

unsigned int texture::createFrameBuffer() {
	
	unsigned int fbo;
	glGenFramebuffers(1, &fbo);
	
	return fbo;
}

unsigned int texture::createDepthMapTexture(unsigned int width, unsigned int height) {

	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor); 
	texture::texturesCompiled.push_back(depthMap);
	return depthMap;
}


void texture::bindFrameBufferColorMap2(unsigned int fbo, unsigned int texture0, unsigned int texture1) {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture0, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texture1, 0);
}

void texture::bindFrameBufferColorMap(unsigned int fbo, unsigned int texture) {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0); //DEFAULT
}

void texture::bindFrameBufferDepthMap(unsigned int fbo, unsigned int texture) {

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture, 0);
	glDrawBuffer(GL_NONE); //sem cor
	glReadBuffer(GL_NONE); //sem cor

	glBindFramebuffer(GL_FRAMEBUFFER, 0); //DEFAULT
}

//PARA CUBEMAPS e SKYBOXES
unsigned int texture::initCompileCubeMap() {
	unsigned int texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	texture::texturesCompiled.push_back(texture_id);

	return texture_id;
}

void texture::allocFaceCubeMap(unsigned int CUBE_MAP_DIR, unsigned int size) {
	glTexImage2D(CUBE_MAP_DIR, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
}

unsigned int texture::attachFaceCubeMap(unsigned int cubemap, unsigned int CUBE_MAP_DIR, unsigned int cubemap_size) {
	unsigned int fbo, rbo;
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, cubemap_size, cubemap_size);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, CUBE_MAP_DIR, cubemap, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, CUBE_MAP_DIR, cubemap, 0);

	return fbo;
}

void texture::addFaceCubeMap(const char * path, unsigned int CUBE_MAP_DIR) {
	texture::getError(path);
	int width, height, nrChannels;

	unsigned char *dataImg = NULL;

	dataImg = stbi_load(path, &width, &height, &nrChannels, 0);

	if (dataImg != NULL) {
		glTexImage2D(CUBE_MAP_DIR, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, dataImg);
	}
	else {
		log(StringBuilder::concat("STBI_LOAD::FAILED_TO_LOAD_IMAGE::", path));
	}

	stbi_image_free(dataImg);
}

void texture::init() {
	stbi_set_flip_vertically_on_load(true);
}

//PARA RECUPERAR PROBLEMAS AP�S gera��o da textura
void texture::getError(const char* path) {
	switch (glGetError()) {
	case GL_NO_ERROR:
	case GL_INVALID_ENUM:
		break;
	case GL_INVALID_VALUE:
		log("A numeric argument is out of range.The offending command is ignored and has no other side effect than to set the error flag.");
		log(path);
		break;
	case GL_INVALID_OPERATION:
		log("The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.");
		log(path);
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		log("The command is trying to render to or read from the framebuffer while the currently bound framebuffer is not framebuffer complete(i.e.the return value from");
		log("glCheckFramebufferStatus is not GL_FRAMEBUFFER_COMPLETE). The offending command is ignored and has no other side effect than to set the error flag. ");
		log(path);
		break;
	case GL_OUT_OF_MEMORY:
		log("There is not enough memory left to execute the command.The state of the GL is undefined, except for the state of the error flags, after this error is recorded.");
		log(path);
		break;
	default:
		log("Erro desconhecido ao criar textura ");
		log(path);
	}

}

unsigned char *texture::load_image(const char * path) {
	int width, height, nrChannels;
	unsigned char *dataImg = stbi_load(path, &width, &height, &nrChannels, 0);
	GLenum format;
	if (nrChannels == 1)
		format = GL_RED;
	else if (nrChannels == 3)
		format = GL_RGB;
	else if (nrChannels == 4)
		format = GL_RGBA;

	return dataImg;
}

//CRIA TEXTURA BASICA PARA USO GERAL
unsigned int texture::compile(const char* path, unsigned int clamp) {

	unsigned int texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	texture::getError(path);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);

	if (clamp == GL_CLAMP_TO_BORDER) {
		float borderColor[] = { 1.0f, 0.0f, 1.0f, 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	}

	int width, height, nrChannels;

	unsigned char *dataImg = stbi_load(path, &width, &height, &nrChannels, 0);
	GLenum format;
	if (nrChannels == 1)
		format = GL_RED;
	else if (nrChannels == 3)
		format = GL_RGB;
	else if (nrChannels == 4)
		format = GL_RGBA;


	if (dataImg) {
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, dataImg);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		log(StringBuilder::concat("STBI_LOAD::FAILED_TO_LOAD_IMAGE::", path));
	}

	stbi_image_free(dataImg);

	texture::texturesCompiled.push_back(texture_id);

	return texture_id;
}

TextureImage *texture::createImage(const char * path) {
	TextureImage *image = new TextureImage();
	image->load(path);
	return image;
}

void texture::freeTexturesCompiled() {

	for (unsigned int texture_id : texture::texturesCompiled) {
		glDeleteTextures(1, &texture_id);
	}

	texture::texturesCompiled.clear();
}

TextureImage::TextureImage() {
	this->dataImg = nullptr;
}

TextureImage::~TextureImage() {
	this->free();
}

const char * TextureImage::getPath() {
	return this->path;
}

void TextureImage::load(const char * pathImg) {
	int width, height, nrChannels;

	unsigned char *data = stbi_load(pathImg, &width, &height, &nrChannels, 0);

	this->width = width;
	this->height = height;
	this->dataImg = data;
	this->nrChannels = nrChannels;
	this->path = pathImg;

	if (this->width % 4 != 0 || this->height % 4 != 0) {
		log(std::string(std::string(this->path) + std::string("' possui dimens�es n�o diviseis por 4, o que pode gerar erros")).c_str());
	}
}

unsigned int TextureImage::flushToGL(unsigned int clamp) {

	unsigned int texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	texture::getError(this->path);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);

	if (clamp == GL_CLAMP_TO_BORDER) {
		float borderColor[] = { 1.0f, 0.0f, 1.0f, 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	}

	GLenum format;
	if (nrChannels == 1) {
		format = GL_RED;
	} else if (nrChannels == 3) {
		format = GL_RGB;
	} else if (nrChannels == 4) {
		format = GL_RGBA;
	}

	if (this->dataImg) {
		glTexImage2D(GL_TEXTURE_2D, 0, format, this->width, this->height, 0, format, GL_UNSIGNED_BYTE, this->dataImg);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		log(StringBuilder::concat("STBI_LOAD::FAILED_TO_LOAD_IMAGE::", path));
	}

	this->id = texture_id;
	texture::texturesCompiled.push_back(texture_id);
	return texture_id;
}

void TextureImage::flushToGLCubeMap(unsigned int CUBE_MAP_DIR) {
	texture::getError(path);

	if (dataImg != NULL) {
		glTexImage2D(CUBE_MAP_DIR, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, dataImg);
		stbi_image_free(dataImg);
	} else {
		log(StringBuilder::concat("STBI_LOAD::FAILED_TO_LOAD_IMAGE::", path));
	}

}

void TextureImage::free() {
	if (this->dataImg) {
		stbi_image_free(dataImg);
	}
}
