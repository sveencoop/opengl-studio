#include "Camera.h"

static double max_angle = glm::radians(89.0);
static double min_angle = -glm::radians(89.0);

void FPSWalk::accPitch(double acc) {
	this->pitch += acc;

	if (this->pitch > max_angle) {
		this->pitch = max_angle;
	} else if (this->pitch < min_angle) {
		this->pitch = min_angle;
	}
}

void FPSWalk::accYaw(double acc) {
	this->yaw += acc;
}

void FPSWalk::foward(double speed) {
	glm::vec3 normal = glm::normalize(glm::vec3(this->camFront.x, 0.0, this->camFront.z));
	this->posCam.x += speed * normal.x;
	this->posCam.z += speed * normal.z;
}

void FPSWalk::backward(double speed) {
	glm::vec3 normal = glm::normalize(glm::vec3(this->camFront.x, 0.0, this->camFront.z));
	this->posCam.x -= speed * normal.x;
	this->posCam.z -= speed * normal.z;
}

void FPSWalk::left(double speed) {
	glm::vec3 normal = glm::normalize(glm::vec3(this->camRight.x, 0.0, this->camRight.z));
	this->posCam.x -= speed * normal.x;
	this->posCam.z -= speed * normal.z;
}

void FPSWalk::right(double speed) {
	glm::vec3 normal = glm::normalize(glm::vec3(this->camRight.x, 0.0, this->camRight.z));
	this->posCam.x += speed * normal.x;
	this->posCam.z += speed * normal.z;
}

void FPSWalk::changeY(float y) {
	this->posCam.y = y;
}

Modulo_Key FPSWalk::getMode() {
	return normal_mode;
}

void FPSNoclipWalk::accPitch(double acc) {
	this->pitch += acc;

	if (this->pitch > max_angle) {
		this->pitch = max_angle;
	} else if (this->pitch < min_angle) {
		this->pitch = min_angle;
	}
}

void FPSNoclipWalk::accYaw(double acc) {
	this->yaw += acc;
}

void FPSNoclipWalk::foward(double speed) {
	this->posCam.x += speed * this->camFront.x;
	this->posCam.y += speed * this->camFront.y;
	this->posCam.z += speed * this->camFront.z;
}

void FPSNoclipWalk::backward(double speed) {
	this->posCam.x -= speed * this->camFront.x;
	this->posCam.y -= speed * this->camFront.y;
	this->posCam.z -= speed * this->camFront.z;
}

void FPSNoclipWalk::left(double speed) {
	this->posCam.x -= speed * this->camRight.x;
	this->posCam.y -= speed * this->camRight.y;
	this->posCam.z -= speed * this->camRight.z;
}

void FPSNoclipWalk::right(double speed) {
	this->posCam.x += speed * this->camRight.x;
	this->posCam.y += speed * this->camRight.y;
	this->posCam.z += speed * this->camRight.z;
}

void FPSNoclipWalk::changeY(float y) {
}

Modulo_Key FPSNoclipWalk::getMode() {
	return fly_mode;
}

void NoWalk::accPitch(double acc) {}
void NoWalk::accYaw(double acc) {}
void NoWalk::foward(double speed) {}
void NoWalk::backward(double speed) {}
void NoWalk::left(double speed) {}
void NoWalk::right(double speed) {}
void NoWalk::changeY(float y) {}

Modulo_Key NoWalk::getMode() {
	return deg90;
}

void Camera::init(float x, float y, float z, float angle, float min, float max) {
	this->yaw = M_PI_2;// face to GL_TEXTURE_CUBE_MAP_NEGATIVE_X
	this->pitch = 0.0f;
	this->camFront = glm::vec3(0.0f, 0.0f, 1.0f);
	this->camUp = glm::vec3(0.0f, 1.0f, 0.0f);
	this->posCam = glm::vec3(x, y, z);
	this->tarCam = glm::vec3(0.0f, 0.5f, 0.0f);
	this->accCamY = 0;
	this->min = min;
	this->max = max;
	this->angle = angle;
	this->projection = glm::perspective(glm::radians(angle), this->w_ratio, min, max);
	GlobalInterfaceIO::log("Alocando camera");
}

void Camera::calcFinalCam() {
	//glm::vec3 acc;
	//acc = glm::vec3(0.0f, 0.1f, 0.0f)*this->accCamY;
	this->view = glm::lookAt(this->posCam, this->posCam + this->camFront/* + acc*/, this->camUp); //firstperson
}

void Camera::compile() {
	this->camFront.x = cos(this->pitch) * cos(this->yaw);
	this->camFront.y = sin(this->pitch);
	this->camFront.z = cos(this->pitch) * sin(this->yaw);
	this->camRight = glm::cross(this->camFront, this->camUp); //se der merda use glm::normalize
}

void Camera::setOrientation(unsigned int DIR) {
	switch (DIR) {
	case GL_TEXTURE_CUBE_MAP_POSITIVE_X:
		this->camFront = glm::vec3(1.0f, 0.0f, 0.0f);
		this->camUp = glm::vec3(0.0f, -1.0f, 0.0f);
		break;
	case GL_TEXTURE_CUBE_MAP_NEGATIVE_X:
		this->camFront = glm::vec3(-1.0f, 0.0f, 0.0f);
		this->camUp = glm::vec3(0.0f, -1.0f, 0.0f);
		break;
	case GL_TEXTURE_CUBE_MAP_POSITIVE_Y:
		this->camFront = glm::vec3(0.0f, 1.0f, 0.0f);
		this->camUp = glm::vec3(0.0f, 0.0f, 1.0f);
		break;
	case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y:
		this->camFront = glm::vec3(0.0f, -1.0f, 0.0f);
		this->camUp = glm::vec3(0.0f, 0.0f, -1.0f);
		break;
	case GL_TEXTURE_CUBE_MAP_POSITIVE_Z:
		this->camFront = glm::vec3(0.0f, 0.0f, 1.0f);
		this->camUp = glm::vec3(0.0f, -1.0f, 0.0f);
		break;
	case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z:
		this->camFront = glm::vec3(0.0f, 0.0f, -1.0f);
		this->camUp = glm::vec3(0.0f, -1.0f, 0.0f);
		break;
	}

}

void Camera::changePosCam(float x, float y, float z) {
	this->posCam = glm::vec3(x, y, z);
}

void Camera::changeTargetCam(float x, float y, float z) {
	this->tarCam = glm::vec3(x, y, z);
	this->compile();
}

void Camera::setWindow(int SCR_WIDTH, int SCR_HEIGHT) {
	this->w_width = SCR_WIDTH;
	this->w_height = SCR_HEIGHT;
	this->w_ratio = ((float)this->w_width) / ((float)this->w_height);
}

void Camera::setAngle(float angle) {
	if (angle != this->angle) {
		this->projection = glm::perspective(glm::radians(angle), this->w_ratio, this->min, this->max);
		this->angle = angle;
	}
}

glm::vec3 Camera::getPosition() {
	return this->posCam;
}

glm::vec3 Camera::getFront() {
	return this->camFront;
}

glm::mat4 Camera::getView() {
	return this->view;
}

glm::mat4 Camera::getProjection() {
	return this->projection;
}

float Camera::getYaw() {
	return this->yaw;
}

float Camera::getPitch() {
	return this->pitch;
}

void Camera::setFront(glm::vec3 front) {
	this->camFront = front;
}

void Camera::setYaw(float newyaw) {
	this->yaw = newyaw;
}

void Camera::setPitch(float newpitch) {
	this->pitch = newpitch;
}


