#include "Pointlight.h"
#include "StringBuilder.h"
#include <glad\glad.h>
#include <string>

Pointlight::Pointlight() {
	this->position = (float*)malloc(sizeof(float) * 3);
}

void Pointlight::setPosition(float x, float y, float z){
	this->position[0] = x;
	this->position[1] = y;
	this->position[2] = z;
}

void Pointlight::setPosition(glm::vec3 *pos) {
	this->position[0] = pos->r;
	this->position[1] = pos->g;
	this->position[2] = pos->b;
}

void Pointlight::scale(float *scale) {
	this->position[0] *= scale[0];
	this->position[1] *= scale[1];
	this->position[2] *= scale[2];
}

void Pointlight::rotate(float *rotate) {

}

void Pointlight::translate(float *translate) {
	this->position[0] += translate[0];
	this->position[1] += translate[1];
	this->position[2] += translate[2];
}

void Pointlight::initAttrs(unsigned int pos) {
	this->attrs = (const char**)malloc(sizeof(const char*) * 8);
	std::string posStrS = std::to_string(pos);
	const char* posStr = posStrS.c_str();

	this->attrs[0] = StringBuilder::concat("pointlight[", posStr, "].ambient");
	this->attrs[1] = StringBuilder::concat("pointlight[", posStr, "].diffuse");
	this->attrs[2] = StringBuilder::concat("pointlight[", posStr, "].specular");
	this->attrs[3] = StringBuilder::concat("pointlight[", posStr, "].position");
	this->attrs[4] = StringBuilder::concat("pointlight[", posStr, "].constant");
	this->attrs[5] = StringBuilder::concat("pointlight[", posStr, "].linear");
	this->attrs[6] = StringBuilder::concat("pointlight[", posStr, "].quadratic");
	this->attrs[7] = StringBuilder::concat("pointlight[", posStr, "].radius");
}

void Pointlight::init(glm::vec3 ambientC, glm::vec3 diffuseC, glm::vec3 specularC) {
	
	this->diffuse = (float*)malloc(sizeof(float) * 3);
	this->ambient = (float*)malloc(sizeof(float) * 3);
	this->specular = (float*)malloc(sizeof(float) * 3);

	this->diffuse[0] = diffuseC.r;
	this->diffuse[1] = diffuseC.g;
	this->diffuse[2] = diffuseC.b;

	this->ambient[0] = ambientC.r;
	this->ambient[1] = ambientC.g;
	this->ambient[2] = ambientC.b;

	this->specular[0] = specularC.r;
	this->specular[1] = specularC.g;
	this->specular[2] = specularC.b;

	this->constant = 1.0f;
	this->linear = 0.18f;
	this->quadratic = 0.064f;

	float lightMax = std::fmaxf(std::fmaxf(diffuseC.r, diffuseC.g), diffuseC.b);
	this->radius = -this->linear + std::sqrtf(this->linear * this->linear - 4 * this->quadratic * (this->constant - (256.0 / 5.0) * lightMax)) / (2 * this->quadratic);
	this->radius /= 2.0;
}

void Pointlight::shaderingData(unsigned int shader) {
	this->attr_ambient = glGetUniformLocation(shader, this->attrs[0]);
	this->attr_diffuse = glGetUniformLocation(shader, this->attrs[1]);
	this->attr_specular = glGetUniformLocation(shader, this->attrs[2]);
	this->attr_position = glGetUniformLocation(shader, this->attrs[3]);
	this->attr_constant = glGetUniformLocation(shader, this->attrs[4]);
	this->attr_linear = glGetUniformLocation(shader, this->attrs[5]);
	this->attr_quadratic = glGetUniformLocation(shader, this->attrs[6]);
	this->attr_radius = glGetUniformLocation(shader, this->attrs[7]);

}

void Pointlight::setAttenuation(float constant,float linear,float quadratic) {
	//this->constant = constant;
	//this->linear = linear;
	//this->quadratic = quadratic;
}

void Pointlight::apply() {

	glUniform3fv(this->attr_ambient, 1, this->ambient);
	glUniform3fv(this->attr_diffuse, 1, this->diffuse);
	glUniform3fv(this->attr_specular, 1, this->specular);
	glUniform3fv(this->attr_position, 1, this->position);

	glUniform1f(this->attr_constant, this->constant);
	glUniform1f(this->attr_linear, this->linear);
	glUniform1f(this->attr_quadratic, this->quadratic);
	
	glUniform1f(this->attr_radius, this->radius);

}


const char * Pointlight::getType() {
	return "Ponto de Luz";
}
