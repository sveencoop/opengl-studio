#include "StringBuilder.h"
#include <string>

namespace StringBuilder {

	char * concat(const char * str1, const char * str2) {
		int len1 = strlen(str1);
		int len2 = strlen(str2);
		int lenFinal = len1 + len2 + 1;

		char* saida = (char*)malloc(sizeof(char)*lenFinal);

		for (int i = 0; i < len1; i++) {
			saida[i] = str1[i];
		}

		for (int i = 0; i < len2; i++) {
			saida[i + len1] = str2[i];
		}

		saida[len1 + len2] = '\0';

		return saida;
	}

	char* concat(const char *str1, const  char *str2, const  char *str3) {
		int len1 = strlen(str1);
		int len2 = strlen(str2);
		int len3 = strlen(str3);
		int lenFinal = len1 + len2 + len3 + 1;

		char* saida = (char*)malloc(sizeof(char)*lenFinal);

		for (int i = 0; i < len1; i++) {
			saida[i] = str1[i];
		}

		for (int i = 0; i < len2; i++) {
			saida[i + len1] = str2[i];
		}

		for (int i = 0; i < len3; i++) {
			saida[i + len1 + len2] = str3[i];
		}

		saida[len1 + len2 + len3] = '\0';

		return saida;
	}

	bool equals(const char *str1, const  char *str2) {
		int len1 = strlen(str1);
		int len2 = strlen(str2);

		if (len1 != len2) {
			return false;
		}

		for (int i = 0; i < len1; i++) {
			if (str1[i] != str2[i]) {
				return false;
			}
		}

		return true;

	}

	char* copy(const char * str) {
		int len = strlen(str);

		char *saida = (char*)malloc(sizeof(char)*len) + 1;

		for (int i = 0; i < len; i++) {
			char a = str[i];
			saida[i] = a;
		}

		saida[len] = '\0';

		return saida;
	}

}