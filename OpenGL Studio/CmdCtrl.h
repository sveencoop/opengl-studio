#pragma once
#include <string>
#include <queue>
#include <vector>
#include <map>

struct Cmd {
	void(*function)(std::map<std::string, std::string>);
	std::string key;
};

struct RunCmd {
	void(*function)(std::map<std::string, std::string>);
	std::map<std::string, std::string> params;
};

class CmdCtrl {
private:
	std::vector<Cmd*> cmds;
	std::queue<RunCmd> queue;

public:
	bool lock;
	CmdCtrl();
	~CmdCtrl();
	void run();
	void registerF(std::string key, void(*function)(std::map<std::string, std::string>));

	bool add(std::string cmd, std::map<std::string, std::string>);
};

