#include "CmdCtrl.h"

CmdCtrl::CmdCtrl() {
	this->lock = false;
}

CmdCtrl::~CmdCtrl() {
	this->cmds.clear();
}

bool CmdCtrl::add(std::string cmd, std::map<std::string, std::string> params) {
	for (Cmd *m : this->cmds) {
		if (m->key == cmd) {
			RunCmd runcmd;
			runcmd.function = m->function;
			runcmd.params = params;
			this->queue.push(runcmd);
			return true;
		}
	}


	return false;
}

void CmdCtrl::run() {
	while (!this->queue.empty()) {
		RunCmd run = this->queue.front();
		run.function(run.params);
		this->queue.pop();
	}
	this->lock = false;
}

void CmdCtrl::registerF(std::string cmd, void(*function)(std::map<std::string, std::string>)) {
	Cmd *novocmd = new Cmd;
	novocmd->key = cmd;
	novocmd->function = function;
	this->cmds.push_back(novocmd);
}
