#pragma once

namespace StringBuilder {
	char* concat(const char *str1, const char *str2);
	char* concat(const char *str1, const char *str2, const char *str3);
	bool equals(const char *str1, const  char *str2);
	char* copy(const char *str);
};

