#pragma once
#include <ode\ode.h>
#include <vector>
#include "Obj3D.h"

class PhysicsEngine {

private:
	static dWorldID world;
	static dSpaceID space;
	static dGeomID  ground;
	static dJointGroupID contactgroup;
	static dJointFeedback *feedback;
	static std::vector<dBodyID> bodies;
	static float max_vel;

	static void collide(void * data, dGeomID o1, dGeomID o2);
	static void bodyUpdateEvent(dBodyID body);
	static void appendObject(Replica *obj, dBodyID body);
	static void createBall(Replica *obj);
	static void createCrate(Replica *obj);
	static void createCapsule(Replica *obj);
	static void createCilindro(Replica *obj);
	static void createObject(Replica *obj);
	static void addBody(dBodyID body);
	static void clampMaxVelocity(dBodyID body, float x, float y, float z);

public:
	static void enableGravity();
	static void init();
	static void loop(double time);
	static void registerObject(Object3D* obj);
	static void resetObjects();
	static void finish();
};

