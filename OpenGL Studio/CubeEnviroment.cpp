#include "CubeEnviroment.h"
#include "TextureControl.h"
#include "InterfaceIO.h"

using namespace GlobalInterfaceIO;

//learn by: https://darrensweeney.net/2016/10/03/dynamic-cube-mapping-in-opengl/

CubeEnviroment::CubeEnviroment(unsigned int cubemap_size, glm::vec3 pos) {

	unsigned int cubemap = texture::initCompileCubeMap();

	texture::allocFaceCubeMap(GL_TEXTURE_CUBE_MAP_POSITIVE_X, cubemap_size);
	texture::allocFaceCubeMap(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, cubemap_size);
	texture::allocFaceCubeMap(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, cubemap_size);
	texture::allocFaceCubeMap(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, cubemap_size);
	texture::allocFaceCubeMap(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, cubemap_size);
	texture::allocFaceCubeMap(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, cubemap_size);

	this->fbo_positive_x = texture::attachFaceCubeMap(cubemap, GL_TEXTURE_CUBE_MAP_POSITIVE_X, cubemap_size);
	this->fbo_negative_x = texture::attachFaceCubeMap(cubemap, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, cubemap_size);
	this->fbo_positive_y = texture::attachFaceCubeMap(cubemap, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, cubemap_size);
	this->fbo_negative_y = texture::attachFaceCubeMap(cubemap, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, cubemap_size);
	this->fbo_positive_z = texture::attachFaceCubeMap(cubemap, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, cubemap_size);
	this->fbo_negative_z = texture::attachFaceCubeMap(cubemap, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, cubemap_size);

	this->id_cubemap = cubemap;
	this->size = cubemap_size;
	this->pos = pos;

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		log("Framebuffer n�o foi carregado ainda :: CUBE_ENVIROMENT");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

