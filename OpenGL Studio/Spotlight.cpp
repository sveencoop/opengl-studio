#include "Spotlight.h"
#include <glad\glad.h>
#include "timer.h"
#include <string>
#include "StringBuilder.h"
#include "TextureControl.h"

Spotlight::Spotlight() {
	this->diffuse = (float*)malloc(sizeof(float) * 3);
	this->ambient = (float*)malloc(sizeof(float) * 3);
	this->specular = (float*)malloc(sizeof(float) * 3);

	this->position = (float*)malloc(sizeof(float) * 3);
	this->direction = (float*)malloc(sizeof(float) * 3);
}

void Spotlight::init() {

	this->diffuse[0] = 1.0f;
	this->diffuse[1] = 1.0f;
	this->diffuse[2] = 1.0f;

	this->ambient[0] = 0.05f;
	this->ambient[1] = 0.05f;
	this->ambient[2] = 0.05f;

	this->specular[0] = 1.0f;
	this->specular[1] = 1.0f;
	this->specular[2] = 1.0f;

	this->constant = 1.0f;
	this->linear = 0.045f;
	this->quadratic = 0.0075f;
	this->innerCutOff = glm::cos(glm::radians(13.0f));
	this->outerCutOff = glm::cos(glm::radians(20.0f));
	this->epsilon = this->innerCutOff - this->outerCutOff;
	this->intensity = 0.0f;
	this->targetIntensity = 0.0f;

	this->radius = -this->linear + std::sqrtf(this->linear * this->linear - 4 * this->quadratic * (this->constant - (256.0 / 5.0) * 1.0)) / (2 * this->quadratic);
}

void Spotlight::initAttrs(unsigned int pos) {
	this->attrs = (const char**)malloc(sizeof(const char*) * 13);
	std::string prefixS = std::string("spotlight[") + std::to_string(pos) + std::string("].");
	const char *prefix = prefixS.c_str();

	this->attrs[0] = StringBuilder::concat(prefix, "ambient");
	this->attrs[1] = StringBuilder::concat(prefix, "diffuse");
	this->attrs[2] = StringBuilder::concat(prefix, "specular");
	this->attrs[3] = StringBuilder::concat(prefix, "intensity");
	this->attrs[4] = StringBuilder::concat(prefix, "constant");
	this->attrs[5] = StringBuilder::concat(prefix, "linear");
	this->attrs[6] = StringBuilder::concat(prefix, "quadratic");
	this->attrs[7] = StringBuilder::concat(prefix, "position");
	this->attrs[8] = StringBuilder::concat(prefix, "direction");
	this->attrs[9] = StringBuilder::concat(prefix, "outerCutOff");
	this->attrs[10] = StringBuilder::concat(prefix, "cutOff");
	this->attrs[11] = StringBuilder::concat(prefix, "epsilon");
	this->attrs[12] = StringBuilder::concat(prefix, "radius");
	this->pos = pos;

}

void Spotlight::init(glm::vec3 ambientC, glm::vec3 diffuseC, glm::vec3 specularC) {

	this->diffuse[0] = diffuseC.r;
	this->diffuse[1] = diffuseC.g;
	this->diffuse[2] = diffuseC.b;

	this->ambient[0] = ambientC.r;
	this->ambient[1] = ambientC.g;
	this->ambient[2] = ambientC.b;

	this->specular[0] = specularC.r;
	this->specular[1] = specularC.g;
	this->specular[2] = specularC.b;

	this->constant = 0.5f;
	this->linear = 0.045f;
	this->quadratic = 0.0075f;
	this->innerCutOff = glm::cos(glm::radians(13.0f));
	this->outerCutOff = glm::cos(glm::radians(20.0f));
	this->intensity = 1.0f;
	this->targetIntensity = 1.0f;

	//this->attrs[12] = StringBuilder::concat("spotlight[", posStr, "].far_plane");
	//this->attrs[13] = StringBuilder::concat("spotShadowMap[", posStr, "]");
	//this->attrs[14] = StringBuilder::concat("spotlight[", posStr, "].lightSpaceMatrix");

	float lightMax = std::fmaxf(std::fmaxf(diffuseC.r, diffuseC.g), diffuseC.b);
	this->radius = -this->linear + std::sqrtf(this->linear * this->linear - 4 * this->quadratic * (this->constant - (256.0 / 5.0) * lightMax)) / (2 * this->quadratic);
	this->radius /= 2.0;
}

void Spotlight::toggleIntensity() {

	if (this->targetIntensity == 0.0) {
		this->targetIntensity = 1.0;
	} else if (this->targetIntensity == 1.0) {
		this->targetIntensity = 0.0;
	}

}

void Spotlight::setAttenuation(float constant, float linear, float quadratic) {
	//this->constant = constant;
	//this->linear = linear;
	//this->quadratic = quadratic;
}

void Spotlight::setPosition(float x, float y, float z) {
	this->position[0] = x;
	this->position[1] = y;
	this->position[2] = z;
}

void Spotlight::setDirection(float x, float y, float z) {
	this->direction[0] = x;
	this->direction[1] = y;
	this->direction[2] = z;
}

void Spotlight::scale(float *scale) {
	this->position[0] *= scale[0];
	this->position[1] *= scale[1];
	this->position[2] *= scale[2];
}

void Spotlight::rotate(float *rotate) {

}

void Spotlight::translate(float *translate) {
	this->position[0] += translate[0];
	this->position[1] += translate[1];
	this->position[2] += translate[2];
}

void Spotlight::setOuterCutOff(float value) {
	this->outerCutOff = value;
}

void Spotlight::setInnerCutOff(float value) {
	this->innerCutOff = value;
}

void Spotlight::updateEpsilon() {
	this->epsilon = this->innerCutOff - this->outerCutOff;
}

void Spotlight::shaderingDataFlashlight(unsigned int shader) {
	this->attr_ambient = glGetUniformLocation(shader, "flashlight.ambient");
	this->attr_diffuse = glGetUniformLocation(shader, "flashlight.diffuse");
	this->attr_specular = glGetUniformLocation(shader, "flashlight.specular");

	this->attr_intensity = glGetUniformLocation(shader, "flashlight.intensity");
	this->attr_constant = glGetUniformLocation(shader, "flashlight.constant");
	this->attr_linear = glGetUniformLocation(shader, "flashlight.linear");
	this->attr_quadratic = glGetUniformLocation(shader, "flashlight.quadratic");

	this->attr_position = glGetUniformLocation(shader, "flashlight.position");
	this->attr_direction = glGetUniformLocation(shader, "flashlight.direction");
	this->attr_outerCutOff = glGetUniformLocation(shader, "flashlight.outerCutOff");
	this->attr_cutOff = glGetUniformLocation(shader, "flashlight.cutOff");

	this->attr_radius = glGetUniformLocation(shader, "flashlight.radius");
}

void Spotlight::shaderingData(unsigned int shader) {
	this->attr_ambient = glGetUniformLocation(shader, this->attrs[0]);
	this->attr_diffuse = glGetUniformLocation(shader, this->attrs[1]);
	this->attr_specular = glGetUniformLocation(shader, this->attrs[2]);

	this->attr_intensity = glGetUniformLocation(shader, this->attrs[3]);
	this->attr_constant = glGetUniformLocation(shader, this->attrs[4]);
	this->attr_linear = glGetUniformLocation(shader, this->attrs[5]);
	this->attr_quadratic = glGetUniformLocation(shader, this->attrs[6]);

	this->attr_position = glGetUniformLocation(shader, this->attrs[7]);
	this->attr_direction = glGetUniformLocation(shader, this->attrs[8]);
	this->attr_outerCutOff = glGetUniformLocation(shader, this->attrs[9]);
	this->attr_cutOff = glGetUniformLocation(shader, this->attrs[10]);
	this->attr_epsilon = glGetUniformLocation(shader, this->attrs[11]);

	this->attr_radius = glGetUniformLocation(shader, this->attrs[12]);
}

void Spotlight::unset() {
	glUniform1f(this->attr_intensity, 0);
}

void Spotlight::apply(float *usethatposition, float *usethatdirection) { //flashlight

	if (this->targetIntensity != this->intensity) {
		this->intensity = this->targetIntensity;
		//double sum = (this->targetIntensity - this->intensity) * 10 * timer::getLastTime();
		//this->intensity += sum;
	}
	if (this->intensity == 0) {
		glUniform1f(this->attr_intensity, 0);
	} else {
		glUniform3fv(this->attr_ambient, 1, this->ambient);
		glUniform3fv(this->attr_diffuse, 1, this->diffuse);
		glUniform3fv(this->attr_specular, 1, this->specular);

		glUniform1f(this->attr_intensity, this->intensity);

		glUniform1f(this->attr_constant, this->constant);
		glUniform1f(this->attr_linear, this->linear);
		glUniform1f(this->attr_quadratic, this->quadratic);

		glUniform3fv(this->attr_position, 1, usethatposition);
		glUniform3fv(this->attr_direction, 1, usethatdirection);
		glUniform1f(this->attr_outerCutOff, this->outerCutOff);
		glUniform1f(this->attr_cutOff, this->innerCutOff);
		
		glUniform1f(this->attr_radius, this->radius);
	}
}

void Spotlight::apply() {
	glUniform3fv(this->attr_ambient, 1, this->ambient);
	glUniform3fv(this->attr_diffuse, 1, this->diffuse);
	glUniform3fv(this->attr_specular, 1, this->specular);

	glUniform1f(this->attr_intensity, this->intensity);

	glUniform1f(this->attr_constant, this->constant);
	glUniform1f(this->attr_linear, this->linear);
	glUniform1f(this->attr_quadratic, this->quadratic);

	glUniform3fv(this->attr_position, 1, this->position);
	glUniform3fv(this->attr_direction, 1, this->direction);
	glUniform1f(this->attr_outerCutOff, this->outerCutOff);
	glUniform1f(this->attr_cutOff, this->innerCutOff);
	glUniform1f(this->attr_epsilon, this->epsilon);

	glUniform1f(this->attr_radius, this->radius);
}

const char * Spotlight::getType() {
	return "Luz Direcional";
}
