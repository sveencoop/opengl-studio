#pragma once

#include <vector>
#include <string>

struct physic_type {
	bool gravity = false;

	int index;
	std::string nome;
	std::string path;
	std::string filename;
	double mass;
};

struct instDataImport {
	float scale[3];
	float translate[3];
	float rotate[3];
};

struct dataImport {
	std::string path;
	std::string filename;

	std::vector<instDataImport> insts;
	physic_type *physic;
};

class bb_dt {

public:
	unsigned int esfera;
	unsigned int cubo;
    unsigned int cilindro;
	unsigned int capsula;
    unsigned int objeto;

	bb_dt();
	std::vector<physic_type*> *tipos_bounding_box;

private:
	void add(std::string name, unsigned int *enum_type);
};

dataImport createDefaultDataImport(const char *path, const char *filename, const char* name);

#define dataImport(X,Y,Z) createDefaultDataImport(X,Y,Z)