#include "ScreenEngine.h"
#include "ShaderControl.h"
#include "TextureControl.h"
#include "InterfaceIO.h"
#include <glad\glad.h>

ScreenEngine::ScreenEngine(unsigned int width, unsigned int height) {

	this->width = width;
	this->height = height;

	unsigned int screen_shader = shader::compileShader("../OpenGL Studio/Shaders/screen/test.vsh", "../OpenGL Studio/Shaders/screen/test.fsh");
	unsigned int loading_shader = shader::compileShader("../OpenGL Studio/Shaders/screenloading/test.vsh", "../OpenGL Studio/Shaders/screenloading/test.fsh");
	unsigned int blur_shader = shader::compileShader("../OpenGL Studio/Shaders/screen/screen.vsh", "../OpenGL Studio/Shaders/screen/screen.fsh");
	unsigned int component_shader = shader::compileShader("../OpenGL Studio/Shaders/ScreenComponent/test.vsh", "../OpenGL Studio/Shaders/ScreenComponent/invert.fsh");

	unsigned int fbo = texture::createFrameBuffer();
	unsigned int *texture = texture::createFrameBufferTexture2(width, height);
	texture::bindFrameBufferColorMap2(fbo, texture[0], texture[1]);

	unsigned int fbo_blur_horizontal = texture::createFrameBuffer();
	unsigned int texture_blur_horizontal = texture::createFrameBufferTexture(width, height);
	texture::bindFrameBufferColorMap(fbo_blur_horizontal, texture_blur_horizontal);

	unsigned int fbo_blur_vertical = texture::createFrameBuffer();
	unsigned int texture_blur_vertical = texture::createFrameBufferTexture(width, height);
	texture::bindFrameBufferColorMap(fbo_blur_vertical, texture_blur_vertical);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	std::string atual_folder = GlobalInterfaceIO::getAtualWorkingFolder("\\..\\OpenGL Studio\\Textures\\Background\\default2.png");
	const char *folder = atual_folder.c_str();

	unsigned int loading_buffer = texture::compile(folder, GL_REPEAT);

	float quadVertices[] = {
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f
	};

	unsigned int quadVAO, quadVBO;

	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);

	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

	glUseProgram(component_shader);

	glUniform1i(glGetUniformLocation(component_shader, "image"), 0);
	glUniform1i(glGetUniformLocation(component_shader, "render"), 1);
	this->attr_id_pos_x = glGetUniformLocation(component_shader, "posX");
	this->attr_id_pos_y = glGetUniformLocation(component_shader, "posY");
	this->attr_id_size_x = glGetUniformLocation(component_shader, "sizeX");
	this->attr_id_size_y = glGetUniformLocation(component_shader, "sizeY");
	this->attr_id_WindowSize = glGetUniformLocation(component_shader, "WindowSize");

	glUseProgram(blur_shader);
	glUniform1i(glGetUniformLocation(screen_shader, "image"), 0);
	this->attr_id_horizontal = glGetUniformLocation(blur_shader, "horizontal");

	glUseProgram(screen_shader);

	this->attr_id_bloom = glGetUniformLocation(screen_shader, "bloom");
	this->attr_id_exposure = glGetUniformLocation(screen_shader, "exposure");
	this->attr_id_gamma = glGetUniformLocation(screen_shader, "gamma");
	this->exposure = 1.0;
	this->gamma = 1.0;
	this->bloom = 1;

	this->apply();
	glUniform1i(glGetUniformLocation(screen_shader, "screenTexture"), 0);
	glUniform1i(glGetUniformLocation(screen_shader, "bloomTexture"), 1);

	this->id_framebuffer_to_screen = fbo;
	this->id_framebuffer_blur_horizontal = fbo_blur_horizontal;
	this->id_framebuffer_blur_vertical = fbo_blur_vertical;

	this->id_texture_loading = loading_buffer;
	this->id_shader_loading = loading_shader;
	this->id_texture = texture[0];
	this->id_texture_bloom = texture[1];
	this->id_texture_blur_horizontal = texture_blur_horizontal;
	this->id_texture_blur_vertical = texture_blur_vertical;

	this->id_shader_component = component_shader;
	this->id_shader_blur = blur_shader;
	this->id_shader = screen_shader;
	this->id_quad_object = quadVAO;

	unsigned int rbo;
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

	unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, attachments);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {

	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void ScreenEngine::apply() {
	glUniform1i(this->attr_id_bloom, this->bloom);
	glUniform1f(this->attr_id_exposure, this->exposure);
	glUniform1f(this->attr_id_gamma, this->gamma);
}

void ScreenEngine::use() {
	glUseProgram(this->id_shader);
}

void ScreenEngine::render() {

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_STENCIL_TEST);
	glDepthMask(GL_TRUE);

	if (this->bloom) {
		/*Blur Bloom*/
		glUseProgram(this->id_shader_blur);
		glBindVertexArray(this->id_quad_object);

		glBindFramebuffer(GL_FRAMEBUFFER, this->id_framebuffer_blur_horizontal);
		glUniform1i(this->attr_id_horizontal, 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->id_texture_bloom);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindFramebuffer(GL_FRAMEBUFFER, this->id_framebuffer_blur_vertical);
		glUniform1i(this->attr_id_horizontal, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->id_texture_blur_horizontal);
		glDrawArrays(GL_TRIANGLES, 0, 6);

	}
	/*Draw Screen*/

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(this->id_shader);
	glBindVertexArray(this->id_quad_object);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->id_texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->id_texture_blur_vertical);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glUseProgram(this->id_shader_component);
	glBindVertexArray(this->id_quad_object);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->id_texture);

	glUniform2f(this->attr_id_WindowSize, this->width, this->height);

	for (ScreenComponent *component : this->components) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, component->id_texture);

		glUniform1f(this->attr_id_pos_x, component->posX);
		glUniform1f(this->attr_id_pos_y, component->posY);
		glUniform1f(this->attr_id_size_x, component->sizeX);
		glUniform1f(this->attr_id_size_y, component->sizeY);

		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
}

void ScreenEngine::addImageComponent(const char * path, float x, float y, int xx, int yy) {

	ScreenComponent *novo = new ScreenComponent;

	novo->id_texture = texture::compile(path);
	novo->posX = x * 2 - 1.0;
	novo->posY = y * 2 - 1.0;
	novo->sizeX = (xx * 2.0) / (float)width;
	novo->sizeY = (yy * 2.0) / (float)height;

	components.push_back(novo);
}

void ScreenEngine::freeShaders() {
	shader::freeShader(this->id_shader);
	shader::freeShader(this->id_shader_blur);
	shader::freeShader(this->id_shader_loading);
	shader::freeShader(this->id_shader_component);
}

void ScreenEngine::freeBuffers() {
	glDeleteFramebuffers(1, &this->id_framebuffer_to_screen);
	glDeleteFramebuffers(1, &this->id_framebuffer_blur_horizontal);
	glDeleteFramebuffers(1, &this->id_framebuffer_blur_vertical);
}
