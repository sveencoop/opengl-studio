#include "openglUtils.h"

namespace openglUtils {

	void use(unsigned int shader_programme, unsigned int vertex_data) {
		glUseProgram(shader_programme);
		glBindVertexArray(vertex_data);
	}

	void setTexture(unsigned int index, unsigned int id) {
		glActiveTexture(GL_TEXTURE0 + index);
		glBindTexture(GL_TEXTURE_2D, id);
	}

	void setVec3(unsigned int shader_programme, const char *name, const float *vec) {
		unsigned int id = glGetUniformLocation(shader_programme, name);
		glUniform3fv(id, 1, vec);
	}

	void setMatriz(unsigned int shader_programme, const char *name, const float *value) {
		unsigned int id = glGetUniformLocation(shader_programme, name);
		glUniformMatrix4fv(id, 1, GL_FALSE, value);
	}

	void setFloat(unsigned int shader_programme, const char * name, float value) {
		unsigned int id = glGetUniformLocation(shader_programme, name);
		glUniform1f(id, value);
	}

	void setInt(unsigned int shader_programme, const char * name, int value) {
		unsigned int id = glGetUniformLocation(shader_programme, name);
		glUniform1i(id, value);
	}

}