#pragma once
#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include "Camera.h"


class InputEvents {
	private:
		GLFWwindow* window;
		Camera *cam;
		double tarAccCamY;
		double accCamY;
		double tarCrouchCamY = 1.5;
		double crouchCamY = 1.5;
		double lastX, lastY;
		Replica *item_grab;
		double item_grab_yaw, item_grab_pitch, item_grab_roll;

		void interact(Replica *m);
	public:
		InputEvents();
		bool shooting;
		bool moving;
		bool running;
		bool strafing;
		bool backward;
		bool second_button;
		void update_grab();
		bool firstMouse = true;
		void bindCamera(Camera *cam);
		void setWindow(GLFWwindow *window, int SCR_WIDTH, int SCR_HEIGHT);
		void applyKeyboard(double time);
		void applyCharTyped(unsigned int key);
		void applyKeyTyped(int key, int scancode, int action, int mods);

		void applyClick(int button, int action, int mods);
		void applyMouse(double xpos, double ypos);
};

