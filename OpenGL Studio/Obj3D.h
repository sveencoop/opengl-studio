#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ode/ode.h>
#include "DataType.h"
#include "TextureControl.h"

struct Limit {
	double min = 1.79769e+308;
	double max = -1.79769e+308;
};

class Limits {
public:
	Limit x;
	Limit y;
	Limit z;
	glm::vec3 get_center();
	Limits(Limits *copy);
	Limits();
	void test(glm::vec3 Position);
	void resize(glm::vec3 resize);
	glm::vec3 *bounding_box();
	double maxRadius();
	double lx();
	double ly();
	double lz();
};

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangents;
	glm::vec3 BiTangents;
	glm::vec2 ShMCoords;
};

class Object3D;

class Replica {
private:

	glm::mat4 transf;
	glm::mat4 oriTranf;
	instDataImport inst;

public:
	Object3D * parent;
	dBodyID rigid_body;

	glm::vec3 center;
	glm::vec3 position;
	glm::vec3 scale;
	glm::mat4 rotateCenterMatriz;
	glm::mat4 adictionalMatriz;
	glm::mat4 rotateMatriz;
	glm::vec3 idColor;
	glm::mat4 getTransformationMatrix();
	Limits *limits;

	void setIdColor(float r, float g, float b);
	void setTransformation(glm::mat4 trans);
	void compileTransform();
	void setAdictionalMatriz(glm::mat4 matriz);
	void setCenter(glm::vec3 center);
	void setPosition(glm::vec3 position);
	void setRotation(glm::vec3 rotation);
	void setRotationOnCenter(glm::vec3 rotationOnCenter);
	void setMatrizRotation(glm::mat4 matriz);
	
	instDataImport getInst();

	void setRigidBody(dBodyID id);
	void updateFromRigidBody();

	Replica(Object3D* parent, instDataImport inst);
	~Replica();
};

struct Material {
	unsigned int textureDiffuse;
	unsigned int textureSpecular;
	unsigned int textureReflection;
	unsigned int textureNormal;
	unsigned int textureEmission;

	int hasNormal;
	int hasEmission;
	int withTexture;
	float shininess;
	float reflection;
	float IOR;
};

class Mesh {
private:
	TextureImage *dataImgDiffuseToLoad;
	TextureImage *dataImgSpecularToLoad;
	TextureImage *dataImgReflectionToLoad;
	TextureImage *dataImgNormalToLoad;
	TextureImage *dataImgEmissionToLoad;

public:
	unsigned int vao;

	const char *name;
	Material *material;

	glm::vec3 diffuseColor;
	std::vector<unsigned int> indices;
	std::vector<Vertex> vertices;

	unsigned int materialBuffer;

	Object3D *parent;

	Mesh(const char* name, std::vector<Vertex>& vertices, std::vector<unsigned int> indices);
	~Mesh();

	void compileVertexData();

	void setShininess(float shininess);
	void setReflection(float value);
	void setIOR(float value);
	void setDiffuseColor(unsigned int id, float r, float g, float b);

	void setTextureImageDiffuse(TextureImage *image);
	void setTextureImageSpecular(TextureImage *image);
	void setTextureImageReflection(TextureImage *image);
	void setTextureImageNormal(TextureImage *image);
	void setTextureImageEmission(TextureImage *image);

	void flushTextures();
	void freeLoad();
};

class Object3D {
private:
	const char *name;

public:
	std::vector<Mesh*> meshes;
	std::vector<Replica*> replicas;

	glm::mat4 oriTranf;
	Limits *limits;
	dataImport dataImported;
	unsigned int shader_programme;

	Object3D(const char* name);
	
	void setShaderProgramme(unsigned int id);
	void translateTransform(glm::vec3 translate);

	Replica *addReplica(instDataImport data);

	void setTransform(glm::mat4 trans);
	void setName(const char *name);
	const char *getName();

	void addMesh(Mesh *m);

	bool isOutlined;
	void setCompileOutline(bool value);
};