#include "DataType.h"

dataImport createDefaultDataImport(const char *path, const char *filename, const char* name) {
	dataImport data;

	instDataImport inst;
	inst.translate[0] = 0;
	inst.translate[1] = 0;
	inst.translate[2] = 0;
	inst.scale[0] = 1;
	inst.scale[1] = 1;
	inst.scale[2] = 1;
	inst.rotate[0] = 0;
	inst.rotate[1] = 0;
	inst.rotate[2] = 0;

	data.path = std::string(path);
	data.filename = std::string(filename);

	bb_dt *bb_datatype = new bb_dt;
	data.physic = bb_datatype->tipos_bounding_box->at(bb_datatype->objeto);
	data.insts.push_back(inst);

	return data;
}

void bb_dt::add(std::string name, unsigned int *enum_type) {
	physic_type *type = new physic_type();
	unsigned int index = tipos_bounding_box->size();
	type->index = index;
	type->nome = name;
	tipos_bounding_box->push_back(type);
	*enum_type = index;
}

bb_dt::bb_dt() {
	this->tipos_bounding_box = new std::vector<physic_type*>();
	this->add("ESFERA", &this->esfera);
	this->add("CILINDRO", &this->cilindro);
	this->add("CUBO", &this->cubo);
	this->add("CAPSULA", &this->capsula);
	this->add("OBJETO", &this->objeto);
}

