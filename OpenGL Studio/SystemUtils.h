#pragma once
#include <wx/string.h>
#include <wx/utils.h>
#include <wx/datetime.h>
#include <wx/file.h>
#include <wx/textfile.h>

class SystemUtils {
public:
	static void getDateTime();
	static void getComputerInfo();
	static void writeFile();
	static void dumpheader();
};

