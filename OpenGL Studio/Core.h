#pragma once

#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include "PhysicsEngine.h"
#include "InputEvents.h"
#include "Obj3D.h"
#include "Camera.h"
#include "Sun.h"
#include "ScreenEngine.h"
#include "Pointlight.h"
#include "Spotlight.h"
#include "Skybox.h"
#include "Fog.h"
#include "LoaderMesh.h"
#include "CubeEnviroment.h"
#include "DataType.h"
#include "Trigger.h"
#include "TextureControl.h"
#include "ShaderControl.h"
#include <string>
#include "CmdCtrl.h"
#include "AnimCtrl.h"

class engineWindow {
	private:
		static void framebuffer_size_callback(GLFWwindow *window, int width, int height);
		static void initEventListeners();
		static bool hasFocus;
		static Trigger* callbackMenu;
		static void callbackMouse(GLFWwindow* window, double xpos, double ypos);
		static void callbackChar(GLFWwindow* window, unsigned int code);
		static void callbackKey(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void callbackClick(GLFWwindow* window, int button, int action, int mods);
		static void error_callback(int error, const char* description);
	public:
		static void force_close();
		static unsigned int w_width;
		static unsigned int w_height;
		static float w_ratio;
		static GLFWwindow *window;
		static InputEvents* controller;
		static void setCallbackMenu(Trigger *callback);

		static void setTitle(std::string title);
		static void init();
		static GLFWwindow* novaJanela(char const *title,int width,int height, bool fullscreen);
		static void loop();

		static void blur();
		static void focus();

		static void end();
};

class Render;
class ShadowMapping;
class ModuloGame;

class engine {
	private:
		static std::vector<Mesh*> toFlushGL;
		static std::vector<TextureImage*> toFlush;
		static std::vector<dataImport> filesToLoad;
		static std::vector<dataImport> itensToLoad;
	public:
		static CmdCtrl *cmdCtrl;
		static Render *render;
		static ModuloGame *moduloGame;

		static unsigned int outline_shader;
		static unsigned int boundingBox_shader;

		static AnimCtrl *swing_item;
		static AnimCtrl *shoot_item;

		static float loading;
		static ScreenEngine *screen;
		static Sun *sun;
		static Camera *cam;
		static Spotlight flashlight;
		static LightsVector *lights;
		static CubeEnviroment *cubemap;
		static Object3D *selected_hand_item;
		static Fog *fog;
		static bool cullface;
		static bool normalmapping;
		static glm::vec3 recoil_aim;

		static std::vector<Object3D*> objects;
		static std::vector<Replica*> allreplicas;

		static std::vector<Object3D*> hand_itens;

		static void clearHightlightObject();

		static void animation_hand_item(double value);
		static void animation_shoot(double value);
		
		static void trigger_shoot();
		static void next_hand_item();

		static void initPhysicsEngine();
		static void finishScene();
		static void loadingRender();
		static void sceneRender();
		static void initAmbience();
		static void initCam(float x, float y, float z);
		static void initCmdCtrl();
		
		static void pushObject(dataImport importData);
		static void pushItens(dataImport importData);
		static void compileShaders();
		static void flushObjects();
		static void flushItens();
		static void flagLoad();
		static Replica* pickObject(float x = -1.0f, float y = -1.0f);

		static void enableCullface(std::map<std::string, std::string> params);
		static void flushToGL(std::map<std::string, std::string> params);
		static void updateParams(std::map<std::string, std::string> params);
		static void updateParamsScreen(std::map<std::string, std::string> params);

		static void cmd(const char* command);
		static void toggleFlyMode();
		static void toggleFlashlight();
		static void toggleFog();

		static void renderCubemap(CubeEnviroment *cubemap);
		static void drawBasic(unsigned int fbo, Camera *cam);
		static void drawBasicScene(Camera *cam);

		static void freeShaders();
		static void freeBuffers();
		static void freeTextures();
		static void clearVectors();
		static void close();

};

class Render {
public:
	virtual void draw(double interval, double time) = 0;
};

class drawBoundingBox : public Render {
public:
	void draw(double interval, double time);
};

class ModuloGame : public Render {
private:
	void gBuffering();
	void drawScene();
	void drawHandItem();
	void drawNoDeferred(double interval);
	void drawDeferred(double interval);
	void drawObject(Object3D *obj, float* view_projection, float *posCam, float *frontCam, float time);
	ShadowMapping *shadow_mapping;
	Skybox *skybox;

	unsigned int texture_positions;
	unsigned int texture_normals;
	unsigned int textures_albedoSpec;

	unsigned int fbo_deferred;

public:
	GameShader * game_shader;
	DeferredShader * deferred_shader;
	void updateShadowMapping();
	void shaderingData();
	void apply();
	void freeShaders();
	void freeBuffers();
	ModuloGame();
	~ModuloGame();
	void draw(double interval, double time);

};

class drawLoading : public Render {
public:
	void draw(double interval, double time);
};

class ShadowMapping {
private:
	unsigned int size;
public:
	unsigned int fbo;
	unsigned int shader;
	glm::mat4 view_projection;
	unsigned int texture;
	ShadowMapping(unsigned int SHADOW_SIZE = 1024);
	void apply();
	void change();
	void update();
};

