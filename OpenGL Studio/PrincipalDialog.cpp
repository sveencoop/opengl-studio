#include "PrincipalDialog.h"
/* Apps Panel */


SidebarApps::SidebarApps(wxPanel * parent) : wxPanel(parent, -1, wxPoint(-1, -1), wxSize(-1, -1), wxBORDER_SUNKEN) {

	this->title_label = new wxStaticText(this, -1, wxT("Aplicativos"));

	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	this->editor_btn = new wxButton(this, ID_EDITOR, wxT("Editor de Cen�rio"));
	this->Connect(ID_EDITOR, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SidebarApps::OpenEditorPanel));

	this->config_btn = new wxButton(this, ID_CONFIG, wxT("Configura��o"));
	this->Connect(ID_CONFIG, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SidebarApps::OpenConfigPanel));

	this->SetMaxSize(wxSize(200, -1));
	this->SetMinSize(wxSize(200, -1));

	vbox->Add(this->title_label, 0.3, wxEXPAND | wxALL, 10);
	vbox->Add(this->editor_btn, 1, wxEXPAND | wxALL, 5);
	vbox->Add(this->config_btn, 0.3, wxEXPAND | wxALL, 5);

	//vbox->SetSizeHints(this);

	this->SetSizer(vbox);
}

void SidebarApps::OpenDefaultPanel(wxCommandEvent & WXUNUSED(event)) {
	this->m_parent->switchPanel(ID_DEFAULT);
}

void SidebarApps::OpenEditorPanel(wxCommandEvent & WXUNUSED(event)) {
	this->m_parent->switchPanel(ID_EDITOR);
}

void SidebarApps::OpenConfigPanel(wxCommandEvent & WXUNUSED(event)) {
	this->m_parent->switchPanel(ID_CONFIG);
}

void SidebarApps::setMenuInicial(MenuInicial * parent) {
	this->m_parent = parent;
}

/* Apps Forms */

DefaultAboutPanel::DefaultAboutPanel(wxPanel* parent) : wxPanel(parent, ID_DEFAULT, wxDefaultPosition, wxSize(270, 150), wxBORDER_SUNKEN) {
	this->SetBackgroundColour(wxColor("rgb(230,230,231)"));
}

void DefaultAboutPanel::setMenuInicial(MenuInicial * parent) {
	this->m_parent = parent;
}

EditorAboutPanel::EditorAboutPanel(wxPanel* parent) : wxPanel(parent, ID_EDITOR, wxDefaultPosition, wxSize(270, 150), wxBORDER_SUNKEN) {

	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	wxButton *button = new wxButton(this, ID_EDITOR, wxT("Abrir"));
	this->Connect(ID_EDITOR, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditorAboutPanel::OpenEditor));

	wxButton *buttonImport = new wxButton(this, ID_DIALOG_IMPORT, wxT("Importar de arquivo"));
	this->Connect(ID_DIALOG_IMPORT, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditorAboutPanel::OpenImportDialog));

	this->buttonRemove = new wxButton(this, ID_REMOVE_ITEM, wxT("Remover Objeto"));
	this->Connect(ID_REMOVE_ITEM, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditorAboutPanel::OnRemoveItem));

	this->buttonEdit = new wxButton(this, ID_EDIT_ITEM, wxT("Editar Objeto"));
	this->Connect(ID_EDIT_ITEM, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditorAboutPanel::OnEditItem));

	wxButton *buttonClear = new wxButton(this, ID_CLEAR_ITENS, wxT("Limpar Tudo"));
	this->Connect(ID_CLEAR_ITENS, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditorAboutPanel::OnClearItem));

	this->w_width = new wxSpinCtrl(this, ID_W_WIDTH, wxT("800"), wxDefaultPosition, wxDefaultSize, 16896L, 800, 1920, 800);
	this->w_height = new wxSpinCtrl(this, ID_W_HEIGHT, wxT("600"), wxDefaultPosition, wxDefaultSize, 16896L, 600, 1080, 600);
	this->w_fullscreen = new wxCheckBox(this, ID_W_FULLSCREEN, wxT("Tela Cheia"));

	wxBoxSizer *buttonsList = new wxBoxSizer(wxHORIZONTAL);

	buttonsList->Add(buttonImport, 0);
	buttonsList->Add(this->buttonRemove, 0);
	buttonsList->Add(this->buttonEdit, 0);
	buttonsList->Add(buttonClear, 0);

	this->list_control = new wxListCtrl(this, ID_LIST, wxDefaultPosition, wxDefaultSize, wxLC_REPORT);
	this->Connect(ID_LIST, wxEVT_LIST_ITEM_SELECTED, wxListEventHandler(EditorAboutPanel::OnListItemSelected));
	this->Connect(ID_LIST, wxEVT_LIST_ITEM_DESELECTED, wxListEventHandler(EditorAboutPanel::OnListItemDeselected));
	this->Connect(ID_LIST, wxEVT_LIST_ITEM_ACTIVATED, wxListEventHandler(EditorAboutPanel::OnActiveItem));

	this->buttonEdit->Disable();
	this->buttonRemove->Disable();
	this->ClearItens();
	vbox->Add(new wxStaticText(this, -1, wxT("Comprimento da Janela")), 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);
	vbox->Add(this->w_width, 0, wxLEFT | wxRIGHT, 10);

	vbox->Add(new wxStaticText(this, -1, wxT("Largura da Janela")), 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);
	vbox->Add(this->w_height, 0, wxLEFT | wxRIGHT, 10);

	vbox->Add(this->w_fullscreen, 0 , wxALL, 10);
	vbox->Add(new wxStaticText(this, -1, wxT("Selecione arquivos (fbx, obj) do Cen�rio para abrir no Editor:")), 0, wxEXPAND | wxALL, 10);
	vbox->Add(new wxStaticText(this, -1, wxT("Objetos 3D habilitados no Mapa abaixo")), 0, wxEXPAND | wxALL, 10);
	vbox->Add(list_control, 0, wxLEFT | wxRIGHT | wxEXPAND, 10);
	vbox->Add(buttonsList, 0, wxLEFT | wxRIGHT, 10);
	vbox->AddStretchSpacer();
	vbox->Add(button, 0, wxBOTTOM | wxRIGHT | wxALIGN_RIGHT, 20);

	this->SetSizer(vbox);
}

void EditorAboutPanel::OpenEditor(wxCommandEvent & WXUNUSED(event)) {
	this->m_parent->Hide();
	int width = this->w_width->GetValue();
	int height = this->w_height->GetValue();
	bool fullscreen = this->w_fullscreen->GetValue();
	initEditorMenu(this->m_parent, this->dataImported);
	std::thread t(&initEditor, this->m_parent, this->dataImported, width, height, fullscreen);
	t.detach();
	//this->m_parent->Show();
}

void EditorAboutPanel::OnListItemDeselected(wxListEvent &event) {
	this->buttonEdit->Disable();
	this->buttonRemove->Disable();
}

void EditorAboutPanel::OnListItemSelected(wxListEvent & event) {
	this->buttonEdit->Enable();
	this->buttonRemove->Enable();
}

void EditorAboutPanel::OnActiveItem(wxListEvent & event) {
	this->EditSelectedItem();
}

void EditorAboutPanel::OnClearItem(wxCommandEvent& event) {
	this->ClearItens();
}

void EditorAboutPanel::OnRemoveItem(wxCommandEvent & WXUNUSED(event)) {

	long item = -1;
	item = this->list_control->GetNextItem(item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	dataImport data = this->dataImported.at(item);

	wxString mensagem = wxString("Deseja realmente desvincular '") + data.filename + wxString("' ?");

	wxMessageDialog *dial = new wxMessageDialog(NULL, mensagem, wxT("Desvincular Objeto?"), wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION);

	int ret = dial->ShowModal();
	dial->Destroy();

	if (ret == wxID_YES) {
		this->list_control->DeleteItem(item);
		this->dataImported.erase(this->dataImported.begin() + item);
	}
}

void EditorAboutPanel::OnEditItem(wxCommandEvent & WXUNUSED(event)) {
	this->EditSelectedItem();
}

void EditorAboutPanel::OpenImportDialog(wxCommandEvent & event) {
	this->NewItem();
}

void EditorAboutPanel::ImportNewItem(dataImport data) {
	long index = this->list_control->GetItemCount();
	dataImported.push_back(data);
	wxListItem item;
	item.SetId(index);

	this->list_control->InsertItem(item);
	this->setItemList(index, data);
}

void EditorAboutPanel::setItemList(long index, dataImport data) {
	this->list_control->SetItem(index, INDEX_COLUMN_FILENAME, data.filename);
}

void EditorAboutPanel::NewItem() {
	ImportDialog *importDialog = new ImportDialog(wxT("Importar Objeto 3D"));
	importDialog->ShowModal();

	if (importDialog->confirmAction) {
		dataImport data = importDialog->getData();

		wxString *error = this->filter(data, true);

		while (error != nullptr) {
			wxMessageDialog *dial = new wxMessageDialog(NULL, *error, wxT("Entrada Invalida"), wxOK | wxICON_EXCLAMATION);
			dial->ShowModal();
			importDialog->ShowModal();
			if (importDialog->confirmAction) {
				data = importDialog->getData();
				free(error);
				error = this->filter(data, true);
			} else {
				importDialog->Destroy();
				break;
			}
		}
		if (error == nullptr) {
			importDialog->Destroy();
			this->ImportNewItem(data);
		}
	}
}

void EditorAboutPanel::EditSelectedItem() {
	ImportDialog *importDialog = new ImportDialog(wxT("Alterando Item"));

	long index = -1;
	index = this->list_control->GetNextItem(index, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);

	dataImport data = this->dataImported.at(index);

	importDialog->setData(data);
	importDialog->ShowModal();

	if (importDialog->confirmAction) {
		dataImport data = importDialog->getData();

		wxString *error = this->filter(data, false);

		while (error != nullptr) {
			wxMessageDialog *dial = new wxMessageDialog(NULL, *error, wxT("Entrada Invalida"), wxOK | wxICON_EXCLAMATION);
			dial->ShowModal();
			importDialog->ShowModal();
			if (importDialog->confirmAction) {
				data = importDialog->getData();
				free(error);
				error = this->filter(data, false);
			} else {
				importDialog->Destroy();
				break;
			}
		}

		if (error == nullptr) {
			importDialog->Destroy();
			this->dataImported[index] = data;
			this->setItemList(index, data);
		}
	}
}

void EditorAboutPanel::ClearItens() {
	this->dataImported.clear();
	this->list_control->ClearAll();
	wxListItem col0;
	col0.SetId(INDEX_COLUMN_FILENAME);
	col0.SetWidth(100);
	col0.SetText(wxT("Arquivo"));
	this->list_control->InsertColumn(INDEX_COLUMN_FILENAME, col0);
}

wxString *EditorAboutPanel::filter(dataImport data, bool notDiscard) {

	wxString fullpath = wxString(data.path) + wxString(data.filename);
	long index = -1;
	index = this->list_control->GetNextItem(index, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	long atual = 0;

	for (dataImport &leitor : this->dataImported) {
		if (index != atual || notDiscard) {
			if ((wxString(leitor.path) + wxString(leitor.filename)) == fullpath) {
				return new wxString("Arquivo j� adicionado");
			}
		}
		atual++;
	}

	return nullptr;
}

void EditorAboutPanel::setMenuInicial(MenuInicial * parent) {
	this->m_parent = parent;
}

ConfigAboutPanel::ConfigAboutPanel(wxPanel* parent) : wxPanel(parent, ID_CONFIG, wxDefaultPosition, wxSize(270, 150), wxBORDER_SUNKEN) {
	this->SetBackgroundColour(wxColor("rgb(20,20,231)"));
}

void ConfigAboutPanel::setMenuInicial(MenuInicial * parent) {
	this->m_parent = parent;
}

/* Menu Inicial */

MenuInicial::MenuInicial(const wxString& title) : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(800, 600)) {
	this->SetIcon(wxIcon("aaaIDI_ICON"));

	this->m_parent = new wxPanel(this, wxID_ANY);

	this->hbox = new wxBoxSizer(wxHORIZONTAL);

	this->appspanel = new SidebarApps(m_parent);
	this->appspanel->setMenuInicial(this);

	this->defaultAboutPanel = new DefaultAboutPanel(m_parent);
	this->defaultAboutPanel->setMenuInicial(this);

	this->editorAboutPanel = new EditorAboutPanel(m_parent);
	this->editorAboutPanel->setMenuInicial(this);

	dataImport defaultMap = dataImport("C:\\Users\\Acer\\source\\repos\\OpenGL Studio\\Maps\\wallstone", "wallstone.fbx", "Map");

	this->editorAboutPanel->ImportNewItem(defaultMap);

	this->configAboutPanel = new ConfigAboutPanel(m_parent);
	this->configAboutPanel->setMenuInicial(this);

	this->editorAboutPanel->Hide();
	this->configAboutPanel->Hide();

	this->hbox->Add(this->appspanel, 1, wxEXPAND | wxALL, 1);
	this->hbox->Add(this->defaultAboutPanel, 1, wxEXPAND | wxALL, 1);
	this->hbox->Add(this->editorAboutPanel, 1, wxEXPAND | wxALL, 1);
	this->hbox->Add(this->configAboutPanel, 1, wxEXPAND | wxALL, 1);

	this->atualPanelOpened_id = ID_DEFAULT;

	m_parent->SetSizer(this->hbox);

	this->menubar = new wxMenuBar;
	this->file = new wxMenu;
	this->file->Append(ID_NOVO, wxT("Novo Projeto"));
	this->file->Append(ID_OPEN, wxT("Abrir Projeto"));
	this->file->Append(ID_SAVE, wxT("Salvar Projeto"));
	this->file->Append(ID_FECHAR, wxT("Fechar Projeto"));

	this->file->AppendSeparator();
	this->file->Append(ID_SAIR, wxT("Sair"));
	this->menubar->Append(file, wxT("Arquivo"));
	this->SetMenuBar(menubar);

	this->Connect(ID_SAIR, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MenuInicial::OnQuit));
	this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MenuInicial::OnClose));

	this->Centre();
}

void MenuInicial::OnQuit(wxCommandEvent & WXUNUSED(event)) {
	this->Close(true);
}

void MenuInicial::OnClose(wxCloseEvent & event) {
	wxMessageDialog *dial = new wxMessageDialog(NULL,
		wxT("Deseja realmente sair?"), wxT("Fechando"),
		wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION);

	int ret = dial->ShowModal();
	dial->Destroy();

	if (ret == wxID_YES) {
		this->Destroy();
	}
}

void MenuInicial::switchPanel(int panel_id) {

	switch (this->atualPanelOpened_id) {
	case ID_CONFIG: this->configAboutPanel->Hide(); break;
	case ID_EDITOR: this->editorAboutPanel->Hide(); break;
	default: this->defaultAboutPanel->Hide();
	}

	switch (panel_id) {
	case ID_CONFIG: this->configAboutPanel->Show(); break;
	case ID_EDITOR: this->editorAboutPanel->Show(); break;
	default: this->defaultAboutPanel->Show();
	}

	this->atualPanelOpened_id = panel_id;

	this->hbox->Layout();
}

void MenuInicial::trigger() {
	this->Show(true);
}

/* APP */

IMPLEMENT_APP(app) //inicializa o programa

bool app::OnInit() {

	wxString directory = wxGetCwd();
	GlobalInterfaceIO::setAtualWorkingFolder(directory.ToStdString());

	MenuInicial *simple = new MenuInicial(wxT("Opengl Studio - Aplicativos"));

	simple->Show(true);

	return true;
}

bool initEditorMenu(MenuInicial * m, std::vector<dataImport> data) {
	manager = new EditorManagerWindows();
	manager->show();

	for (dataImport d : data) {
		manager->editorframe->addImport(&d);
	}

	return true;
}

void initEngine() {
	engine::flushObjects();
	engine::flushItens();
	engine::flagLoad();
}

bool initEditor(MenuInicial* m, std::vector<dataImport> data, int width, int height, bool fullscreen) {
	engineWindow::init();
	GLFWwindow* window;

	if ((window = engineWindow::novaJanela("OpenGL Studio", width, height, fullscreen)) != nullptr) {
		engine::initCmdCtrl();
		engine::initCam(0.0f, 2.5f, 0.0f);
		engine::initAmbience();
		engine::compileShaders();
		engine::initPhysicsEngine();
		engine::loadingRender();
		engine::finishScene();
		for (dataImport d : data) {
			engine::pushObject(d);
		}
		const char *path = "C:\\Users\\Acer\\source\\repos\\OpenGL Studio\\Maps\\objetos";
		engine::pushItens(dataImport(path, "ump.fbx", "SMG UMP"));
		engine::pushItens(dataImport(path, "m24.fbx", "SMG M24"));
		engine::pushItens(dataImport(path, "shotgun.fbx", "SHOTGUN"));
		engine::pushItens(dataImport(path, "m4a1.fbx", "RIFLE M4A1"));
		engine::pushItens(dataImport(path, "pistol.fbx", "DEAGLE"));

		std::thread t(&initEngine);
		t.detach();

		engineWindow::loop();
		engineWindow::end();
		manager->destroy();
		delete manager;
		manager = nullptr;
		m->Show();
	} else {
		return false;
	}
	return true;
}

