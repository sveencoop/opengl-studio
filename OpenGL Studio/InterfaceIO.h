#pragma once
#include "Light.h"
#include "Obj3D.h"
#include "Fog.h"
#include "Sun.h"
#include <vector>

enum message_type {
	LOG = 0,
	POPUP,
	ERRO
};

enum action_type {
	ADD,
	REMOVE,
	EDIT,
	SET
};

enum val_output {
	FPS_COUNTER = 0,
	MOUSE_YAW,
	MOUSE_PITCH
};

class InterfaceIO {
public:
	virtual void echo(const char* mensagem, message_type type) = 0;
};

class InterfaceInfoIO { //engine -> interaface
public:
	virtual void echo(val_output key, const char* value) = 0;
	virtual void echo_action(action_type action, Object3D *object, unsigned int pos) = 0;
	virtual void echo_action(action_type action, std::vector<Light*> lights, unsigned int pos) = 0;
	virtual void echo_action(action_type action, Fog *fog) = 0;
	virtual void echo_action(action_type action, Sun *sun) = 0;
};

namespace GlobalInterfaceIO {

	void setAtualWorkingFolder(std::string path);
	std::string getAtualWorkingFolder(const char *path = "");

	void alert(const char* mensagem);
	void error(const char* mensagem);
	void log(const char* mensagem);

	void addObjectInfo(Object3D* m, unsigned int pos);
	void addLightInfo(std::vector<Light*> lights, unsigned int pos);
	void addFogInfo(Fog *fog);
	void addSunInfo(Sun *sun);
	void setValOutput(val_output key,const char* value);

	void setGlobalIO(InterfaceIO *io);
	void setGlobalInfoIO(InterfaceInfoIO *io);
};

