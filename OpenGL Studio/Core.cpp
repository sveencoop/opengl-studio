#define STB_IMAGE_IMPLEMENTATION
#define _USE_MATH_DEFINES
#define ENGINE_MACRO
#include "Core.h"
#include "InterfaceIO.h"
#include "StringBuilder.h"
#include <thread>

GLFWwindow *engineWindow::window;
InputEvents *engineWindow::controller;
unsigned int engineWindow::w_width;
unsigned int engineWindow::w_height;
float engineWindow::w_ratio;
bool engineWindow::hasFocus;
Trigger *engineWindow::callbackMenu;

using namespace GlobalInterfaceIO;

void engineWindow::init() {

	glfwInit();

	engineWindow::hasFocus = false;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if defined(__APPLE__)
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif;

	glfwSetTime(0);
	glfwWindowHint(GLFW_SAMPLES, 0);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	texture::init();
}

GLFWwindow* engineWindow::novaJanela(char const *title, int width, int height, bool fullscreen) {
	GLFWwindow* newwindow;
	if (fullscreen) {
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		newwindow = glfwCreateWindow(mode->width, mode->height, title, monitor, NULL);
		width = mode->width;
		height = mode->height;
	} else {
		newwindow = glfwCreateWindow(width, height, title, NULL, NULL);
	}

	glfwSetWindowPos(newwindow, 500, 30);

	if (newwindow == NULL) {
		log("Failed to create GLFW window");
		glfwTerminate();
		return nullptr;
	}

	glfwMakeContextCurrent(newwindow);

	GLADloadproc a = (GLADloadproc)glfwGetProcAddress;

	if (!gladLoadGLLoader(a))
		throw std::runtime_error("Could not initialize GLAD!");
	glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM

	engineWindow::window = newwindow;

	glfwSetFramebufferSizeCallback(newwindow, engineWindow::framebuffer_size_callback);
	glfwSetErrorCallback(engineWindow::error_callback);

	engineWindow::w_width = width;
	engineWindow::w_height = height;
	engineWindow::w_ratio = ((float)width) / ((float)height);
	engineWindow::initEventListeners();

	return engineWindow::window;
}

void engineWindow::error_callback(int error_code, const char* description) {
	error(StringBuilder::concat("OpenglStudio Error:", description));
}

void engineWindow::loop() {
	double lastTime = glfwGetTime();

	while (!glfwWindowShouldClose(engineWindow::window)) {
		double timeValue = glfwGetTime();
		double interval = timeValue - lastTime;
		lastTime = timeValue;

		if (engine::cmdCtrl->lock) {
			engine::cmdCtrl->run();
		}
		engineWindow::controller->applyKeyboard(interval);
		engine::render->draw(interval, timeValue);
		glfwSwapBuffers(engineWindow::window);
		glfwWaitEventsTimeout(0.015);
	}
}

void engineWindow::force_close() {
	glfwSetWindowShouldClose(engineWindow::window, GLFW_TRUE);
}

void engineWindow::end() {
	engine::freeShaders();
	engine::freeBuffers();
	engine::freeTextures();
	engine::clearVectors();
	engine::close();
	glfwTerminate();
}

void engineWindow::framebuffer_size_callback(GLFWwindow *usethatwindow, int width, int height) {
	glViewport(0, 0, width, height);
}

void engineWindow::setTitle(std::string title) {
	glfwSetWindowTitle(engineWindow::window, title.c_str());
}

void engineWindow::initEventListeners() {
	engineWindow::controller = new InputEvents();
	engineWindow::controller->setWindow(engineWindow::window, engineWindow::w_width, engineWindow::w_height);
	glfwSetCursorPosCallback(window, engineWindow::callbackMouse);
	glfwSetCharCallback(window, engineWindow::callbackChar);
	glfwSetKeyCallback(window, engineWindow::callbackKey);
	glfwSetMouseButtonCallback(window, engineWindow::callbackClick);
}

void engineWindow::setCallbackMenu(Trigger *callback) {
	engineWindow::callbackMenu = callback;
}

void engineWindow::callbackMouse(GLFWwindow* usethatwindow, double xpos, double ypos) {
	if (engineWindow::hasFocus) {
		engineWindow::controller->applyMouse(xpos, ypos);
	}
}

void engineWindow::callbackChar(GLFWwindow* usethatwindow, unsigned int code) {
	engineWindow::controller->applyCharTyped(code);
}

void engineWindow::callbackKey(GLFWwindow* usethatwindow, int key, int scancode, int action, int mods) {
	engineWindow::controller->applyKeyTyped(key, scancode, action, mods);
}

void engineWindow::callbackClick(GLFWwindow* usethatwindow, int button, int action, int mods) {

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		double xpos, ypos;
		if (engineWindow::hasFocus) {
			xpos = engineWindow::w_width / 2;
			ypos = engineWindow::w_height / 2;
		} else {
			glfwGetCursorPos(usethatwindow, &xpos, &ypos);
		}
		Replica *obj = engine::pickObject(xpos, ypos);
		if (obj != nullptr) {
			dBodyID body = obj->rigid_body;
			if (body != nullptr) {
				const dReal *linear_vel = dBodyGetLinearVel(body);
				glm::vec3 front = engine::cam->getFront() * 10.0f;
				dBodySetLinearVel(body, linear_vel[0] + front.x, linear_vel[1] + front.y, linear_vel[2] + front.z);
			}
		}
	}

	if (engineWindow::hasFocus) {
		engineWindow::controller->applyClick(button, action, mods);
	} else {
		//openglWindow::focus();
	}
}

void engineWindow::blur() {
	if (engineWindow::hasFocus) {
		glfwSetInputMode(engineWindow::window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		engineWindow::hasFocus = false;
	}
}

void engineWindow::focus() {
	if (!engineWindow::hasFocus) {
		glfwSetInputMode(engineWindow::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		engineWindow::hasFocus = true;
	}
}

unsigned int engine::outline_shader;
unsigned int engine::boundingBox_shader;

std::vector<Mesh*> engine::toFlushGL;
std::vector<TextureImage*> engine::toFlush;
std::vector<Object3D*> engine::objects;
std::vector<Object3D*> engine::hand_itens;
std::vector<Replica*> engine::allreplicas;
std::vector<dataImport> engine::filesToLoad;
std::vector<dataImport> engine::itensToLoad;

Object3D *engine::selected_hand_item = nullptr;
Spotlight engine::flashlight;
Camera *engine::cam = nullptr;
Sun *engine::sun = nullptr;
ScreenEngine *engine::screen = nullptr;
CubeEnviroment *engine::cubemap = nullptr;
LightsVector *engine::lights = nullptr;
Fog *engine::fog = nullptr;

Render *engine::render = nullptr;
ModuloGame *engine::moduloGame = nullptr;

CmdCtrl *engine::cmdCtrl = nullptr;
float engine::loading;
bool engine::cullface = true;
bool engine::normalmapping = true;
AnimCtrl *engine::swing_item = nullptr;
AnimCtrl *engine::shoot_item = nullptr;
glm::vec3 engine::recoil_aim;

void engine::initAmbience() {
	engine::outline_shader = shader::compileShader("../OpenGL Studio/Shaders/outline/test.vsh", "../OpenGL Studio/Shaders/outline/test.fsh");
	engine::boundingBox_shader = shader::compileShader("../OpenGL Studio/Shaders/colorPicking/test.vsh", "../OpenGL Studio/Shaders/colorPicking/test.fsh");
	engine::flashlight.init();
	engine::sun = new Sun();
	engine::sun->init();
	engine::fog = new Fog();
	engine::lights = new LightsVector();
	engine::lights->totalPointlight = 0;
	engine::lights->totalSpotlight = 0;
#ifdef ENGINE_MACRO
	addFogInfo(engine::fog);
	addSunInfo(engine::sun);
#endif
}

void engine::initCam(float x, float y, float z) {
	engine::cam = new FPSWalk();
	engine::cam->setWindow(engineWindow::w_width, engineWindow::w_height);
	engine::cam->init(x, y, z, 78.0f, 0.1f, 400.0f);
	engineWindow::controller->bindCamera(cam);
	engine::cam->compile();

	engine::swing_item = new AnimCtrl(engine::animation_hand_item, true, 2 * M_PI);
	engine::shoot_item = new AnimCtrl(engine::animation_shoot, false, M_PI / 10.0f);
	engine::shoot_item->setLockedTime(M_PI / 20.0f);
}

void engine::initCmdCtrl() {
	engine::cmdCtrl = new CmdCtrl();
	engine::cmdCtrl->registerF("flush_to_gl", engine::flushToGL);
	engine::cmdCtrl->registerF("cullface", engine::enableCullface);
	engine::cmdCtrl->registerF("update_game", engine::updateParams);
	engine::cmdCtrl->registerF("update_screen", engine::updateParamsScreen);
}

void engine::enableCullface(std::map<std::string, std::string> params) {
	bool enable;

	std::map<std::string, std::string>::iterator it = params.find("enable");

	if (it != params.end()) {
		enable = (it->second == "true" ? true : false);
	} else {
		enable = !engine::cullface;
	}

	if (enable) {
		glEnable(GL_CULL_FACE);
	} else {
		glDisable(GL_CULL_FACE);
	}
	engine::cullface = enable;
}

void engine::cmd(const char * command) {
	std::map<std::string, std::string> params;
	engine::cmdCtrl->add(command, params);
	engine::cmdCtrl->lock = true;
}

void engine::toggleFlyMode() {
	glm::vec3 pos = engine::cam->getPosition();
	float yaw = engine::cam->getYaw();
	float pitch = engine::cam->getPitch();
	Modulo_Key mode = engine::cam->getMode();
	delete engine::cam;

	float angle = 90;//vai ser descartado de qualquer forma
	if (mode == fly_mode) {
		engine::cam = new FPSWalk();
	} else {
		engine::cam = new FPSNoclipWalk();
	}
	engine::cam->setWindow(engineWindow::w_width, engineWindow::w_height);
	engine::cam->init(pos.x, pos.y, pos.z, angle, 0.1f, 400.0f);
	engine::cam->setYaw(yaw);
	engine::cam->setPitch(pitch);
	engineWindow::controller->bindCamera(cam);
	engine::cam->compile();
}

void engine::toggleFlashlight() {
	engine::flashlight.toggleIntensity();
}

void engine::toggleFog() {
	engine::fog->enabled = 1 - engine::fog->enabled;
}

void engine::compileShaders() {
}

void engine::flushToGL(std::map<std::string, std::string> params) {
	for (TextureImage *t : engine::toFlush) {
		t->flushToGL(GL_REPEAT);
	}

	for (Mesh *m : engine::toFlushGL) {
		m->flushTextures();
		m->compileVertexData();
	}

	for (TextureImage *t : engine::toFlush) {
		delete t;
	}

	engine::toFlush.clear();
	engine::toFlushGL.clear();

	engine::cubemap = new CubeEnviroment(1024, glm::vec3(1.5, 4.0, 3.0));
	engine::renderCubemap(engine::cubemap);

	engine::sceneRender();

	for (Object3D *obj : engine::objects) {
		PhysicsEngine::registerObject(obj);
	}

	std::thread t(PhysicsEngine::enableGravity);
	t.detach();
}

void engine::updateParams(std::map<std::string, std::string> params) {
	engine::moduloGame->apply();
}

void engine::updateParamsScreen(std::map<std::string, std::string> params) {
	engine::screen->use();
	engine::screen->apply();
}

void engine::flagLoad() {
	engine::cmdCtrl->lock = true;
}

Replica* engine::pickObject(float x, float y) {

	if (x == -1 || y == -1) {
		x = engineWindow::w_width / 2;
		y = engineWindow::w_height / 2;
	}

	Render *r = new drawBoundingBox();

	r->draw(0, 0);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned char data[4];
	glReadPixels(x, 600 - y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	int pickedID = data[0] + data[1] * 256 + data[2] * 256 * 256 - 1;

	engine::clearHightlightObject();

	if (pickedID != -1 && pickedID < engine::allreplicas.size()) {
		return engine::allreplicas[pickedID];
	}
	return nullptr;
}

void engine::flushObjects() {
	engine::loading = 0;
	float total = engine::filesToLoad.size();
	float count = 0;
	for (dataImport &data : engine::filesToLoad) {
		LoaderMesh *loader;
		std::string temp_path = data.path + std::string("\\");
		loader = new LoaderMesh(temp_path.c_str(), data.filename.c_str());

		double time = glfwGetTime();

		log(std::string(std::string("Loading ") + data.filename).c_str());

		loader->loader(engine::toFlushGL);

		loader->flushTextures(engine::toFlush);

		for (instDataImport inst : data.insts) {
			for (Light *l : loader->lights) {
				if (l->getType() == "Luz Direcional") {
					Spotlight *s = (Spotlight*)l;
					s->scale(inst.scale);
					s->translate(inst.translate);
					s->initAttrs(engine::lights->totalSpotlight);
					engine::lights->totalSpotlight++;
				} else if (l->getType() == "Ponto de Luz") {
					Pointlight *p = (Pointlight*)l;
					p->scale(inst.scale);
					p->translate(inst.translate);
					p->initAttrs(engine::lights->totalPointlight);
					engine::lights->totalPointlight++;
				}
				engine::lights->list.push_back(l);
			}
		}

		for (Object3D *obj : loader->objects) {
			obj->dataImported = data;

			for (instDataImport inst : data.insts) {
				obj->addReplica(inst);
			}
			engine::objects.push_back(obj);

			for (Replica *rep : obj->replicas) {
				engine::allreplicas.push_back(rep);
				int countObj = engine::allreplicas.size();
				int r = (countObj & 0x000000FF) >> 0;
				int g = (countObj & 0x0000FF00) >> 8;
				int b = (countObj & 0x00FF0000) >> 16;
				rep->setIdColor(r / 255.0, g / 255.0, b / 255.0);
			}
#ifdef ENGINE_MACRO
			addObjectInfo(obj, count);
#endif
		}
#ifdef ENGINE_MACRO
		addLightInfo(loader->lights, count);
#endif
		double delta = glfwGetTime() - time;

		log(std::string(std::string("Loaded - Total time: ") + std::to_string(delta) + std::string(" seconds")).c_str());

		count++;
		engine::loading = count / total;

	}
	engine::filesToLoad.clear();
	std::map<std::string, std::string> params;
	engine::cmdCtrl->add("flush_to_gl", params);
}

void engine::flushItens() {
	engine::loading = 0;
	float total = engine::filesToLoad.size();
	float count = 0;
	for (dataImport &data : engine::itensToLoad) {
		LoaderMesh *loader;
		std::string temp_path = data.path + std::string("\\");
		loader = new LoaderMesh(temp_path.c_str(), data.filename.c_str());

		double time = glfwGetTime();

		log(std::string(std::string("Loading ") + data.filename).c_str());

		loader->loader(engine::toFlushGL);

		loader->flushTextures(engine::toFlush);

		for (Object3D *obj : loader->objects) {
			obj->dataImported = data;
			//obj->translateTransform(glm::vec3(-0.6, -1.0, 1.0));
			for (instDataImport inst : data.insts) {
				Replica *r = obj->addReplica(inst);
			}

			engine::hand_itens.push_back(obj);

		}
		double delta = glfwGetTime() - time;

		log(std::string(std::string("Loaded - Total time: ") + std::to_string(delta) + std::string(" seconds")).c_str());

		count++;
		engine::loading = count / total;

	}
	engine::selected_hand_item = engine::hand_itens.at(0);
	engine::itensToLoad.clear();
	std::map<std::string, std::string> params;
	engine::cmdCtrl->add("flush_to_gl", params);
}

void engine::pushObject(dataImport importData) {
	engine::filesToLoad.push_back(importData);
}

void engine::pushItens(dataImport importData) {
	engine::itensToLoad.push_back(importData);
}

void engine::clearHightlightObject() {
	for (Object3D *obj : engine::objects) {
		obj->isOutlined = false;
	}
}

void engine::animation_hand_item(double value) {
	glm::mat4 matriz = glm::mat4(1.0);

	if (engineWindow::controller->second_button) {
		matriz = glm::translate(matriz, glm::vec3(-0.5f, 0.56f, -0.407f));
		matriz = glm::rotate(matriz, -0.055f, glm::vec3(0, 1, 0));
	} else if (engineWindow::controller->moving) {
		double acc = sin(value*(engineWindow::controller->running ? 14 : 8)) * 0.05;
		matriz = glm::translate(matriz, glm::vec3(-0.33f, -1 * abs(acc), acc));
		matriz = glm::rotate(matriz, (engineWindow::controller->running ? 0.46f : 0.23f), glm::vec3(0, 1, 0));
		matriz = glm::rotate(matriz, -0.05f, glm::vec3(0, 0, 1));
	} else {
		if (engineWindow::controller->strafing) {
			double acc = sin(value * 6)*0.1;
			matriz = glm::translate(matriz, glm::vec3(0.0, -1 * abs(acc), 0));
			matriz = glm::rotate(matriz, -0.05f, glm::vec3(0, 0, 1));
		} else if (engineWindow::controller->backward) {
			double acc = sin(value * 6)*0.1;
			matriz = glm::translate(matriz, glm::vec3(-0.5, -1 * abs(acc), 0));
			matriz = glm::rotate(matriz, 0.20f, glm::vec3(0, 0, 1));
		} else {
			double acc = sin(value) * 0.05;
			matriz = glm::translate(matriz, glm::vec3(0.0, -acc, 0.2 + acc));
			matriz = glm::rotate(matriz, -0.05f, glm::vec3(0, 0, 1));
		}
	}
	for (Replica *r : engine::selected_hand_item->replicas) {
		r->setAdictionalMatriz(matriz);
	}
}

void engine::next_hand_item() {
	bool find = false;
	int total = engine::hand_itens.size();
	for (int i = 0; i < total * 2; ++i) {
		Object3D *o = engine::hand_itens.at(i % total);
		if (find) {
			engine::selected_hand_item = o;
			break;
		}
		if (o == engine::selected_hand_item) {
			find = true;
		}
	}
}

void engine::animation_shoot(double value) {
	glm::mat4 matriz = glm::mat4(1.0);
	double acc = cos(value*5.0);

	if (engineWindow::controller->second_button) {
		matriz = glm::translate(matriz, glm::vec3(-0.5f - acc * 0.1, 0.56f, -0.407f));
		matriz = glm::rotate(matriz, (float)(engine::recoil_aim.y*0.01 - 0.055f), glm::vec3(0, 1, 0));
		matriz = glm::rotate(matriz, (float)(acc*0.05 + engine::recoil_aim.z*0.01), glm::vec3(0, 0, 1));
	} else {
		matriz = glm::translate(matriz, glm::vec3(-acc * 0.2, 0.0, 0.2));
		matriz = glm::rotate(matriz, (float)(engine::recoil_aim.y*0.05), glm::vec3(0, 1, 0));
		matriz = glm::rotate(matriz, (float)(acc*0.1 + engine::recoil_aim.z*0.05), glm::vec3(0, 0, 1));
	}

	for (Replica *r : engine::selected_hand_item->replicas) {
		r->setAdictionalMatriz(matriz);
	}
}

void engine::trigger_shoot() {
	if (!engine::shoot_item->isLocked()) {
		engine::recoil_aim = glm::vec3(0.0, (rand()%10000)/10000.0f - 0.5f, (rand() % 10000) / 10000.0f - 0.5f);
		engine::shoot_item->start(glfwGetTime());
	}
}

void engine::initPhysicsEngine() {
	PhysicsEngine::init();
}

void engine::finishScene() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	//glEnable(GL_MULTISAMPLE);

	engine::screen = new ScreenEngine(engineWindow::w_width, engineWindow::w_height);
	engine::screen->addImageComponent("..\\OpenGL Studio\\Textures\\crosshair.png", 0.5, 0.5, 10, 10);
}

void engine::loadingRender() {
	engine::render = new drawLoading();
}

void engine::sceneRender() {
	engine::moduloGame = new ModuloGame();
	engine::render = engine::moduloGame;
	engine::moduloGame->shaderingData();
}

void engine::renderCubemap(CubeEnviroment *usethatcubemap) {

	Camera *camera = new NoWalk();
	unsigned int size = usethatcubemap->size;
	camera->setWindow(size, size);
	glm::vec3 p = engine::cam->getPosition();
	camera->init(p.x, p.y - 5.0, p.z, 90.0f, 1.0f, 100.0f);

	glViewport(0, 0, size, size); // Set size of the viewport as size of cube map

	glDisable(GL_STENCIL_TEST);
	glEnable(GL_DEPTH_TEST);
	camera->setOrientation(GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	engine::drawBasic(usethatcubemap->fbo_positive_x, camera);

	camera->setOrientation(GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	engine::drawBasic(usethatcubemap->fbo_negative_x, camera);

	camera->setOrientation(GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	engine::drawBasic(usethatcubemap->fbo_positive_y, camera);

	camera->setOrientation(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	engine::drawBasic(usethatcubemap->fbo_negative_y, camera);

	camera->setOrientation(GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	engine::drawBasic(usethatcubemap->fbo_positive_z, camera);

	camera->setOrientation(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
	engine::drawBasic(usethatcubemap->fbo_negative_z, camera);

	glViewport(0, 0, engineWindow::w_width, engineWindow::w_height);
	glEnable(GL_STENCIL_TEST);

	free(camera);
}

void engine::drawBasic(unsigned int fbo, Camera *usethatcam) {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glClearColor(0.25f, 0.25f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	engine::drawBasicScene(usethatcam);
}

void engine::drawBasicScene(Camera *usethatcam) {
	//usethatcam->calcFinalCam();

	//glm::mat4 view = usethatcam->getView();
	//glm::mat4 projection = usethatcam->getProjection();
	//glm::vec3 posCam = usethatcam->getPosition();
	//glm::vec3 frontCam = usethatcam->getFront();

	//glm::mat4 view_projection = projection * view;

	//for (Object3D *obj : engine::objects) {
	//	unsigned int programme = obj->shader_programme;

	//	for (Replica *replica : obj->replicas) {
	//		glm::mat4 trans = replica->getTransformationMatrix();

	//		for (Mesh *m : obj->meshes) {
	//			unsigned int vao = m->vao;
	//			glUseProgram(programme);
	//			glBindVertexArray(vao);

	//			openglUtils::setMatriz(programme, "view_projection", glm::value_ptr(view_projection));
	//			openglUtils::setMatriz(programme, "transform", glm::value_ptr(trans));

	//			if (m->textureDiffuse != -1) {

	//				openglUtils::setTexture(0, m->textureDiffuse);
	//				if (m->textureSpecular != -1) {
	//					openglUtils::setInt(programme, "material.specmap", 0);
	//				} else {
	//					openglUtils::setTexture(1, m->textureSpecular);
	//				}
	//				if (m->textureReflection != -1) {
	//					openglUtils::setTexture(2, m->textureReflection);
	//				}

	//				glActiveTexture(GL_TEXTURE4);
	//				glBindTexture(GL_TEXTURE_CUBE_MAP, engine::skybox->texture);
	//				openglUtils::setInt(programme, "cubemap", 4);

	//				openglUtils::setVec3(programme, "viewPos", glm::value_ptr(posCam));

	//				openglUtils::setInt(programme, "NR_POINT_LIGHTS", engine::lights->totalPointlight);
	//				openglUtils::setInt(programme, "NR_SPOT_LIGHTS", engine::lights->totalSpotlight);
	//				for (Light *elem : engine::lights->list) {
	//					elem->apply();
	//				}

	//				engine::sun->apply();
	//				engine::flashlight.apply(glm::value_ptr(posCam), glm::value_ptr(frontCam));

	//				openglUtils::setFloat(programme, "material.reflect", 0.0);
	//				openglUtils::setFloat(programme, "material.IOR", 0.0);
	//				openglUtils::setFloat(programme, "material.shininess", m->shininess);
	//			}

	//			glDrawElements(GL_TRIANGLES, m->indices.size(), GL_UNSIGNED_INT, 0);
	//		}
	//	}
	//}

	//engine::skybox->render(view, projection, posCam);
}

void engine::freeShaders() {

	engine::moduloGame->freeShaders();

	shader::freeShader(engine::outline_shader);
	shader::freeShader(engine::boundingBox_shader);

	engine::screen->freeShaders();

}

void engine::freeBuffers() {
	glDeleteFramebuffers(1, &engine::cubemap->fbo_negative_x);
	glDeleteFramebuffers(1, &engine::cubemap->fbo_positive_x);
	glDeleteFramebuffers(1, &engine::cubemap->fbo_negative_y);
	glDeleteFramebuffers(1, &engine::cubemap->fbo_positive_y);
	glDeleteFramebuffers(1, &engine::cubemap->fbo_negative_z);
	glDeleteFramebuffers(1, &engine::cubemap->fbo_positive_z);
	engine::moduloGame->freeBuffers();
	engine::screen->freeBuffers();
}

void engine::freeTextures() {
	texture::freeTexturesCompiled();
}

void engine::clearVectors() {

	delete engine::selected_hand_item;

	engine::objects.clear();
	engine::hand_itens.clear();
	engine::lights->list.clear();
	engine::allreplicas.clear();
	engine::lights->totalPointlight = 0;
	engine::lights->totalSpotlight = 0;
	//engine::RgdMap.clear();

	delete engine::lights;
	delete engine::sun;
	delete engine::cam;
	delete engine::screen;
	delete engine::cubemap;
	delete engine::fog;

	if (engine::moduloGame == engine::render) {
		delete engine::moduloGame;
	} else {
		delete engine::render;
	}

	engine::lights = nullptr;
	engine::sun = nullptr;
	engine::cam = nullptr;
	engine::screen = nullptr;
	engine::cubemap = nullptr;
	engine::fog = nullptr;
	engine::render = nullptr;
	engine::moduloGame = nullptr;
	engine::selected_hand_item = nullptr;
}

void engine::close() {
	PhysicsEngine::finish();
	engine::swing_item->end();
}

//DESENHA O CEN�RIO
void ModuloGame::draw(double interval, double time) {
	if (engine::shoot_item->isRunning()) {
		engine::shoot_item->trigger(time);
		if (!engine::shoot_item->isLocked() && engineWindow::controller->shooting) {
			engine::trigger_shoot();
		}
	} else {
		engine::swing_item->trigger(time);
	}
	this->drawNoDeferred(interval);
}

void ModuloGame::drawNoDeferred(double interval) {
	//openglDraw::renderCubemap(openglDraw::cubemap);
	//render screen
	PhysicsEngine::loop(interval);

	//TO SHADOWMAPPING
	glEnable(GL_DEPTH_TEST);
	this->updateShadowMapping();

	//SCREENOBJ
	glBindFramebuffer(GL_FRAMEBUFFER, engine::screen->id_framebuffer_to_screen);
	glEnable(GL_STENCIL_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glStencilFunc(GL_ALWAYS, 1, 0xFF);//PASSA SE FOR 0X00, COM REFER�NCIA EM 1
	glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);//PASSANDO NO TESTE MUDA PARA O REF DO FUNC = 1
	glStencilMask(0xFF);
	engine::cam->calcFinalCam();

	this->drawHandItem();

	glStencilFunc(GL_NOTEQUAL, 1, 0xFF); // Pass test if stencil value is 1
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);//PASSANDO NO TESTE MUDA PARA O REF DO FUNC = 1

	this->drawScene();

	//WINDOW
	engine::screen->render();
}

void ModuloGame::drawDeferred(double interval) {
	PhysicsEngine::loop(interval);

	//TO SHADOWMAPPING
	glEnable(GL_DEPTH_TEST);
	this->updateShadowMapping();

	//GBUFFER
	glBindFramebuffer(GL_FRAMEBUFFER, this->fbo_deferred);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	this->gBuffering();

	//SCREENOBJ + DEFERRED
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glBindFramebuffer(GL_FRAMEBUFFER, engine::screen->id_framebuffer_to_screen);
	glUseProgram(this->deferred_shader->id_deffered_program);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->texture_positions);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->texture_normals);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, this->textures_albedoSpec);

	glBindVertexArray(engine::screen->id_quad_object);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	//WINDOW
	engine::screen->render();
}

ModuloGame::ModuloGame() {
	this->game_shader = new GameShader();
	this->deferred_shader = new DeferredShader();
	this->shadow_mapping = new ShadowMapping(10240);
	this->skybox = new Skybox();
	this->skybox->load();
	this->skybox->flushToGL();

	this->fbo_deferred = texture::createFrameBuffer();
	glBindFramebuffer(GL_FRAMEBUFFER, this->fbo_deferred);
	unsigned int *texture = texture::createFrameBufferTextureDeferred(engineWindow::w_width, engineWindow::w_height, this->fbo_deferred);
	this->texture_positions = texture[0];
	this->texture_normals = texture[1];
	this->textures_albedoSpec = texture[2];

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	engine::swing_item->start(glfwGetTime());
}

ModuloGame::~ModuloGame() {

}

void ModuloGame::gBuffering() {

	engine::cam->calcFinalCam();

	glm::mat4 view = engine::cam->getView();
	glm::mat4 projection = engine::cam->getProjection();
	glm::vec3 posCam = engine::cam->getPosition();
	glm::vec3 frontCam = engine::cam->getFront();

	glm::mat4 view_projection = projection * view;

	float *view_projectionPtr = glm::value_ptr(view_projection);
	float *frontCamPtr = glm::value_ptr(frontCam);

	glUseProgram(this->deferred_shader->id_gbuffer_program);
	glUniformMatrix4fv(this->deferred_shader->view_projection, 1, GL_FALSE, view_projectionPtr);

	for (Object3D *obj : engine::objects) {
		for (Mesh *mesh : obj->meshes) {
			glBindVertexArray(mesh->vao);

			if (mesh->material->textureDiffuse != -1) {
				//FLAG TEXTURE
				glUniform1i(this->deferred_shader->material.withTexture, 1);
				//DIFUSE TEXTURE
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, mesh->material->textureDiffuse);
				//SPECULAR MAP
				if (mesh->material->textureSpecular != -1) {
					glUniform1i(this->deferred_shader->material.specmap, 0);
				} else {
					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D, mesh->material->textureSpecular);
				}
				//NORMALMAPPING 
				if (mesh->material->textureNormal != -1 && engine::normalmapping) {
					glActiveTexture(GL_TEXTURE2);
					glBindTexture(GL_TEXTURE_2D, mesh->material->textureNormal);
					glUniform1i(this->deferred_shader->material.hasNormal, 1);
				} else {
					glUniform1i(this->deferred_shader->material.hasNormal, 0);
				}
			} else {
				//FLAG TEXTURE
				glUniform1i(this->game_shader->material.withTexture, 0);
			}

			for (Replica *r : obj->replicas) {
				glUniformMatrix4fv(this->game_shader->transform, 1, GL_FALSE, glm::value_ptr(r->getTransformationMatrix()));
				glDrawElements(GL_TRIANGLES, mesh->indices.size(), GL_UNSIGNED_INT, 0);
			}
		}
	}
	this->skybox->render(view, projection, posCam);
}


void ModuloGame::drawScene() {

	double time = glfwGetTime();

	glm::mat4 view = engine::cam->getView();
	glm::mat4 projection = engine::cam->getProjection();
	glm::vec3 posCam = engine::cam->getPosition();
	glm::vec3 frontCam = engine::cam->getFront();

	glm::mat4 view_projection = projection * view;

	float *view_projectionPtr = glm::value_ptr(view_projection);
	float *posCamPtr = glm::value_ptr(posCam);
	float *frontCamPtr = glm::value_ptr(frontCam);

	glUseProgram(this->game_shader->id);
	glUniformMatrix4fv(this->game_shader->view_projection, 1, GL_FALSE, view_projectionPtr);
	glUniform3fv(this->game_shader->viewPos, 1, posCamPtr);

	//glActiveTexture(GL_TEXTURE5);
	//glBindTexture(GL_TEXTURE_CUBE_MAP, engine::cubemap->id_cubemap);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, this->shadow_mapping->texture);

	glUniform1f(this->game_shader->time, time);

	engine::flashlight.apply(posCamPtr, frontCamPtr);

	for (Object3D *obj : engine::objects) {
		for (Mesh *mesh : obj->meshes) {
			glBindVertexArray(mesh->vao);

			if (mesh->material->textureDiffuse != -1) {
				//FLAG TEXTURE
				glUniform1i(this->game_shader->material.withTexture, 1);
				glActiveTexture(GL_TEXTURE0);
				//DIFUSE TEXTURE
				glBindTexture(GL_TEXTURE_2D, mesh->material->textureDiffuse);
				//SPECULAR MAP
				if (mesh->material->textureSpecular != -1) {
					glUniform1i(this->game_shader->material.specmap, 0);
				} else {
					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D, mesh->material->textureSpecular);
				}
				//REFLECTION MAP
				if (mesh->material->textureReflection != -1) {
					glActiveTexture(GL_TEXTURE2);
					glBindTexture(GL_TEXTURE_2D, mesh->material->textureReflection);
				}
				//NORMALMAPPING 
				if (mesh->material->textureNormal != -1 && engine::normalmapping) {
					glActiveTexture(GL_TEXTURE3);
					glBindTexture(GL_TEXTURE_2D, mesh->material->textureNormal);
					glUniform1i(this->game_shader->material.hasNormal, 1);
				} else {
					glUniform1i(this->game_shader->material.hasNormal, 0);
				}
				//EMISSION MAP
				//if (mesh->material->textureEmission != -1) {
				//	glActiveTexture(GL_TEXTURE4);
				//	glBindTexture(GL_TEXTURE_2D, mesh->material->textureEmission);
				//	glUniform1i(this->game_shader->material.hasEmission, 1);
				//} else {
				//	glUniform1i(this->game_shader->material.hasEmission, 0);
				//}
			} else {
				//FLAG TEXTURE
				glUniform1i(this->game_shader->material.withTexture, 0);
			}

			//OTHERS
			glUniform1f(this->game_shader->material.reflect, mesh->material->reflection);
			//glUniform1f(this->game_shader->material.IOR, mesh->material->IOR);
			glUniform1f(this->game_shader->material.shininess, mesh->material->shininess);

			for (Replica *r : obj->replicas) {
				glUniformMatrix4fv(this->game_shader->transform, 1, GL_FALSE, glm::value_ptr(r->getTransformationMatrix()));
				glDrawElements(GL_TRIANGLES, mesh->indices.size(), GL_UNSIGNED_INT, 0);
			}
		}
	}
	this->skybox->render(view, projection, posCam);
}

void ModuloGame::drawHandItem() {

	glm::mat4 view = engine::cam->getView();
	glm::mat4 projection = engine::cam->getProjection();
	glm::vec3 posCam = engine::cam->getPosition();
	glm::vec3 frontCamPtr = engine::cam->getFront();

	glm::vec3 center = posCam + frontCamPtr;

	glm::mat4 view_projection = projection * view;

	glm::vec3 rotation = glm::vec3(0.0);
	rotation.y = -engine::cam->getYaw();
	rotation.z = engine::cam->getPitch();

	float *view_projectionPtr = glm::value_ptr(view_projection);
	float *posCamPtr = glm::value_ptr(posCam);

	glUseProgram(this->game_shader->id);
	glUniformMatrix4fv(this->game_shader->view_projection, 1, GL_FALSE, view_projectionPtr);
	glUniform3fv(this->game_shader->viewPos, 1, posCamPtr);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, this->shadow_mapping->texture);

	engine::flashlight.unset();
	for (Mesh *mesh : engine::selected_hand_item->meshes) {
		glBindVertexArray(mesh->vao);

		if (mesh->material->textureDiffuse != -1) {
			//FLAG TEXTURE
			glUniform1i(this->game_shader->material.withTexture, 1);
			glActiveTexture(GL_TEXTURE0);
			//DIFUSE TEXTURE
			glBindTexture(GL_TEXTURE_2D, mesh->material->textureDiffuse);
			//SPECULAR MAP
			if (mesh->material->textureSpecular != -1) {
				glUniform1i(this->game_shader->material.specmap, 0);
			} else {
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, mesh->material->textureSpecular);
			}
			//NORMALMAPPING 
			if (mesh->material->textureNormal != -1 && engine::normalmapping) {
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, mesh->material->textureNormal);
				glUniform1i(this->game_shader->material.hasNormal, 1);
			} else {
				glUniform1i(this->game_shader->material.hasNormal, 0);
			}
		} else {
			//FLAG TEXTURE
			glUniform1i(this->game_shader->material.withTexture, 0);
		}

		for (Replica *r : engine::selected_hand_item->replicas) {
			r->setPosition(center);
			r->setRotation(rotation);
			r->compileTransform();
			glUniformMatrix4fv(this->game_shader->transform, 1, GL_FALSE, glm::value_ptr(r->getTransformationMatrix()));
			glDrawElements(GL_TRIANGLES, mesh->indices.size(), GL_UNSIGNED_INT, 0);
		}
	}
}

void ModuloGame::updateShadowMapping() {
	this->shadow_mapping->update();
	glViewport(0, 0, engineWindow::w_width, engineWindow::w_height);
}

void ModuloGame::shaderingData() {

	glUseProgram(this->deferred_shader->id_gbuffer_program);
	this->deferred_shader->shadering_data_gbuffer();

	glUseProgram(this->deferred_shader->id_deffered_program);
	this->deferred_shader->shadering_data_deffered();

	unsigned int shader = this->game_shader->id;
	glUseProgram(shader);

	this->game_shader->shadering_data();

	engine::sun->shaderingData(shader);
	engine::flashlight.shaderingDataFlashlight(shader);
	engine::fog->shaderingData(shader);
	for (Light *l : engine::lights->list) {
		l->shaderingData(shader);
		l->apply();
	}
	engine::sun->apply();
	engine::fog->apply();

	glUniform1i(this->game_shader->NR_POINT_LIGHTS, engine::lights->totalPointlight);
	glUniform1i(this->game_shader->NR_SPOT_LIGHTS, engine::lights->totalSpotlight);

	this->shadow_mapping->apply();
}

void ModuloGame::apply() {
	unsigned int shader = this->game_shader->id;
	glUseProgram(shader);
	for (Light *l : engine::lights->list) {
		l->shaderingData(shader);
		l->apply();
	}
	engine::sun->apply();
	engine::fog->apply();

	glUniform1i(this->game_shader->NR_POINT_LIGHTS, engine::lights->totalPointlight);
	glUniform1i(this->game_shader->NR_SPOT_LIGHTS, engine::lights->totalSpotlight);

	this->shadow_mapping->apply();
}

void ModuloGame::freeShaders() {
	shader::freeShader(this->game_shader->id);
	this->skybox->freeShaders();

	delete this->game_shader;
	this->game_shader = nullptr;
}

void ModuloGame::freeBuffers() {
	glDeleteFramebuffers(1, &this->shadow_mapping->fbo);
}

//DESENHA O LOADING

void drawLoading::draw(double interval, double time) {

	double color = (sin(time * 2) + 1) / 8.0;
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_STENCIL_TEST);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.25f, 0.25f, color + 0.25f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(engine::screen->id_shader_loading);
	glBindVertexArray(engine::screen->id_quad_object);

	static unsigned int id_loading = glGetUniformLocation(engine::screen->id_shader_loading, "loading");
	static unsigned int id_time = glGetUniformLocation(engine::screen->id_shader_loading, "time");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, engine::screen->id_texture_loading);
	glUniform1f(id_loading, engine::loading);
	glUniform1f(id_time, time);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

//DESENHA BOUNDING BOX

void drawBoundingBox::draw(double interval, double time) {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	engine::cam->calcFinalCam();

	glm::mat4 view = engine::cam->getView();
	glm::mat4 projection = engine::cam->getProjection();

	glm::mat4 view_projection = projection * view;

	glUseProgram(engine::boundingBox_shader);

	static unsigned int id_vp = glGetUniformLocation(engine::boundingBox_shader, "view_projection");
	static unsigned int id_colour = glGetUniformLocation(engine::boundingBox_shader, "colour");
	static unsigned int id_transform = glGetUniformLocation(engine::boundingBox_shader, "transform");
	glUniformMatrix4fv(id_vp, 1, GL_FALSE, glm::value_ptr(view_projection));
	for (Object3D *obj : engine::objects) {
		for (Replica *r : obj->replicas) {
			glUniformMatrix4fv(id_transform, 1, GL_FALSE, glm::value_ptr(r->getTransformationMatrix()));
			glUniform3fv(id_colour, 1, glm::value_ptr(r->idColor));
			for (Mesh *m : obj->meshes) {
				glBindVertexArray(m->vao);
				glDrawElements(GL_TRIANGLES, m->indices.size(), GL_UNSIGNED_INT, 0);
			}
		}
	}

}

//RENDERS ESPECIAIS

ShadowMapping::ShadowMapping(unsigned int SHADOW_SIZE) {

	this->size = SHADOW_SIZE;
	this->fbo = texture::createFrameBuffer();
	this->texture = texture::createDepthMapTexture(SHADOW_SIZE, SHADOW_SIZE);
	texture::bindFrameBufferDepthMap(this->fbo, this->texture);

	this->shader = shader::compileShader("../OpenGL Studio/Shaders/depth/test.vsh", "../OpenGL Studio/Shaders/depth/test.fsh");

	this->change();
}

void ShadowMapping::apply() {
	glUniformMatrix4fv(engine::moduloGame->game_shader->lightSpaceMatrix, 1, GL_FALSE, glm::value_ptr(this->view_projection));
}

void ShadowMapping::change() {
	float near_plane = 1.0f, far_plane = 800.0f;

	glm::mat4 projection = glm::ortho(-400.0f, 400.0f, -400.0f, 400.0f, near_plane, far_plane);

	glm::vec3 pos = glm::vec3(-engine::sun->direction[0] * 400.0f, -engine::sun->direction[1] * 400.0f, -engine::sun->direction[2] * 400.0f);

	glm::mat4 view = glm::lookAt(pos,
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));

	this->view_projection = projection * view;

}

void ShadowMapping::update() {

	if (engine::cullface) {
		glDisable(GL_CULL_FACE);
	}

	glViewport(0, 0, this->size, this->size);
	glBindFramebuffer(GL_FRAMEBUFFER, this->fbo);
	glClear(GL_DEPTH_BUFFER_BIT);

	glUseProgram(this->shader);

	static unsigned int id_vp = glGetUniformLocation(this->shader, "view_projection");
	static unsigned int id_transform = glGetUniformLocation(this->shader, "transform");
	glUniformMatrix4fv(id_vp, 1, GL_FALSE, glm::value_ptr(this->view_projection));

	for (Object3D *obj : engine::objects) {
		for (Replica *r : obj->replicas) {
			glUniformMatrix4fv(id_transform, 1, GL_FALSE, glm::value_ptr(r->getTransformationMatrix()));
			for (Mesh *m : obj->meshes) {
				glBindVertexArray(m->vao);
				glDrawElements(GL_TRIANGLES, m->indices.size(), GL_UNSIGNED_INT, 0);
			}
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (engine::cullface) {
		glEnable(GL_CULL_FACE);
	}

}

