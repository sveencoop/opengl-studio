#include "EditorManagerWindows.h"

EditorManagerWindows::EditorManagerWindows() {

	wxWindowBase *window = nullptr;

	this->editorframe = new EditorSidepanel("Editor de Cen�rio");
	this->editorframe->SetPosition(wxPoint(0,0));
	this->editorframe->SetParent(window);
}

void EditorManagerWindows::show() {
	this->editorframe->Show();
}

void EditorManagerWindows::destroy() {
	if (this->editorframe) {
		this->editorframe->Destroy();
	}
	GlobalInterfaceIO::setGlobalIO(nullptr);
}
