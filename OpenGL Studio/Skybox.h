#pragma once
#include <glad\glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "TextureControl.h"

class Skybox {
	private:
		unsigned int attr_view;
		unsigned int attr_projection;

		TextureImage *rt;
		TextureImage *lf;
		TextureImage *up;
		TextureImage *dn;
		TextureImage *bk;
		TextureImage *ft;

		unsigned int programme;
		unsigned int vao;
		unsigned int texture;

	public:
		Skybox();
		void load(const char *path = "\\..\\OpenGL Studio\\Textures\\Skybox\\night\\");
		void render(glm::mat4 view, glm::mat4 projection, glm::vec3 posCam);
		void flushToGL();
		void freeShaders();
};

