#include "EditorFrame.h"
#include "InterfaceIO.h"


EditorSidepanel::EditorSidepanel(const wxString& title) : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(500, 640)) {
	style(this);
	this->SetIcon(wxIcon("aaaIDI_ICON"));

	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	this->createDetailsBar(vbox);

	wxNotebook *tabs = new wxNotebook(this, -1, wxDefaultPosition, wxDefaultSize, wxNB_LEFT);

	EditorSidepanel::createPageEdit(tabs);
	EditorSidepanel::createPageConsole(tabs);
	EditorSidepanel::createPageConfiguration(tabs);

	vbox->Add(tabs, 1, wxEXPAND);

	this->SetSizer(vbox);
	GlobalInterfaceIO::setGlobalIO(this);
	GlobalInterfaceIO::setGlobalInfoIO(this);

	this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(EditorSidepanel::close_event));
}

EditorSidepanel::~EditorSidepanel() {
	this->mapObject.clear();
	this->importedObjs.clear();
	this->importedLights.clear();
}

void EditorSidepanel::createDetailsBar(wxBoxSizer * principal) {
	wxPanel *panel = new wxPanel(this);
	principal->Add(panel, 0, wxALL | wxEXPAND, 10);
}

void EditorSidepanel::createPageConfiguration(wxNotebook* parent) {
	wxPanel *panel = new wxPanel(parent);

	wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

	this->gamma = new wxSlider(panel, ID_GAMMA, 10, 1, 50, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL | wxSL_BOTTOM);
	this->Connect(ID_GAMMA, wxEVT_SLIDER, wxScrollEventHandler(EditorSidepanel::gamma_event));

	sizer->Add(style(new wxStaticText(panel, -1, "Gamma")), 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
	sizer->Add(style(this->gamma), 0, wxBOTTOM | wxLEFT | wxRIGHT, 10);

	this->hdr = new wxSlider(panel, ID_HDR, 10, 1, 100, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL | wxSL_BOTTOM);
	this->Connect(ID_HDR, wxEVT_SLIDER, wxScrollEventHandler(EditorSidepanel::hdr_event));

	sizer->Add(style(new wxStaticText(panel, -1, "HDR Exposure")), 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
	sizer->Add(style(this->hdr), 0, wxBOTTOM | wxLEFT | wxRIGHT, 10);

	this->cull_face = new wxCheckBox(panel, ID_CULL_FACE, "CULL FACE");
	this->cull_face->SetValue(true);
	this->Connect(ID_CULL_FACE, wxEVT_CHECKBOX, wxCommandEventHandler(EditorSidepanel::cull_face_event));

	sizer->Add(style(this->cull_face), 0, wxLEFT | wxRIGHT | wxTOP, 10);

	this->normalmap = new wxCheckBox(panel, ID_NORMALMAP, "Normal Mapping");
	this->normalmap->SetValue(true);
	this->Connect(ID_NORMALMAP, wxEVT_CHECKBOX, wxCommandEventHandler(EditorSidepanel::normal_mapping_event));

	sizer->Add(style(this->normalmap), 0, wxLEFT | wxRIGHT, 10);

	this->bloom = new wxCheckBox(panel, ID_BLOOM, "Bloom");
	this->bloom->SetValue(true);
	this->Connect(ID_BLOOM, wxEVT_CHECKBOX, wxCommandEventHandler(EditorSidepanel::bloom_event));

	sizer->Add(style(this->bloom), 0, wxLEFT | wxRIGHT, 10);

	panel->SetSizer(sizer);

	parent->AddPage(panel, "Configurações", false);
}

void EditorSidepanel::createPageEdit(wxNotebook* parent) {

	wxBoxSizer *sizer_principal = new wxBoxSizer(wxVERTICAL);

	wxNotebook *tabs = new wxNotebook(parent, -1);
	wxPanel *ambience_page = EditorSidepanel::createPageEditAmbience(tabs);
	wxPanel *objects_page = EditorSidepanel::createPageEditObjects(tabs);
	wxPanel *lights_page = EditorSidepanel::createPegaEditIlumination(tabs);

	tabs->AddPage(objects_page, "Objetos", false);
	tabs->AddPage(lights_page, "Iluminação", false);
	tabs->AddPage(ambience_page, "Ambiente", false);

	parent->AddPage(tabs, "Edição", false);
}

void EditorSidepanel::createPageConsole(wxNotebook * parent) {

	this->console = new wxTextCtrl(parent, -1, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_MULTILINE | wxTE_BESTWRAP);
	this->console->SetBackgroundColour("black");
	this->console->SetForegroundColour("white");

	parent->AddPage(this->console, "Console", true);
}

void EditorSidepanel::addImport(dataImport *data) {
	wxTreeItemId id_obj = this->treeObjects->InsertItem(this->treeObjects->GetRootItem(), this->importedObjs.size(), std::string("File: ") + data->filename);
	TreeObject *obj = new TreeObject();
	obj->id = id_obj;
	obj->importedData = data;
	this->importedObjs.push_back(obj);

	wxTreeItemId id_light = this->treeLights->InsertItem(this->treeLights->GetRootItem(), this->importedLights.size(), std::string("File: ") + data->filename);
	TreeLight light;
	light.id = id_light;
	light.importedData = data;
	this->importedLights.push_back(light);
}

void EditorSidepanel::addObject(Object3D *object, unsigned int pos) {
	TreeObject *fileobj = this->importedObjs[pos]; //array fora do limites
	wxTreeItemId id = this->treeObjects->InsertItem(fileobj->id, fileobj->meshes.size(), object->getName());
	fileobj->meshes.insert(std::pair<wxTreeItemId, Object3D*>(id, object));
	this->mapObject.insert(std::pair<Object3D*, wxTreeItemId>(object, id));
}

void EditorSidepanel::addLight(std::vector<Light*> lights, unsigned int pos) {
	TreeLight obj = this->importedLights[pos]; //array fora do limites
	obj.lights = lights;
	int count = 0;
	for (Light *light : lights) {
		this->treeObjects->InsertItem(obj.id, count, std::string(light->getType()) + std::string(" ") + std::to_string(count));
		count++;
	}
}

void EditorSidepanel::clickObject(wxTreeEvent & event) {
	wxTreeItemId root = this->treeObjects->GetRootItem();
	wxTreeItemId id = event.GetItem();
	if (root != id) {
		wxTreeItemId parent = this->treeObjects->GetItemParent(id);
		engine::clearHightlightObject();

		if (parent == root) {
			for (TreeObject *obj : this->importedObjs) {
				if (obj->id == id) {
					std::map<wxTreeItemId, Object3D*>::iterator it = obj->meshes.begin();
					for (it = obj->meshes.begin(); it != obj->meshes.end(); ++it) {
						it->second->isOutlined = true;
					}
					break;
				}
			}
		} else {
			for (TreeObject *obj : this->importedObjs) {
				if (obj->id == parent) {
					std::map<wxTreeItemId, Object3D*>::iterator it = obj->meshes.find(id);
					if (it != obj->meshes.end()) {
						Object3D *m = it->second;
						m->isOutlined = true;
						break;
					}
				}
			}
		}
	}

}

wxPanel* EditorSidepanel::createPageEditAmbience(wxNotebook* parent) {
	wxPanel *panel = new wxPanel(parent);
	wxBoxSizer *principal = new wxBoxSizer(wxHORIZONTAL);

	wxStaticBox *box_sun = new wxStaticBox(panel, wxID_ANY, "Luz Direcional");
	wxStaticBoxSizer *boxsizer_sun = new wxStaticBoxSizer(box_sun, wxVERTICAL);

	box_sun->SetForegroundColour("white");

	this->color_sun_ambient = new wxColourPickerCtrl(panel, ID_SUN_AMBIENT);
	this->Connect(ID_SUN_AMBIENT, wxEVT_COLOURPICKER_CHANGED, wxColourPickerEventHandler(EditorSidepanel::sun_ambient_event));

	this->color_sun_diffuse = new wxColourPickerCtrl(panel, ID_SUN_DIFFUSE);
	this->Connect(ID_SUN_DIFFUSE, wxEVT_COLOURPICKER_CHANGED, wxColourPickerEventHandler(EditorSidepanel::sun_diffuse_event));

	this->position_sun = new wxCoord3D(panel, -1.0, 1.0);
	this->position_sun->change(this, wxSpinDoubleEventHandler(EditorSidepanel::sun_position_change));

	boxsizer_sun->Add(style(new wxStaticText(panel, -1, "Luz Ambiente")), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_sun->Add(style(this->color_sun_ambient), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_sun->AddSpacer(5);

	boxsizer_sun->Add(style(new wxStaticText(panel, -1, "Luz Difusa")), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_sun->Add(style(this->color_sun_diffuse), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_sun->AddSpacer(5);

	boxsizer_sun->Add(this->position_sun, 0, wxLEFT | wxRIGHT, 8);

	wxStaticBox *box_fog = new wxStaticBox(panel, wxID_ANY, "FOG");
	wxStaticBoxSizer *boxsizer_fog = new wxStaticBoxSizer(box_fog, wxVERTICAL);

	box_fog->SetForegroundColour("white");

	this->enable_fog = new wxCheckBox(panel, ID_FOG_ENABLED, "Ativado");
	this->enable_fog->SetValue(true);
	this->Connect(ID_FOG_ENABLED, wxEVT_CHECKBOX, wxCommandEventHandler(EditorSidepanel::fog_enabled_event));

	this->color_fog = new wxColourPickerCtrl(panel, ID_FOG_COLOR);
	this->color_fog->SetColour(wxString("RGB(255,255,255)"));
	this->Connect(ID_FOG_COLOR, wxEVT_COLOURPICKER_CHANGED, wxColourPickerEventHandler(EditorSidepanel::fog_color_event));

	this->max_fog = new wxSlider(panel, ID_FOG_MAX, 40, 1, 100, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL | wxSL_BOTTOM);
	this->Connect(ID_FOG_MAX, wxEVT_SLIDER, wxScrollEventHandler(EditorSidepanel::fog_max_event));

	this->gap_fog = new wxSpinCtrlDouble(panel, ID_FOG_GAP, "0", wxDefaultPosition, wxDefaultSize, 16896L, 0, 800);
	this->Connect(ID_FOG_GAP, wxEVT_SPINCTRLDOUBLE, wxSpinDoubleEventHandler(EditorSidepanel::fog_gap_event));

	this->delta_fog = new wxSpinCtrlDouble(panel, ID_FOG_DELTA, "40", wxDefaultPosition, wxDefaultSize, 16896L, 0, 800);
	this->Connect(ID_FOG_DELTA, wxEVT_SPINCTRLDOUBLE, wxSpinDoubleEventHandler(EditorSidepanel::fog_delta_event));

	boxsizer_fog->Add(style(this->enable_fog), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_fog->AddSpacer(5);

	boxsizer_fog->Add(style(new wxStaticText(panel, -1, "Cor")), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_fog->Add(style(this->color_fog), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_fog->AddSpacer(5);

	boxsizer_fog->Add(style(new wxStaticText(panel, -1, "Maximo")), 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	boxsizer_fog->Add(style(this->max_fog));
	boxsizer_fog->AddSpacer(5);

	boxsizer_fog->Add(style(new wxStaticText(panel, -1, "Delta")), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_fog->Add(style(this->delta_fog));
	boxsizer_fog->AddSpacer(5);

	boxsizer_fog->Add(style(new wxStaticText(panel, -1, "Deslocamento")), 0, wxLEFT | wxRIGHT, 10);
	boxsizer_fog->Add(style(this->gap_fog));

	principal->Add(boxsizer_sun, 0, wxALL, 10);
	principal->Add(boxsizer_fog, 0, wxALL, 10);

	panel->SetSizer(principal);
	return panel;
}

void EditorSidepanel::updateFog() {
	double max = this->max_fog->GetValue() / 100.0;
	double delta = this->delta_fog->GetValue();
	double gap = this->gap_fog->GetValue();
	bool enabled = this->enable_fog->GetValue();
	wxColor color = this->color_fog->GetColour();

	this->fog_control->max = max;
	this->fog_control->gap = gap;
	this->fog_control->delta = delta;
	this->fog_control->enabled = (enabled ? 1.0 : 0.0);
	this->fog_control->color[0] = color.Red();
	this->fog_control->color[1] = color.Green();
	this->fog_control->color[2] = color.Blue();
}

void EditorSidepanel::fog_enabled_event(wxCommandEvent &event) {
	this->fog_control->enabled = event.IsChecked();
}

void EditorSidepanel::fog_color_event(wxColourPickerEvent & event) {
	wxColour color = event.GetColour();
	this->fog_control->color[0] = color.Red() / 255.0;
	this->fog_control->color[1] = color.Green() / 255.0;
	this->fog_control->color[2] = color.Blue() / 255.0;
}

void EditorSidepanel::fog_max_event(wxScrollEvent & event) {
	this->fog_control->max = event.GetPosition() / 100.0;
}

void EditorSidepanel::fog_gap_event(wxSpinDoubleEvent & event) {
	this->fog_control->gap = event.GetValue();
}

void EditorSidepanel::fog_delta_event(wxSpinDoubleEvent & event) {
	this->fog_control->delta = event.GetValue();
}

void EditorSidepanel::sun_ambient_event(wxColourPickerEvent &event) {
	wxColour color = event.GetColour();
	this->sun_control->ambient[0] = color.Red() / 255.0;
	this->sun_control->ambient[1] = color.Green() / 255.0;
	this->sun_control->ambient[2] = color.Blue() / 255.0;
}

void EditorSidepanel::sun_diffuse_event(wxColourPickerEvent & event) {
	wxColour color = event.GetColour();
	this->sun_control->diffuse[0] = color.Red() / 255.0;
	this->sun_control->diffuse[1] = color.Green() / 255.0;
	this->sun_control->diffuse[2] = color.Blue() / 255.0;
}

void EditorSidepanel::sun_position_change(wxSpinDoubleEvent & event) {
	float x = this->position_sun->getX();
	float y = this->position_sun->getY();
	float z = this->position_sun->getZ();
	this->sun_control->setPosition(x, y, z);
	engine::moduloGame->updateShadowMapping();
}

void EditorSidepanel::gamma_event(wxScrollEvent & event) {
	engine::screen->gamma = event.GetPosition() / 10.0;
	engine::cmd("update_screen");
}

void EditorSidepanel::hdr_event(wxScrollEvent & event) {
	engine::screen->exposure = event.GetPosition() / 10.0;
	engine::cmd("update_screen");
}

void EditorSidepanel::cull_face_event(wxCommandEvent & event) {
	std::map<std::string, std::string> params;
	params.insert(std::pair<std::string, std::string>("enable",(event.IsChecked()?"true":"false")));
	engine::cmdCtrl->add("cullface", params);
	engine::cmdCtrl->lock = true;
}

void EditorSidepanel::normal_mapping_event(wxCommandEvent & event) {
	engine::normalmapping = event.IsChecked();
}

void EditorSidepanel::bloom_event(wxCommandEvent & event) {
	engine::screen->bloom = (event.IsChecked() ? 1 : 0);
	engine::cmd("update_screen");
}

void EditorSidepanel::close_event(wxCloseEvent & event) {
	event.Veto();
	engineWindow::force_close();
}

UINT32 EditorSidepanel::rgbToUint32(float* rgb) {
	int r = rgb[0] * 255;
	int g = rgb[1] * 255;
	int b = rgb[2] * 255;
	return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}

wxPanel* EditorSidepanel::createPageEditObjects(wxNotebook* parent) {
	wxPanel *panel = new wxPanel(parent);
	wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);

	//Objects Panel
	wxBoxSizer *sizerObjects = new wxBoxSizer(wxVERTICAL);

	wxPanel *panelObjects = new wxPanel(panel, -1, wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN);

	wxStaticText *label_anti = new wxStaticText(panelObjects, -1, "Objetos");
	label_anti->SetForegroundColour("#FFFFFF");
	this->treeObjects = new wxTreeCtrl(panelObjects, ID_TREE_OBJECT);
	this->treeObjects->SetBackgroundColour("#444444");
	this->treeObjects->SetForegroundColour("#FFFFFF");

	wxTreeItemId nome_mapa = this->treeObjects->AddRoot("Mapa Teste");

	this->treeObjects->ExpandAll();

	this->Connect(ID_TREE_OBJECT, wxEVT_TREE_ITEM_ACTIVATED, wxTreeEventHandler(EditorSidepanel::clickObject));

	sizerObjects->Add(label_anti, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
	sizerObjects->Add(this->treeObjects, 1, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
	panelObjects->SetSizer(sizerObjects);

	//Properties Panel

	wxBoxSizer *sizerProperties = new wxBoxSizer(wxVERTICAL);
	wxPanel *panelProperties = new wxPanel(panel, -1, wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN);

	sizerProperties->Add(style(new wxStaticText(panelProperties, -1, "Propriedades")), 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);

	this->propertyGrid = new wxPropertyGrid(panelProperties, ID_EDIT_OBJECT);
	this->propertyGrid->Append(new wxStringProperty("Identificação", wxPG_LABEL, ""));

	wxPGProperty* translacao = this->propertyGrid->Append(new wxStringProperty("Translação", wxPG_LABEL, "<composed>"));

	this->propertyGrid->AppendIn(translacao, new wxIntProperty("X", wxPG_LABEL, 0));
	this->propertyGrid->AppendIn(translacao, new wxIntProperty("Y", wxPG_LABEL, 0));
	this->propertyGrid->AppendIn(translacao, new wxIntProperty("Z", wxPG_LABEL, 0));

	wxPGProperty* redimensionar = this->propertyGrid->Append(new wxStringProperty("Redimensionar", wxPG_LABEL, "<composed>"));

	this->propertyGrid->AppendIn(redimensionar, new wxIntProperty("X", wxPG_LABEL, 0));
	this->propertyGrid->AppendIn(redimensionar, new wxIntProperty("Y", wxPG_LABEL, 0));
	this->propertyGrid->AppendIn(redimensionar, new wxIntProperty("Z", wxPG_LABEL, 0));

	wxPGProperty* rotacionar = this->propertyGrid->Append(new wxStringProperty("Rotacionar", wxPG_LABEL, "<composed>"));

	this->propertyGrid->AppendIn(rotacionar, new wxIntProperty("X", wxPG_LABEL, 0));
	this->propertyGrid->AppendIn(rotacionar, new wxIntProperty("Y", wxPG_LABEL, 0));
	this->propertyGrid->AppendIn(rotacionar, new wxIntProperty("Z", wxPG_LABEL, 0));

	rotacionar->SetExpanded(false);
	redimensionar->SetExpanded(false);
	translacao->SetExpanded(false);

	wxArrayString arrShaders;
	arrShaders.Add("COMPLETO");
	arrShaders.Add("ONDA (GEOMETRIA)");
	arrShaders.Add("SOMENTE DIFUSA");
	arrShaders.Add("COORDENADAS");
	propertyGrid->Append(new wxEnumProperty("Shader", wxPG_LABEL, arrShaders));

	sizerProperties->Add(style(propertyGrid), 1, wxEXPAND | wxLEFT | wxRIGHT, 10);

	panelProperties->SetSizer(sizerProperties);

	//Panel

	sizer->Add(panelObjects, 0.6, wxEXPAND);
	sizer->Add(panelProperties, 2.0, wxEXPAND);
	panel->SetSizer(sizer);

	return panel;
}

wxPanel* EditorSidepanel::createPegaEditIlumination(wxNotebook * parent) {
	wxPanel *panel = new wxPanel(parent);
	wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

	//Objects Panel
	wxBoxSizer *sizerLights = new wxBoxSizer(wxVERTICAL);

	wxPanel *panelLights = new wxPanel(panel, -1, wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN);

	wxStaticText *label_anti = new wxStaticText(panelLights, -1, "Recursos");
	label_anti->SetForegroundColour("#FFFFFF");
	this->treeLights = new wxTreeCtrl(panelLights, ID_TREE_LIGHT);
	this->treeLights->SetBackgroundColour("#444444");
	this->treeLights->SetForegroundColour("#FFFFFF");

	wxTreeItemId nome_mapa = this->treeLights->AddRoot("Mapa Teste");

	this->treeLights->ExpandAll();

	sizerLights->Add(label_anti, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
	sizerLights->Add(this->treeLights, 1, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
	panelLights->SetSizer(sizerLights);

	//Properties Panel

	wxBoxSizer *sizerProperties = new wxBoxSizer(wxVERTICAL);
	wxPanel *panelProperties = new wxPanel(panel, -1, wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN);

	panelProperties->SetSizer(sizerProperties);

	//Panel

	sizer->Add(panelLights, 1, wxEXPAND);
	sizer->Add(panelProperties, 0, wxEXPAND);
	panel->SetSizer(sizer);

	return panel;
}

wxWindow *EditorSidepanel::style(wxWindow * obj) {
	obj->SetBackgroundColour("#444444");
	obj->SetForegroundColour("white");
	return obj;
}

void EditorSidepanel::echo(val_output key, const char* value) {
	switch (key) {
	case FPS_COUNTER: this->fps_label->SetLabel(value); break;
	}
}

void EditorSidepanel::echo(const char * mensagem, message_type type) {
	wxMessageDialog * dial;
	switch (type) {
	case LOG:
		this->appendConsole(mensagem);
		break;
	case POPUP:
		dial = new wxMessageDialog(NULL, wxString(mensagem), wxT("Opss"), wxOK | wxICON_INFORMATION);
		dial->ShowModal();
		break;
	case ERRO:
		dial = new wxMessageDialog(NULL, wxString(mensagem), wxT("Opss"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		break;
	}
}

void EditorSidepanel::echo_action(action_type action, Object3D *object, unsigned int pos) {
	switch (action) {
	case ADD:
		this->addObject(object, pos);
		break;
	}
}

void EditorSidepanel::echo_action(action_type action, std::vector<Light*> lights, unsigned int pos) {
	switch (action) {
	case ADD:
		this->addLight(lights, pos);
		break;
	}
}

void EditorSidepanel::echo_action(action_type action, Fog * fog) {
	switch (action) {
	case SET:
		this->fog_control = fog;
		this->enable_fog->SetValue(fog->enabled);
		this->max_fog->SetValue(fog->max * 100);
		this->delta_fog->SetValue(fog->delta);
		this->gap_fog->SetValue(fog->gap);
		this->color_fog->SetColour(wxColour(this->rgbToUint32(fog->color)));
		break;
	}

}

void EditorSidepanel::echo_action(action_type action, Sun * sun) {
	switch (action) {
	case SET:
		this->sun_control = sun;
		this->color_sun_ambient->SetColour(wxColour(this->rgbToUint32(sun->ambient)));
		this->color_sun_diffuse->SetColour(wxColour(this->rgbToUint32(sun->diffuse)));

		this->position_sun->setValue(sun->direction[0], sun->direction[1], sun->direction[2]);
		break;
	}
}

void EditorSidepanel::appendConsole(const char * mensagem) {
	wxString atual = this->console->GetValue();
	this->console->SetValue(atual + wxString(mensagem) + wxString("\n"));
}

void wxCoord3D::change(wxFrame *frame, wxEventFunction event) {
	frame->Connect(this->x_input->GetId(), wxEVT_SPINCTRLDOUBLE, event);
	frame->Connect(this->y_input->GetId(), wxEVT_SPINCTRLDOUBLE, event);
	frame->Connect(this->z_input->GetId(), wxEVT_SPINCTRLDOUBLE, event);
}

void wxCoord3D::setValue(double x, double y, double z) {
	this->x_input->SetValue(x);
	this->y_input->SetValue(y);
	this->z_input->SetValue(z);
}

float wxCoord3D::getX() {
	return this->x_input->GetValue();
}

float wxCoord3D::getY() {
	return this->y_input->GetValue();
}

float wxCoord3D::getZ() {
	return this->z_input->GetValue();
}

wxCoord3D::wxCoord3D(wxPanel *panel, double min, double max) : wxFlexGridSizer(4, 2, 3, 3) {
	static int offset = 500;
	this->x_input = new wxSpinCtrlDouble(panel, ID_X + offset, "0", wxDefaultPosition, wxSize(80, 20), 16896L, min, max, 0.0, 0.1);
	this->y_input = new wxSpinCtrlDouble(panel, ID_Y + offset, "1", wxDefaultPosition, wxSize(80, 20), 16896L, min, max, 0.0, 0.1);
	this->z_input = new wxSpinCtrlDouble(panel, ID_Z + offset, "0", wxDefaultPosition, wxSize(80, 20), 16896L, min, max, 0.0, 0.1);
	this->Add(EditorSidepanel::style(new wxStaticText(panel, -1, "x:")));
	this->Add(this->x_input, 1.0f / 3.0f, wxLEFT | wxRIGHT, 2);
	this->Add(EditorSidepanel::style(new wxStaticText(panel, -1, "y:")));
	this->Add(this->y_input, 1.0f / 3.0f, wxLEFT | wxRIGHT, 2);
	this->Add(EditorSidepanel::style(new wxStaticText(panel, -1, "z:")));
	this->Add(this->z_input, 1.0f / 3.0f, wxLEFT | wxRIGHT, 2);
	offset += 3;
}