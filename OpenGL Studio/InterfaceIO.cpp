#include "InterfaceIO.h"

namespace GlobalInterfaceIO {

	InterfaceIO *global;
	InterfaceInfoIO *globalInfo;
	std::string atualWorkingFolder;

	void setAtualWorkingFolder(std::string path) {
		atualWorkingFolder = path;
	}

	std::string getAtualWorkingFolder(const char *path) {
		std::string atual_folder = atualWorkingFolder + std::string(path);
		return atual_folder;
	}

	void alert(const char * mensagem) {
		global->echo(mensagem, POPUP);
	}

	void error(const char * mensagem) {
		global->echo(mensagem, ERRO);
	}

	void log(const char * mensagem) {
		global->echo(mensagem, LOG);
	}

	void addFogInfo(Fog *fog) {
		globalInfo->echo_action(SET, fog);
	}

	void addSunInfo(Sun * sun) {
		globalInfo->echo_action(SET, sun);
	}

	void addObjectInfo(Object3D* m, unsigned int pos) {
		globalInfo->echo_action(ADD, m, pos);
	}

	void addLightInfo(std::vector<Light*> lights, unsigned int pos) {
		globalInfo->echo_action(ADD, lights, pos);
	}

	void setValOutput(val_output key, const char* value) {
		globalInfo->echo(key, value);
	}

	void setGlobalIO(InterfaceIO *io) {
		global = io;
	}

	void setGlobalInfoIO(InterfaceInfoIO *io) {
		globalInfo = io;
	}

}

