#pragma once
#include <vector>

struct ScreenComponent{
	float posX;
	float posY;
	float sizeX;
	float sizeY;

	unsigned int id_texture;
};

class ScreenEngine {
	private:
		int width;
		int height;
		unsigned int id_framebuffer_blur_horizontal;
		unsigned int id_framebuffer_blur_vertical;

		unsigned int id_texture_blur_horizontal;
		unsigned int id_texture_blur_vertical;
		unsigned int id_texture_bloom; //color attachment1
		unsigned int id_texture; //color attachment0

		unsigned int id_shader; //shader screen
		unsigned int id_shader_blur; //shader blur bloom
		unsigned int id_shader_component;

		//shader bloom
		unsigned int attr_id_horizontal;

		//shader screen
		unsigned int attr_id_bloom;
		unsigned int attr_id_exposure;
		unsigned int attr_id_gamma;

		//shader component
		unsigned int attr_id_pos_x;
		unsigned int attr_id_pos_y;
		unsigned int attr_id_size_x;
		unsigned int attr_id_size_y;
		unsigned int attr_id_WindowSize;

		std::vector<ScreenComponent*> components;

	public:
		unsigned int id_framebuffer_to_screen; //framebuffer attachment0/attachment1
		unsigned int id_quad_object; //quad

		unsigned int id_texture_loading;//image from screen load
		unsigned int id_shader_loading;//shader to screen load

		float exposure;
		float gamma;
		int bloom;

		ScreenEngine(unsigned int width, unsigned int height);
		void apply();
		void use();
		void render();

		void addImageComponent(const char *path, float x, float y,int xx, int yy);

		void freeShaders();
		void freeBuffers();
};

