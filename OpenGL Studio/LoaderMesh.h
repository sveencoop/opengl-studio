#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include "Obj3D.h"
#include "Pointlight.h"
#include "Spotlight.h"
#include "TextureControl.h"

class LoaderMesh {
	private:
		const char *path;
		const char *directory;
		std::vector<TextureImage*> textureImagesCompiled;
		float shininess;
		float IOR;
		void processLights(const aiScene *scene);

		Mesh* processMesh(Object3D* obj, aiMesh *mesh, const aiScene *scene);
		Pointlight* processPointlight(aiLight * light, glm::mat4 transformation);
		Spotlight* processSpotlight(aiLight * light, glm::mat4 transformation);
		glm::vec3 convert(aiColor3D value);
		TextureImage *getTexture(const char *path);

	public:
		std::vector<Light*> lights;
		std::vector<Object3D*> objects;
		LoaderMesh();
		LoaderMesh(const char *directory, const char *path);
		void loader(std::vector<Mesh*>& meshes);
		void processNode(aiNode *node, const aiScene *scene, std::vector<Mesh*>& meshes);
		void processRootNode(aiNode *node, const aiScene *scene, std::vector<Mesh*>& meshes);
		void setShininess(float);
		void setIOR(float);
		void flushTextures(std::vector<TextureImage*>& images);
};

