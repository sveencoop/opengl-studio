#include "Skybox.h"
#include "InterfaceIO.h"
#include "StringBuilder.h"
#include "ShaderControl.h"

Skybox::Skybox() {
	this->bk = nullptr;
	this->dn = nullptr;
	this->ft = nullptr;
	this->lf = nullptr;
	this->rt = nullptr;
	this->up = nullptr;
}

void Skybox::load(const char *path) {

	std::string atual_folder = GlobalInterfaceIO::getAtualWorkingFolder(path);
	const char *folder = atual_folder.c_str();

	this->bk = texture::createImage(StringBuilder::concat(folder, "bk.jpg"));
	this->dn = texture::createImage(StringBuilder::concat(folder, "dn.jpg"));
	this->ft = texture::createImage(StringBuilder::concat(folder, "ft.jpg"));
	this->lf = texture::createImage(StringBuilder::concat(folder, "lf.jpg"));
	this->rt = texture::createImage(StringBuilder::concat(folder, "rt.jpg"));
	this->up = texture::createImage(StringBuilder::concat(folder, "up.jpg"));
}

void Skybox::render(glm::mat4 view, glm::mat4 projection, glm::vec3 posCam) {

	glDepthFunc(GL_LEQUAL);
	view = glm ::mat4(glm::mat3(view));
	glUseProgram(this->programme);
	glBindVertexArray(this->vao);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, this->texture);
	glUniformMatrix4fv(this->attr_view, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(this->attr_projection, 1, GL_FALSE, glm::value_ptr(projection));

	//openglUtils::setMatriz(this->programme, "view", glm::value_ptr(view));
	//openglUtils::setMatriz(this->programme, "projection", glm::value_ptr(projection));

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glDepthFunc(GL_LESS);
}

void Skybox::flushToGL() {

	float skyboxVertices[] = {

		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	unsigned int newvao, newvbo;
	glGenVertexArrays(1, &newvao);
	glGenBuffers(1, &newvbo);
	glBindVertexArray(newvao);
	glBindBuffer(GL_ARRAY_BUFFER, newvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	this->vao = newvao;
	this->programme = shader::compileShader("../OpenGL Studio/Shaders/skybox/test.vsh", "../OpenGL Studio/Shaders/skybox/test.fsh");
	this->texture = texture::initCompileCubeMap();
	this->rt->flushToGLCubeMap(GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	this->lf->flushToGLCubeMap(GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	this->up->flushToGLCubeMap(GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	this->dn->flushToGLCubeMap(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	this->bk->flushToGLCubeMap(GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	this->ft->flushToGLCubeMap(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	glUseProgram(this->programme);
	glUniform1i(glGetUniformLocation(this->programme, "skytexture"), 0);
	this->attr_view = glGetUniformLocation(this->programme, "view");
	this->attr_projection = glGetUniformLocation(this->programme, "projection");
}

void Skybox::freeShaders() {
	shader::freeShader(this->programme);
}
