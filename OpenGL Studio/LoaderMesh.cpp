#include "LoaderMesh.h"
#include "InterfaceIO.h"
#include "StringBuilder.h"

using namespace GlobalInterfaceIO;

LoaderMesh::LoaderMesh() {
	this->shininess = 16;
	this->IOR = 0;
}

LoaderMesh::LoaderMesh(const char * directory, const char * path) {
	this->directory = directory;
	this->path = path;
	this->shininess = 32;
	this->IOR = 0;
}

void LoaderMesh::loader(std::vector<Mesh*>& meshes) {

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(StringBuilder::concat(this->directory, this->path),
		aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_PreTransformVertices | aiProcess_FixInfacingNormals | aiProcess_CalcTangentSpace);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		const char *error_msg = importer.GetErrorString();
		error(StringBuilder::concat(this->path, " get error on ASSIMP::", error_msg));
		return;
	}

	aiNode* root = scene->mRootNode;

	this->processRootNode(root, scene, meshes);
	this->processLights(scene);

}

void LoaderMesh::processLights(const aiScene * scene) {

	aiNode* root = scene->mRootNode;

	aiMatrix4x4 transformation = root->mTransformation;
	glm::mat4 trans = glm::mat4(transformation.a1, transformation.b1, transformation.c1, transformation.d1,
		transformation.a2, transformation.b2, transformation.c2, transformation.d2,
		transformation.a3, transformation.b3, transformation.c3, transformation.d3,
		transformation.a4, transformation.b4, transformation.c4, transformation.d4);


	for (unsigned int i = 0; i < scene->mNumLights; i++) {
		aiLight *light = scene->mLights[i];

		if (light->mType == aiLightSource_POINT) {
			Light* lightnew = processPointlight(light, trans);
			lights.push_back(lightnew);

		} else if (light->mType == aiLightSource_SPOT) {

			Light *lightnew = processSpotlight(light, trans);
			lights.push_back(lightnew);
		}
	}
}

void LoaderMesh::processNode(aiNode *node, const aiScene *scene, std::vector<Mesh*>& meshes) {
	aiMatrix4x4 transformation = node->mTransformation;

	const char *name = node->mName.C_Str();

	aiMetadata *data = node->mMetaData;

	Object3D *obj = new Object3D(name);

	glm::mat4 trans = glm::mat4(transformation.a1, transformation.b1, transformation.c1, transformation.d1,
		transformation.a2, transformation.b2, transformation.c2, transformation.d2,
		transformation.a3, transformation.b3, transformation.c3, transformation.d3,
		transformation.a4, transformation.b4, transformation.c4, transformation.d4);


	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		Mesh *novo = processMesh(obj, mesh, scene);
		
		meshes.push_back(novo);
		obj->addMesh(novo);
	}
	obj->setTransform(trans);
	objects.push_back(obj);
	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		processNode(node->mChildren[i], scene, meshes);
	}
}

void LoaderMesh::processRootNode(aiNode * node, const aiScene * scene, std::vector<Mesh*>& meshes) {
	if (node->mNumMeshes == 0 || node->mNumChildren != 0) {
		for (unsigned int i = 0; i < node->mNumChildren; i++) {
			if (node->mChildren[i]->mNumMeshes != 0) {
				this->processNode(node->mChildren[i], scene, meshes);
			}
		}
	} else {
		this->processNode(node, scene, meshes);
	}

}

Pointlight* LoaderMesh::processPointlight(aiLight * light, glm::mat4 transformation) {
	Pointlight *p = new Pointlight();
	glm::vec4 pos = glm::vec4(light->mPosition.x, light->mPosition.y, light->mPosition.z, 1.0);
	pos = transformation * pos;
	p->setPosition(pos.x, pos.y, pos.z);

	p->init(convert(light->mColorAmbient), convert(light->mColorDiffuse), convert(light->mColorSpecular));
	p->setAttenuation(light->mAttenuationConstant, light->mAttenuationLinear, light->mAttenuationQuadratic);
	return p;
}

Spotlight* LoaderMesh::processSpotlight(aiLight * light, glm::mat4 transformation) {
	Spotlight *s = new Spotlight();
	glm::vec4 dir = glm::vec4(light->mDirection.x, light->mDirection.y, light->mDirection.z, 1.0);
	glm::vec4 pos = glm::vec4(light->mPosition.x, light->mPosition.y, light->mPosition.z, 1.0);
	pos = transformation * pos;
	dir = transformation * dir;
	s->setPosition(pos.x, pos.y, pos.z);
	s->setDirection(dir.x, dir.y, dir.z);

	s->init(convert(light->mColorAmbient), convert(light->mColorDiffuse), convert(light->mColorSpecular));
	s->setAttenuation(light->mAttenuationConstant, light->mAttenuationLinear, light->mAttenuationQuadratic);
	s->setInnerCutOff(light->mAngleOuterCone);
	s->setOuterCutOff(light->mAngleInnerCone);
	s->updateEpsilon();
	return s;
}

glm::vec3 LoaderMesh::convert(aiColor3D value) {
	glm::vec3 saida;
	saida.r = value.r;
	saida.g = value.g;
	saida.b = value.b;
	return saida;
}

Mesh *LoaderMesh::processMesh(Object3D* obj, aiMesh *mesh, const aiScene *scene) {
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;

		vertex.Position.x = mesh->mVertices[i].x;
		vertex.Position.y = mesh->mVertices[i].y;
		vertex.Position.z = mesh->mVertices[i].z;

		obj->limits->test(vertex.Position);

		glm::vec3 normal = glm::normalize(glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z));

		vertex.Normal.x = normal.x;
		vertex.Normal.y = normal.y;
		vertex.Normal.z = normal.z;

		if(mesh->mTangents){
			glm::vec3 tangent = glm::normalize(glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z));
			glm::vec3 bitangent = glm::normalize(glm::vec3(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z));

			vertex.Tangents.x = tangent.x;
			vertex.Tangents.y = tangent.y;
			vertex.Tangents.z = tangent.z;
			vertex.BiTangents.x = tangent.x;
			vertex.BiTangents.y = tangent.y;
			vertex.BiTangents.z = tangent.z;
		} else {
			vertex.Tangents.x = 0.0;
			vertex.Tangents.y = 1.0;
			vertex.Tangents.z = 0.0;
			vertex.BiTangents.x = 1.0;
			vertex.BiTangents.y = 0.0;
			vertex.BiTangents.z = 0.0;
		}
		if (mesh->mTextureCoords[0]) {
			vertex.TexCoords.x = mesh->mTextureCoords[0][i].x;
			vertex.TexCoords.y = mesh->mTextureCoords[0][i].y;
		}
		else {
			vertex.TexCoords = glm::vec2(0.0, 0.0);
		}

		if (mesh->mTextureCoords[1]) {
			vertex.ShMCoords.x = mesh->mTextureCoords[1][i].x;
			vertex.ShMCoords.y = mesh->mTextureCoords[1][i].y;
		} else {
			vertex.ShMCoords = glm::vec2(0.0, 0.0);
		}


		vertices.push_back(vertex);
	}

	for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	Mesh *m = new Mesh(StringBuilder::copy(mesh->mName.C_Str()), vertices, indices);

	if (mesh->mMaterialIndex >= 0) {
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		unsigned int total;
		

		//int totais[13];

		//totais[0] = material->GetTextureCount(aiTextureType_DIFFUSE);
		//totais[1] = material->GetTextureCount(aiTextureType_SPECULAR);
		//totais[2] = material->GetTextureCount(aiTextureType_REFLECTION);
		//totais[3] = material->GetTextureCount(aiTextureType_NORMALS);
		//totais[4] = material->GetTextureCount(aiTextureType_AMBIENT);
		//totais[5] = material->GetTextureCount(aiTextureType_SHININESS);
		//totais[6] = material->GetTextureCount(aiTextureType_AMBIENT);
		//totais[7] = material->GetTextureCount(aiTextureType_DISPLACEMENT);
		//totais[8] = material->GetTextureCount(aiTextureType_EMISSIVE);
		//totais[9] = material->GetTextureCount(aiTextureType_HEIGHT);
		//totais[10] = material->GetTextureCount(aiTextureType_LIGHTMAP);
		//totais[11] = material->GetTextureCount(aiTextureType_UNKNOWN);
		//totais[12] = material->GetTextureCount(aiTextureType_NONE);
		
		total = material->GetTextureCount(aiTextureType_DIFFUSE);

		for (unsigned int i = 0; i < total; i++) {
			aiString str;

			material->GetTexture(aiTextureType_DIFFUSE, i, &str);
			const char* path_t = str.C_Str();
			TextureImage *image = this->getTexture(path_t);
			m->setTextureImageDiffuse(image);
		}

		total = material->GetTextureCount(aiTextureType_SPECULAR);

		for (unsigned int i = 0; i < total; i++) {
			aiString str;

			material->GetTexture(aiTextureType_SPECULAR, i, &str);
			m->setTextureImageSpecular(this->getTexture(str.C_Str()));
		}

		total = material->GetTextureCount(aiTextureType_REFLECTION);

		for (unsigned int i = 0; i < total; i++) {
			aiString str;
			material->GetTexture(aiTextureType_REFLECTION, i, &str);
			m->setTextureImageReflection(this->getTexture(str.C_Str()));
			m->setReflection(1.0);
		}

		total = material->GetTextureCount(aiTextureType_NORMALS);

		for (unsigned int i = 0; i < total; i++) {
			aiString str;
			material->GetTexture(aiTextureType_NORMALS, i, &str);
			m->setTextureImageNormal(this->getTexture(str.C_Str()));
		}


		total = material->GetTextureCount(aiTextureType_EMISSIVE);

		for (unsigned int i = 0; i < total; i++) {
			aiString str;
			material->GetTexture(aiTextureType_EMISSIVE, i, &str);
			m->setTextureImageEmission(this->getTexture(str.C_Str()));
		}


		float readshininess = 0.0;
		unsigned int max;
		aiGetMaterialFloatArray(material, AI_MATKEY_SHININESS_STRENGTH, &readshininess, &max);
		if (readshininess != 0.0) {
			m->setShininess(readshininess);
		}
	}

	m->setShininess(this->shininess);
	m->setIOR(this->IOR);

	return m;
}

TextureImage *LoaderMesh::getTexture(const char *searchpath) {

	const char *filepath = StringBuilder::concat(this->directory, searchpath);
	std::vector<TextureImage*>::reverse_iterator it = this->textureImagesCompiled.rbegin();

	while(it != this->textureImagesCompiled.rend()) {
		if (StringBuilder::equals((*it)->getPath(), filepath)) {
			return (*it);
		}
		it++;
	}

	TextureImage *image = texture::createImage(filepath);
	this->textureImagesCompiled.push_back(image);
	return image;
}

void LoaderMesh::setShininess(float value) {
	this->shininess = value;
}

void LoaderMesh::setIOR(float value) {
	this->IOR = value;
}

void LoaderMesh::flushTextures(std::vector<TextureImage*>& images) {
	for (TextureImage *t : this->textureImagesCompiled) {
		images.push_back(t);
	}
	this->textureImagesCompiled.clear();
}

