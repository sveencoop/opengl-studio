#pragma once
#include <vector>

class Light {

	protected:
		const char** attrs;

		float *diffuse;
		float *ambient;
		float *specular;

		float constant;
		float linear;
		float quadratic;

		float *position;
		float radius;

	public:
		virtual void apply() = 0;
		virtual void shaderingData(unsigned int shader) = 0;
		virtual const char* getType() = 0;
};

struct LightsVector{
	std::vector<Light*> list;
	unsigned int totalPointlight;
	unsigned int totalSpotlight;
};
