#version 400

layout(location = 0) in vec2 vp;   // Vertices
layout(location = 1) in vec2 aTexCoord; // Textura

uniform float posX;
uniform float posY;
uniform float sizeX;
uniform float sizeY;

out vec2 TexCoord;

void main() {
	gl_Position = vec4(vp.x*sizeX + posX, vp.y*sizeY + posY, 0, 1.0);
	TexCoord = aTexCoord;
}
