#version 400

out vec4 frag_colour;

in vec2 TexCoord;

uniform sampler2D image;
uniform sampler2D render;
uniform vec2 WindowSize;

void main() {

	vec4 source = texture(image, TexCoord.st);
	if (source.a < 0.5)
		discard;
	
	vec2 pos = vec2(gl_FragCoord.x/ WindowSize.x, gl_FragCoord.y / WindowSize.y);

	vec4 renderc = texture(render, pos);
	renderc.r = 1 - renderc.r;
	renderc.g = 1 - renderc.g;
	renderc.b = 1 - renderc.b;
	renderc.a = renderc.a;

	frag_colour = renderc;
}
