#version 400

layout(location = 0) in vec3 vp;   // Vertices

void main() {
	gl_Position = vec4(vp,0.0);
}
