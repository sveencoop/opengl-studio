#version 400
#define M_PI 3.1415926535897932384626433832795
#define M_E 2.71828182845904
#define M_IE 0.367879441171442


#define VTXSIZE 0.01f // Amplitude: Turbulencia da onda

#define WAVESIZE 10.0f  // Frequency: Tipo de repeti��o

#define FACTOR 0.2f //altura para cima e para baixo da onda
#define SPEED 1.0f //velocidade
#define OCTAVES 2 //quantidade de ondas: afeta muito a performance

#define MAX 3

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform float time;
uniform mat4 view_projection;
out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;
out float accY1;
out float accY2;


in VS_OUT{
	vec3 normal;
	vec2 texCoords;
	vec3 fragPos;
} gs_in[];

// Example of the same wave function used in the vertex engine

float voronoi(vec3 pos, float timer) {
	return pow(pow(pos.x, M_E) + pow(pos.y, M_E) + pow(pos.z, M_E), M_IE);
}

float wave(float x, float y, float timer,float speed){
	float z = 0.0f;
	float octaves = OCTAVES;
	float factor = FACTOR * SPEED;
	float d = sqrt(x * x + y * y);

	do {
		z -= factor * cos(timer * speed + (1 / factor) * x * y * WAVESIZE);
		factor = factor / 2.0;
		octaves--;
	} while (octaves > 0);

	return VTXSIZE * d * z;
}

float water_spiral_acc(float x, float z, float time) {
	x += time;
	z += time;
	return 2 * (cos(sqrt((x*x)+(z*z))) +
		cos(sqrt((x + 0.913 * 2 * M_PI)*(x + 0.913 * 2 * M_PI) + (z*z))) +
		cos(sqrt((x - 0.913 * 2 * M_PI)*(x - 0.913 * 2 * M_PI) + (z*z)))) * 0.15;
}

float water_river_acc(float x, float z, float time) {
	return 0.15*sin(x + z)*(sin(x + time) + cos(z + time));
}

vec3 GetNormal(vec4[3] position) {
	vec3 a = vec3(position[0]) - vec3(position[1]);
	vec3 b = vec3(position[2]) - vec3(position[1]);
	return normalize(cross(b, a));
}

void main() {

	vec4[MAX] acc;
	vec4[MAX] position;

	acc[0] = vec4(0.0, wave(gl_in[0].gl_Position.x, gl_in[0].gl_Position.z, time, SPEED), 0.0, 0.0);
	acc[1] = vec4(0.0, wave(gl_in[1].gl_Position.x, gl_in[1].gl_Position.z, time, SPEED), 0.0, 0.0);
	acc[2] = vec4(0.0, wave(gl_in[2].gl_Position.x, gl_in[2].gl_Position.z, time, SPEED), 0.0, 0.0);

	position[0] = gl_in[0].gl_Position + acc[0];
	position[1] = gl_in[1].gl_Position + acc[1];
	position[2] = gl_in[2].gl_Position + acc[2];

	vec3 normalface = GetNormal(position);
	int i = 0;

	for (i = 0; i < MAX; i++) {
		gl_Position = view_projection * position[i];
		Normal = normalface;
		accY1 = acc[i].y;
		accY2 = acc[i].y;
		FragPos = vec3(position[i]);
		TexCoord = gs_in[i].texCoords;
		EmitVertex();
	}

	EndPrimitive();
}