#version 400

layout(triangles) in;
layout(triangle_strip, max_vertices = 6) out;

uniform float time;
uniform mat4 view_projection;
out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;

in VS_OUT{
	vec3 normal;
	vec2 texCoords;
	vec3 fragPos;
} gs_in[];


vec3 GetNormal() {
	vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
	vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
	return normalize(cross(a, b));
}

void main() {
	vec4 normal = vec4(GetNormal(),0.0) * (sin(time)+1.0);
	
	for (int i = 0; i < 3; i++) {
		gl_Position = view_projection * (gl_in[i].gl_Position - normal);
		Normal = gs_in[i].normal;
		FragPos = gs_in[i].fragPos;
		TexCoord = vec2(gs_in[i].texCoords.x, gs_in[i].texCoords.y);
		EmitVertex();
	}

	EndPrimitive();

	for (int i = 2; i >=0 ; i--) {
		gl_Position = view_projection * (gl_in[i].gl_Position - normal);
		Normal = gs_in[i].normal;
		FragPos = gs_in[i].fragPos + vec3(normal);
		TexCoord = vec2(gs_in[i].texCoords.x, gs_in[i].texCoords.y);
		EmitVertex();
	}

	EndPrimitive();
}