#version 400

layout (location = 0) in vec3 vp;   // Vertices
layout (location = 1) in vec3 aNormal; // Normais
layout (location = 2) in vec2 aTexCoord; // Textura

out vec3 Normal;
out vec3 FragPos;  

out VS_OUT{
	vec3 normal;
	vec2 texCoords;
	vec3 fragPos;
} vs_out;

uniform mat4 transform;

void main() {
	gl_Position =  transform * vec4(vp, 1.0);
	Normal = mat3(transpose(inverse(transform))) * aNormal;
	vs_out.normal = Normal;
	vs_out.texCoords = aTexCoord;
    vs_out.fragPos = vec3(transform * vec4(vp, 1.0));
}
