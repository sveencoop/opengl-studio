#version 400

#define SIZE 20

layout(triangles) in;
layout(triangle_strip, max_vertices = 4) out;

uniform float time;
uniform mat4 view_projection;
out vec4 COLOR;


void main() {

	int timeint = int(time * 100);
	timeint = (timeint % 100);
	float accy = 1 - 2*(timeint / 100.0);
	vec4[4] offset;

	offset[0] = vec4(-0.05, -0.2 ,0.0, 0.0);
	offset[1] = vec4(-0.05, 0.2, 0.0, 0.0);
	offset[2] = vec4(0.05, -0.2, 0.0, 0.0);
	offset[3] = vec4(0.05, 0.2, 0.0, 0.0);
	
	vec4 color = vec4(1.0, 1.0, 1.0, 1.0);
			
	vec4 pos = SIZE * vec4(gl_in[0].gl_Position.x, accy, gl_in[0].gl_Position.z, 0.0);

	gl_Position = view_projection * (pos + offset[0]);
	EmitVertex();
			
	gl_Position = view_projection * (pos + offset[1]);
	EmitVertex();
			
	gl_Position = view_projection * (pos + offset[2]);
	EmitVertex();

	gl_Position = view_projection * (pos + offset[3]);
	EmitVertex();
			
	EndPrimitive();
	
}