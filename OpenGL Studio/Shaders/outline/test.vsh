#version 400

layout (location = 0) in vec3 vp;   // Vertices
layout (location = 1) in vec3 aNormal; //Normais

uniform mat4 transform;
uniform mat4 view;
uniform mat4 projection;

void main() {
	vec4 pos = projection * view  * transform * vec4(vp + normalize(aNormal) * 0.02, 1.0);
	gl_Position = pos.xyww;
}
