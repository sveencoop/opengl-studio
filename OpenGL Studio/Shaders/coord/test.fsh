#version 400

out vec4 frag_colour;

in vec3 FragPos;  

void main() {
	vec3 pos = (FragPos+1.0)/2.0;
    frag_colour = vec4(pos.x,pos.y,pos.z,1.0);
}