#version 400

layout (location = 0) in vec3 vp;   // Vertices
layout (location = 1) in vec3 aNormal; //Normais
layout (location = 2) in vec2 aTexCoord; // Textura

out vec3 FragPos;  

uniform mat4 transform;
uniform mat4 view_projection;

void main() {
	vec4 pos = view_projection * transform * vec4(vp, 1.0);
	gl_Position = pos;
	FragPos = vp/400.0;
}
