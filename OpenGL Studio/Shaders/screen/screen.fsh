#version 400

out vec4 frag_colour;

in vec2 TexCoord;

uniform sampler2D image;
uniform int horizontal;


float weight[6] = float[] (0.4 ,0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

vec3 blur() {
    vec2 tex_offset = 1.0 / textureSize(image, 0); 
    vec3 result = texture(image, TexCoord).rgb * weight[0];
    if(horizontal != 0) {
        for(int i = 1; i < 6; i++) {
            result += texture(image, TexCoord + vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
            result += texture(image, TexCoord - vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
        }
    } else {
        for(int i = 1; i < 6; i++)  {
            result += texture(image, TexCoord + vec2(0.0, tex_offset.y * i)).rgb * weight[i];
            result += texture(image, TexCoord - vec2(0.0, tex_offset.y * i)).rgb * weight[i];
        }
    }
    return result;
}

void main() {
	frag_colour = vec4(blur(), 1.0);
}
