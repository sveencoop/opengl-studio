#version 400

out vec4 frag_colour;

in vec2 TexCoord;

uniform sampler2D screenTexture;
uniform sampler2D bloomTexture;

uniform float gamma;
uniform float exposure;
uniform int bloom;

vec3 gamHdr() {
	
	vec3 hdrColor = texture(screenTexture, TexCoord.st).rgb;
	if (bloom != 0) {
		hdrColor += texture(bloomTexture, TexCoord.st).rgb;
	}
	vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);

	return pow(mapped, vec3(1.0 / gamma));
}

void main() {
	frag_colour = vec4(gamHdr(), 1.0);
}
