#version 400

struct Material {
	sampler2D diffmap;
	sampler2D specmap;
	sampler2D normalmap;
	int hasNormal;
	int withTexture;
};

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normals;
layout(location = 2) out vec4 out_albedo_n_spec;

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;
in mat3 TBN;

uniform Material material;

void main() {
	// store the fragment position vector in the first gbuffer texture
	out_position = FragPos;

	if (material.withTexture != 0) {
		// also store the per-fragment normals into the gbuffer
		if (material.hasNormal != 0) {
			vec3 color_normal = vec3(texture(material.normalmap, TexCoord)) * 2.0f - 1.0f;
			out_normals = normalize(TBN*color_normal);
		} else {
			out_normals = normalize(Normal);
		}
		// and the diffuse per-fragment color
		out_albedo_n_spec.rgb = texture(material.diffmap, TexCoord).rgb;
		// store specular intensity in gAlbedoSpec's alpha component
		out_albedo_n_spec.a = 1.0;
	} else {
		out_normals = normalize(Normal);
		out_albedo_n_spec = vec4(0.6, 0.0, 0.6, 1.0);
	}
}