#version 400

layout(location = 0) out vec4 frag_colour;
layout(location = 1) out vec4 BrightColor;

in vec2 TexCoord;

uniform sampler2D gbuffer_position;
uniform sampler2D gbuffer_normals;
uniform sampler2D gbuffer_albedospec;

void main() {
	frag_colour = vec4(texture(gbuffer_albedospec, TexCoord).rgb, 1.0);

	float brightness = dot(frag_colour.rgb, vec3(0.2126, 0.7152, 0.0722));
	if (brightness > 1.0) {
		BrightColor = vec4(frag_colour.rgb, 1.0);
	} else {
		BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
	}
}
