#version 400

layout (location = 0) in vec3 vp;   // Vertices
layout (location = 1) in vec3 aNormal; //Normais
layout (location = 2) in vec2 aTexCoord; // Textura
layout (location = 3) in vec3 aTangent; // Tangent
layout (location = 4) in vec3 aBitangent; // TexCordShadow
layout (location = 5) in vec2 aTexCoordShadow; // TexCordShadow

out vec2 TexCoord;
out vec2 TexCoordShadow;
out vec3 Normal;
out vec3 FragPos;
out vec4 FragPosLightSpace;
out mat3 TBN;

uniform mat4 transform;
uniform mat4 view_projection;
uniform mat4 lightSpaceMatrix;

void main() {

	gl_Position = view_projection * transform * vec4(vp, 1.0);
	TexCoord = aTexCoord;
	TexCoordShadow = aTexCoordShadow;

	vec3 T = vec3(normalize(transform * vec4(aTangent,1.0)));
	vec3 B = vec3(normalize(transform * vec4(aBitangent, 1.0)));
	vec3 N = vec3(normalize(transform * vec4(aNormal, 1.0)));

	TBN = mat3(T, B, N);

	Normal = mat3(transpose(inverse(transform))) * aNormal;
    FragPos = vec3(transform * vec4(vp, 1.0));
	FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
}
