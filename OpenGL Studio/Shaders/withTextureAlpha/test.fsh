#version 400

struct Material {
	sampler2D diffmap;
	sampler2D specmap;
	sampler2D reflexmap;
	sampler2D normalmap;
	sampler2D emissionMap;
	int hasNormal;
	int hasEmission;
	int withTexture;
	float shininess;
	float reflect;
	float IOR;
};

struct Spotlight {
	float intensity;

	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;
	float epsilon;

	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float radius;
};

struct Flashlight {
	float intensity;

	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float radius;
};

struct Pointlight {
	vec3 position;

	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float radius;
};

struct Sun {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
};

struct Fog {
	vec3 color;
	float max;
	float delta;
	float gap;
	int enabled;
};

layout(location = 0) out vec4 frag_colour;
layout(location = 1) out vec4 BrightColor;

in vec2 TexCoord;
in vec2 TexCoordShadow;
in vec3 Normal;
in vec3 FragPos;
in vec4 FragPosLightSpace;
in mat3 TBN;

uniform int NR_POINT_LIGHTS;
uniform int NR_SPOT_LIGHTS;

uniform Fog fog;

uniform Flashlight flashlight;
uniform Pointlight pointlight[50];
uniform Spotlight spotlight[50];
uniform Sun sun;
uniform Material material;
uniform vec3 viewPos;
uniform sampler2D shadowMap;
uniform samplerCube cubemap;
uniform float time;

vec3 viewDir;
vec3 norm;
vec4 tex;
vec4 spectex;

vec4 calcSun();
vec4 calcFlashlight();
vec4 calcPointlight(Pointlight light);
vec4 calcSpotlight(Spotlight light);
vec4 calcFog(vec4 atual);
vec3 calcReflection();
vec3 calcRefraction();

void main() {
	if (gl_FrontFacing) {
		if (material.withTexture != 0) {
			tex = texture(material.diffmap, TexCoord);
			if (material.hasNormal != 0) {
				vec3 color_normal = vec3(texture(material.normalmap, TexCoord)) * 2.0f - 1.0f;
				norm = normalize(TBN*color_normal);
			} else {
				norm = normalize(Normal);
			}

			spectex = texture(material.specmap, TexCoord);
		} else {
			tex = vec4(0.7, 0.7, 0.7, 1.0);
			spectex = vec4(1.0, 1.0, 1.0, 1.0);
			norm = normalize(Normal);
		}

		viewDir = normalize(viewPos - FragPos);

		vec4 resultSun = calcSun();
		vec4 resultLight;
		for (int i = 0; i < NR_POINT_LIGHTS; i++) {
			resultLight += calcPointlight(pointlight[i]);
		}

		for (int i = 0; i < NR_SPOT_LIGHTS; i++) {
			resultLight += calcSpotlight(spotlight[i]);
		}

		vec4 resultFlashlight = calcFlashlight();

		vec4 result = resultSun + resultLight + resultFlashlight;

		if (fog.enabled != 0) {
			result = calcFog(result);
		}

		result = vec4(result.rgb, tex.a);

		if (material.reflect != 0) {
			vec3 reflectr = calcReflection();
			result += vec4(reflectr, (reflectr.r + reflectr.g + reflectr.b) / 3.0);
		}

		if (material.IOR != 0) {
			vec3 refractr = calcRefraction();
			result += vec4(refractr, (refractr.r + refractr.g + refractr.b) / 3.0);
		}

		frag_colour = result;
	} else {
		frag_colour = vec4(0.6, 0.0, 0.6, 1.0);
	}

	float brightness = dot(frag_colour.rgb, vec3(0.2126, 0.7152, 0.0722));
	if (brightness > 1.0)
		BrightColor = vec4(frag_colour.rgb, 1.0);
	else
		BrightColor = vec4(0.0, 0.0, 0.0, 1.0);

}

vec3 calcReflection() {
	vec3 reflextex = vec3(texture(material.reflexmap, TexCoord));
	vec3 I = normalize(FragPos - viewPos);
	vec3 R = reflect(I, normalize(Normal));
	vec3 color = texture(cubemap, R).rgb;
	return color * reflextex;
}

vec3 calcRefraction() {
	float ratio = 1.00 / material.IOR;
	vec3 R = refract(viewDir, normalize(Normal), ratio);
	return texture(cubemap, R).rgb;
}


vec4 calcFog(vec4 atual) {
	float distance = length(viewPos - FragPos);

	distance = clamp((distance - fog.gap) / fog.delta, 0.0, 1.0);

	return vec4(fog.color*fog.max, 1.0)*distance + atual * (1 - distance);
}

vec4 calcSun() {

	vec3 lightDir = normalize(-sun.direction);
	float diff = max(dot(norm, lightDir), 0.0);
	vec4 diffuse = diff * tex;

	vec3 projCoords = FragPosLightSpace.xyz / FragPosLightSpace.w;

	if (projCoords.z > 1.0)
		return tex * vec4(sun.ambient, 1.0) + diffuse * vec4(sun.diffuse, 1.0);

	projCoords = projCoords * 0.5 + 0.5;

	float closestDepth = texture(shadowMap, projCoords.xy).r;

	float currentDepth = projCoords.z;

	float bias = max(0.0005 * (1.0 - diff), 0.0005);

	float shadow = 0.0;

	vec2 texelSize = 0.5 / textureSize(shadowMap, 0);
	for (int x = -1; x <= 1; ++x) {
		for (int y = -1; y <= 1; ++y) {
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
		}
	}
	shadow /= 9.0;

	return tex * vec4(sun.ambient, 1.0) + (1 - shadow) * diffuse * vec4(sun.diffuse, 1.0);
}

vec4 calcFlashlight() {
	if (flashlight.intensity != 0) {
		float distance = length(viewPos - FragPos);
		if (distance < flashlight.radius) {
			float theta = dot(viewDir, normalize(-flashlight.direction));

			float diff = max(dot(norm, viewDir), 0.0);
			vec4 diffuse = diff * tex;

			vec3 halfwayDir = viewDir;
			float spec = pow(max(dot(norm, halfwayDir), 0.0), material.shininess);
			vec4 specular = spec * spectex;

			float attenuation = 1.0 / (flashlight.constant + flashlight.linear * distance + flashlight.quadratic * (distance * distance));

			float epsilon = flashlight.cutOff - flashlight.outerCutOff;
			float intensity = clamp((theta - flashlight.outerCutOff) / epsilon, 0.0, 1.0);

			vec4 pointlight = clamp(flashlight.intensity * attenuation * (diffuse * vec4(flashlight.diffuse, 1.0) + specular * vec4(flashlight.specular, 1.0)), 0.0, 1.0);
			vec4 spotlight = clamp((diffuse * vec4(flashlight.diffuse, 1.0) + specular * vec4(flashlight.specular, 1.0)) * attenuation  * intensity * flashlight.intensity, 0.0, 1.0);

			return pointlight * 0.1 + spotlight;
		}
	}
	return vec4(0.0, 0.0, 0.0, 0.0);

}

vec4 calcSpotlight(Spotlight light) {

	if (light.intensity != 0) {
		float distance = length(light.position - FragPos);
		if (distance < light.radius) {
			vec3 lightDir = normalize(light.position - FragPos);
			float theta = dot(lightDir, normalize(-light.direction));

			float diff = max(dot(norm, lightDir), 0.0);
			vec4 diffuse = diff * tex;

			vec3 halfwayDir = normalize(lightDir + viewDir);
			float spec = pow(max(dot(norm, halfwayDir), 0.0), material.shininess);
			vec4 specular = spec * spectex;

			float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

			float intensity = clamp((theta - light.outerCutOff) / light.epsilon, 0.0, 1.0);

			return (diffuse * vec4(light.diffuse, 1.0) + specular * vec4(light.specular, 1.0)) * attenuation  * intensity * light.intensity;
		}
	}

	return vec4(0.0, 0.0, 0.0, 0.0);
}

vec4 calcPointlight(Pointlight light) {

	vec3 lightVetor = light.position - FragPos;
	vec3 lightDir = normalize(lightVetor);
	float distance = length(lightVetor);

	if (distance < light.radius) {
		float diff = max(dot(norm, lightDir), 0.0);
		vec4 diffuse = diff * tex;
		vec4 specular;
		if (spectex.a > 0.1) {
			vec3 halfwayDir = normalize(lightDir + viewDir);
			float spec = pow(max(dot(norm, halfwayDir), 0.0), material.shininess);
			specular = spec * spectex;
		} else {
			specular = vec4(0.0, 0.0, 0.0, 0.0);
		}
		float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
		return attenuation * (diffuse * vec4(light.diffuse, 1.0) + specular * vec4(light.specular, 1.0));
	} else {
		return vec4(0.0, 0.0, 0.0, 0.0);
	}
}