#version 400

out vec4 frag_colour;

in vec2 TexCoord;

uniform sampler2D screenTexture;

void main() {
	float depthValue = texture(screenTexture, TexCoord).r;
	frag_colour = vec4(vec3(depthValue), 1.0);
}
