#version 400

layout (location = 0) in vec3 vp;   // Vertices

uniform mat4 transform;
uniform mat4 view_projection;

void main() {
	gl_Position = view_projection * transform * vec4(vp, 1.0);
}
