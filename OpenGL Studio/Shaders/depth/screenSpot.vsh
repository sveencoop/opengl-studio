#version 400

layout(location = 0) in vec2 vp;   // Vertices
layout(location = 1) in vec2 aTexCoord; // Textura

out vec2 TexCoord;

void main() {
	gl_Position = vec4(vp.x, vp.y, 0, 1.0);
	TexCoord = aTexCoord;
}
