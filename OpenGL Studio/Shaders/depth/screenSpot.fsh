#version 400

out vec4 frag_colour;

in vec2 TexCoord;

uniform float near_plane;
uniform float far_plane;
uniform sampler2D screenTexture;

float LinearizeDepth(float depth) {
	float z = depth * 2.0 - 1.0; // Back to NDC 
	return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));
}

void main() {
	float depthValue = texture(screenTexture, TexCoord).r;
	frag_colour = vec4(vec3(LinearizeDepth(depthValue) / far_plane), 1.0);
}
