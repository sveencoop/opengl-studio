#version 330 core

//layout(location = 0) out vec3 out_position;
//layout(location = 1) out vec3 out_normals;
//layout(location = 2) out vec4 out_albedo_n_spec;
layout(location = 0) out vec4 frag_colour;
layout(location = 1) out vec4 BrightColor;

in vec3 textureDir; 
uniform samplerCube skytexture;

void main() {
	frag_colour = texture(skytexture, textureDir);
	//out_albedo_n_spec = texture(skytexture, textureDir);

	float brightness = dot(frag_colour.rgb, vec3(0.7126, 0.7152, 0.0722));
	if (brightness > 1.0)
		BrightColor = vec4(frag_colour.rgb, 1.0);
	else
		BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
}