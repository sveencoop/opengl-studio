#version 400

struct Material {
	sampler2D diffmap;
	sampler2D specmap;
	sampler2D reflexmap;
    float shininess;
	float reflect;
	float IOR;
}; 

struct Spotlight {
	float intensity;

    vec3 position;
    vec3 direction;
    float cutOff;
	float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
		
	float constant;
    float linear;
    float quadratic;
};

struct Flashlight {
	float intensity;

    vec3 position;
    vec3 direction;
    float cutOff;
	float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
		
	float constant;
    float linear;
    float quadratic;
};

struct Pointlight {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
		
	float constant;
    float linear;
    float quadratic;
};

struct Sun{
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
};

struct Fog{
	vec3 color;
	float max;
	float min;
	float delta;
	float gap;
	int enabled;
};

out vec4 frag_colour;

in vec2 TexCoord;
in vec3 Normal; 
in vec3 FragPos;  

uniform int NR_POINT_LIGHTS;
uniform int NR_SPOT_LIGHTS;

uniform Fog fog;

uniform Flashlight flashlight;
uniform Pointlight pointlight[50];
uniform Spotlight spotlight[50]; 
uniform Sun sun;  
uniform Material material;
uniform vec3 viewPos;
uniform samplerCube cubemap;
uniform float time;

vec3 viewDir;
vec3 norm;
vec4 tex;
vec4 spectex;

vec4 calcSun();
vec4 calcFlashlight();
vec4 calcPointlight(Pointlight light);
vec4 calcSpotlight(Spotlight light);
vec4 calcFog();

void main() {
	if(gl_FrontFacing){
        norm = normalize(Normal);
		tex = texture(material.diffmap, TexCoord);
		spectex = texture(material.specmap, TexCoord);

		viewDir = normalize(viewPos - FragPos);

		vec4 resultSun = calcSun();
		vec4 resultLight;
		for(int i = 0; i < NR_POINT_LIGHTS; i++){
			resultLight += calcPointlight(pointlight[i]);
		}
	
		for(int i = 0; i < NR_SPOT_LIGHTS; i++){
			resultLight += calcSpotlight(spotlight[i]);
		}

		vec4 resultFlashlight = calcFlashlight();

		vec4 result = resultSun + resultLight + resultFlashlight;

		if(fog.enabled != 0) {
			result += calcFog(); 
		}

		result = vec4(result.rgb,tex.a);

		frag_colour = result;
	} else {
        frag_colour = vec4(1.0,0.0,1.0,1.0);
	}
  
}

vec3 calcReflection() {
	vec3 reflextex = vec3(texture(material.reflexmap, TexCoord));
	vec3 I = normalize(FragPos - viewPos);
    vec3 R = reflect(I, normalize(Normal));
	vec3 color = texture(cubemap, R).rgb;
	return color * reflextex;
}

vec3 calcRefraction() {             
    float ratio = 1.00 / material.IOR;
    vec3 R = refract(viewDir, normalize(Normal), ratio);
    return texture(cubemap, R).rgb;
}  


vec4 calcFog(){
	float distance = length(viewPos - FragPos);

	distance = clamp((distance - fog.gap)/fog.delta,0.0,1.0);

	return vec4(fog.color*fog.max,1.0)*distance;
}

vec4 calcFlashlight(){
	if(flashlight.intensity != 0) {
		vec3 lightDir = viewDir;
		float theta = dot(lightDir, normalize(-flashlight.direction));
    
		float diff = max(dot(norm, lightDir), 0.0);
		vec4 diffuse = diff * tex;
	
		float distance = length(viewPos - FragPos);
		float attenuation = 1.0 / (flashlight.constant + flashlight.linear * distance + flashlight.quadratic * (distance * distance)); 

		float epsilon   = flashlight.cutOff - flashlight.outerCutOff;
		float intensity = clamp((theta - flashlight.outerCutOff) / epsilon, 0.0, 1.0);        

		vec4 pointlight = clamp(flashlight.intensity * attenuation * (vec4(flashlight.ambient,1.0) * tex + diffuse * vec4(flashlight.diffuse,1.0)), 0.0, 1.0);
		vec4 spotlight = clamp((tex * vec4(flashlight.ambient,1.0) + diffuse * vec4(flashlight.diffuse,1.0)) * attenuation  * intensity * flashlight.intensity, 0.0, 1.0);

		return pointlight*0.1 + spotlight;
		
	} else {
		return vec4(0.0,0.0,0.0,0.0);
	}
}

vec4 calcSun() {

	vec3 lightDir = normalize(-sun.direction);
	float diff = max(dot(norm, lightDir), 0.0);
	vec4 diffuse = diff * tex;

	return tex * vec4(sun.ambient, 1.0) + diffuse * vec4(sun.diffuse, 1.0);
}

vec4 calcSpotlight(Spotlight light){
	if(light.intensity != 0) {
		vec3 lightDir = normalize(light.position - FragPos);
		float theta = dot(lightDir, normalize(-light.direction));
    
		float diff = max(dot(norm, lightDir), 0.0);
		vec4 diffuse = diff * tex;

		float distance = length(light.position - FragPos);
		float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance)); 

		float epsilon   = light.cutOff - light.outerCutOff;
		float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);        

		return clamp((tex * vec4(light.ambient,1.0) + diffuse * vec4(light.diffuse,1.0)) * attenuation  * intensity * light.intensity, 0.0, 1.0);
	} else {
		return vec4(0.0,0.0,0.0,0.0);
	}
}

vec4 calcPointlight(Pointlight light) {
	
	vec3 lightDir = normalize(light.position - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec4 diffuse = diff * tex;

	float distance = length(light.position - FragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance)); 

	return clamp(attenuation * (vec4(light.ambient,1.0) * tex + diffuse * vec4(light.diffuse,1.0)), 0.0, 1.0);
}