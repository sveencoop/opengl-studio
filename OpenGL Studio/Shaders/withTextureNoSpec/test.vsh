#version 400

layout (location = 0) in vec3 vp;   // Vertices
layout (location = 1) in vec3 aNormal; //Normais
layout (location = 2) in vec2 aTexCoord; // Textura

out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;  

uniform mat4 transform;
uniform mat4 view_projection;

void main() {

	gl_Position = view_projection * transform * vec4(vp, 1.0);
	TexCoord = aTexCoord;
	Normal = mat3(transpose(inverse(transform))) * aNormal;
    FragPos = vec3(transform * vec4(vp, 1.0));
}
