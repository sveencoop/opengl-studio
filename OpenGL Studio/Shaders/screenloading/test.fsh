#version 400

out vec4 frag_colour;

in vec2 TexCoord;

uniform sampler2D screenTexture;
uniform float loading;
uniform float time;

void main() {
	vec4 image = vec4(texture(screenTexture, TexCoord));
	if(loading <= TexCoord.x) {
		image.a = image.a*(sin(TexCoord.x*20 + time*6)+1)/8.0;
	}
	frag_colour = image;
}
