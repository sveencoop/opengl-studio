#pragma once
#include <wx/wx.h>
#include <wx/wxprec.h>
#include <wx/stattext.h>
#include <wx/notebook.h>
#include <wx/treectrl.h>
#include <wx/clrpicker.h>
#include <wx/spinctrl.h>
#include <wx/slider.h>
#include <wx/propgrid/propgrid.h>

#include "Core.h"
#include <vector>
#include <map>

const int ID_FOG_ENABLED = 1;
const int ID_FOG_MIN = 2;
const int ID_FOG_MAX = 3;
const int ID_FOG_DELTA = 4;
const int ID_FOG_GAP = 5;
const int ID_FOG_COLOR = 6;

const int ID_SUN_AMBIENT = 8;
const int ID_SUN_DIFFUSE = 9;
const int ID_SUN_POSITION = 10;

const int ID_GAMMA = 14;
const int ID_HDR = 15;
const int ID_CULL_FACE = 16;
const int ID_BLOOM = 17;
const int ID_NORMALMAP = 18;

const int ID_TREE_OBJECT = 100;
const int ID_TREE_LIGHT = 101;

const int ID_EDIT_OBJECT = 200;

struct TreeObject {
	dataImport *importedData;
	wxTreeItemId id;
	std::map<wxTreeItemId,Object3D*> meshes;
};

struct TreeLight {
	dataImport *importedData;
	wxTreeItemId id;
	std::vector<Light*> lights;
};

class wxCoord3D : public wxFlexGridSizer {
private:
	const int ID_X = 1;
	const int ID_Y = 2;
	const int ID_Z = 3;
	wxPanel *panel;
	void(*callback)(float x, float y, float z);
	wxSpinCtrlDouble *x_input;
	wxSpinCtrlDouble *y_input;
	wxSpinCtrlDouble *z_input;
public:
	wxCoord3D(wxPanel *panel,double min,double max);
	void change(wxFrame *frame, wxEventFunction event);
	void setValue(double x, double y, double z);
	float getX();
	float getY();
	float getZ();
};

class EditorSidepanel : public wxFrame, InterfaceInfoIO, InterfaceIO {
private:
	wxStaticText *fps_label;

	wxTextCtrl* console;
	wxTreeCtrl *treeObjects;
	wxTreeCtrl *treeLights;

	//LISTS
	std::map<Object3D*, wxTreeItemId> mapObject;
	std::vector<TreeObject*> importedObjs;
	std::vector<TreeLight> importedLights;

	//FOG
	Fog *fog_control;

	wxPropertyGrid *propertyGrid;
	wxCheckBox *enable_fog;
	wxColourPickerCtrl *color_fog;
	wxSlider *max_fog;
	wxSpinCtrlDouble *delta_fog;
	wxSpinCtrlDouble *gap_fog;
	void updateFog();

	//SUN
	Sun *sun_control;

	wxColourPickerCtrl *color_sun_ambient;
	wxColourPickerCtrl *color_sun_diffuse;
	wxCoord3D *position_sun;

	//GAMMA
	wxSlider *hdr;
	wxSlider *gamma;
	wxCheckBox *cull_face;
	wxCheckBox *bloom;
	wxCheckBox *normalmap;

	//EVENTOS
	void clickObject(wxTreeEvent& event);
	void fog_enabled_event(wxCommandEvent &event);
	void fog_color_event(wxColourPickerEvent &event);
	void fog_max_event(wxScrollEvent &event);
	void fog_gap_event(wxSpinDoubleEvent &event);
	void fog_delta_event(wxSpinDoubleEvent &event);

	void sun_ambient_event(wxColourPickerEvent &event);
	void sun_diffuse_event(wxColourPickerEvent &event);
	void sun_position_change(wxSpinDoubleEvent &event);

	void gamma_event(wxScrollEvent &event);
	void hdr_event(wxScrollEvent &event);
	void cull_face_event(wxCommandEvent &event);
	void normal_mapping_event(wxCommandEvent &event);
	void bloom_event(wxCommandEvent &event);

	void close_event(wxCloseEvent &event);

	UINT32 rgbToUint32(float *rgb);

public:
	EditorSidepanel(const wxString &title);
	~EditorSidepanel();
	void createDetailsBar(wxBoxSizer *principal);

	void createPageConfiguration(wxNotebook *parent);
	void createPageEdit(wxNotebook *parent);
	void createPageConsole(wxNotebook *parent);

	void addImport(dataImport *data);
	void addObject(Object3D *object,unsigned int pos);
	void addLight(std::vector<Light*> lights,unsigned int pos);

	wxPanel* createPageEditAmbience(wxNotebook *parent);
	wxPanel* createPageEditObjects(wxNotebook *parent);
	wxPanel* createPegaEditIlumination(wxNotebook *parent);

	//STYLE
	static wxWindow *style(wxWindow *obj);

	//INTERFACE IO
	void echo(val_output key, const char* value);
	void echo(const char* mensagem, message_type type);
	void echo_action(action_type action, Object3D *object, unsigned int pos);
	void echo_action(action_type action, std::vector<Light*> lights, unsigned int pos);
	void echo_action(action_type action, Fog *fog);
	void echo_action(action_type action, Sun *sun);

	void appendConsole(const char* mensagem);
};

