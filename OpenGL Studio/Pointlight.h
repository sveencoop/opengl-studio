#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Light.h"

class Pointlight : public Light{
	private:
		unsigned int attr_ambient;
		unsigned int attr_diffuse;
		unsigned int attr_specular;
		unsigned int attr_position;
		unsigned int attr_constant;
		unsigned int attr_linear;
		unsigned int attr_quadratic;
		unsigned int attr_radius;

	public:
		Pointlight();
		void setAttenuation(float constant, float linear, float quadratic);
		void setPosition(float x,float y,float z);
		void setPosition(glm::vec3 *pos);

		void scale(float *scale);
		void rotate(float *rotate);
		void translate(float *translate);

		void initAttrs(unsigned int pos);
		void init(glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular);
		void shaderingData(unsigned int shader);
		void apply();
		const char* getType();
};

