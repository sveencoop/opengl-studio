#include "InputEvents.h"
#include "timer.h"
#include "Core.h"
#include "InterfaceIO.h"
#include <math.h> 

void InputEvents::interact(Replica* m) {
	if (this->item_grab == nullptr) {
		if (m->rigid_body != nullptr) {
			this->item_grab = m;
			float *matriz = glm::value_ptr(this->item_grab->rotateMatriz);

			double obj_yaw = atan2(matriz[4], matriz[0]);
			double obj_pitch = atan2(-matriz[8], sqrt(matriz[9] * matriz[9] + matriz[10] * matriz[10]));
			double obj_roll = atan2(matriz[9], matriz[10]);

			this->item_grab_yaw = obj_yaw - glm::radians(this->cam->getYaw());
			this->item_grab_pitch = obj_pitch - glm::radians(this->cam->getPitch());
			this->item_grab_roll = obj_roll;

			dBodySetLinearVel(this->item_grab->rigid_body, 0, 0, 0);
			dBodySetAngularVel(this->item_grab->rigid_body, 0, 0, 0);
			dBodySetGravityMode(this->item_grab->rigid_body, 0);
		}
	} else {
		dBodySetGravityMode(this->item_grab->rigid_body, 1);
		this->item_grab = nullptr;
	}
}

void InputEvents::update_grab() {
	if (this->item_grab != nullptr) {
		glm::vec3 pos = this->cam->getPosition() + this->cam->getFront() * 3.0f;

		//const dReal *oldpos = dBodyGetPosition(this->item_grab->rigid_body);
		//glm::vec3 vel = pos - glm::vec3(oldpos[0],oldpos[1],oldpos[2]);
		dBodySetPosition(this->item_grab->rigid_body, pos.x, pos.y, pos.z);
		//dBodySetLinearVel(this->item_grab->rigid_body, vel.x, vel.y, vel.z);

		dMatrix3 R;
		dRFromEulerAngles(R, glm::radians(this->cam->getPitch()) + this->item_grab_pitch, glm::radians(this->cam->getYaw()) + this->item_grab_yaw, this->item_grab_roll);
		//dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, -glm::radians(this->cam->getYaw()) - this->item_grab_yaw);
		//dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, 0.0);

		//dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, M_PI / 2.0);

		dBodySetRotation(this->item_grab->rigid_body, R);
		dBodySetLinearVel(this->item_grab->rigid_body, 0.0, 0.0, 0.0);
		dBodySetAngularVel(this->item_grab->rigid_body, 0.0, 0.0, 0.0);

	}
}

InputEvents::InputEvents() {
	this->item_grab = nullptr;
	this->second_button = false;
	this->shooting = false;
}

void InputEvents::bindCamera(Camera *newcam) {
	this->cam = newcam;
}

void InputEvents::setWindow(GLFWwindow *newwindow, int SCR_WIDTH, int SCR_HEIGHT) {
	this->window = newwindow;
	this->lastX = SCR_WIDTH / 2;
	this->lastY = SCR_HEIGHT / 2;
}

void InputEvents::applyKeyboard(double deltaTime) {

	double cameraSpeed = 10.0f * deltaTime; // adjust accordingly
	this->running = false;
	this->moving = false;
	this->strafing = false;
	this->backward = false;

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		cameraSpeed *= 1.75;
		this->running = true;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		this->cam->accYaw(-0.03);
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		this->cam->accYaw(0.03);
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		this->cam->foward(cameraSpeed);
		cameraSpeed *= 0.70;//sqrt(2)/2,v->1
		this->moving = true;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		this->cam->backward(cameraSpeed);
		cameraSpeed *= 0.70;//sqrt(2)/2,v->1
		this->backward = true;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		this->cam->left(cameraSpeed);
		this->strafing = true;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		this->cam->right(cameraSpeed);
		this->strafing = true;
	}

	if (this->moving && this->running) {
		this->cam->setAngle(82.0);
	} else {
		this->cam->setAngle(78.0);
	}

	this->cam->compile();
	this->update_grab();
}

void InputEvents::applyCharTyped(unsigned int key) {

}
//Chamada ao N�cleo causa redundancia
//switch case torna a sele��o inflexivel
void InputEvents::applyKeyTyped(int key, int scancode, int action, int mods) {
	if (action == GLFW_PRESS) {
		switch (key) {
		case GLFW_KEY_F:
			engine::toggleFlashlight();
			break;
		case GLFW_KEY_Q:
			engine::next_hand_item();
			break;
		case GLFW_KEY_F1:
			engine::toggleFlyMode();
			break;
		case GLFW_KEY_ESCAPE:
			engineWindow::blur();
			break;
		case GLFW_KEY_F2:
			engineWindow::focus();
			break;
		case GLFW_KEY_E:
			Replica *r = engine::pickObject();
			if (r != nullptr) {
				this->interact(r);
			}
			break;
		}
	}
}

void InputEvents::applyClick(int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		if (this->item_grab != nullptr) {
			glm::vec3 front = this->cam->getFront() * 10.0f;
			dBodySetGravityMode(this->item_grab->rigid_body, 1);
			dBodySetForce(this->item_grab->rigid_body, front.x, front.y, front.z);
			dBodySetAngularVel(this->item_grab->rigid_body, front.z / 2.0f, -front.y / 2.0f, -front.x / 2.0f);
			this->item_grab = nullptr;
		} else {
			if (action == GLFW_PRESS) {
				this->shooting = true;
				engine::trigger_shoot();
			} else if (action == GLFW_RELEASE) {
				this->shooting = false;
			}
		}
	} else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
		if (action == GLFW_PRESS) {
			this->second_button = true;
		} else if(action == GLFW_RELEASE) {
			this->second_button = false;
		}
	} 
}

double toRadians = (M_PI / 180.0)*0.05f;

void InputEvents::applyMouse(double xpos, double ypos) {
	//if (firstMouse) {
	//	lastX = xpos;
	//	lastY = ypos;
	//	firstMouse = false;
	//} else {
	this->cam->accYaw((xpos - lastX)*toRadians);
	this->cam->accPitch((lastY - ypos)*toRadians);
	lastX = xpos;
	lastY = ypos;
	this->cam->compile();
	this->update_grab();
	//}
}
