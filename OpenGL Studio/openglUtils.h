#pragma once
#include <glad\glad.h>

namespace openglUtils {
	void use(unsigned int shader_programme, unsigned int vertex_data);
	void setVec3(unsigned int shader_programme, const char * name, const float *vec);
	void setTexture(unsigned int index, unsigned int id);
	void setMatriz(unsigned int shader_programme, const char * name, const float * value);
	void setFloat(unsigned int shader_programme, const char *name, float value);
	void setInt(unsigned int shader_programme, const char *name, int value);
};