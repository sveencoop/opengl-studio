#include "ImportDialog.h"

ImportDialog::ImportDialog(const wxString & title)
	: wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 300)) {

	this->atualInst = nullptr;
	this->countInst = 0;
	this->confirmAction = false;
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
	this->tabs = new wxNotebook(this, -1, wxDefaultPosition, wxDefaultSize, wxNB_TOP);

	wxStaticText* obs_label = new wxStaticText(this, -1, wxT("Obs: Arquivos OBJ n�o possuem recursos de ilumina��o, cameras ou anima��es !"));
	obs_label->Wrap(300);

	wxString path = wxGetHomeDir() + wxT("\\");
	this->filepicker = new wxFilePickerCtrl(this, ID_FILE, path, "Selecione o arquivo 3D", "FBX files (*.fbx)|*.fbx|OBJ Wavefront files (*.obj)|*.obj");

	this->filepicker->SetInitialDirectory(path);
	this->filepicker->SetPath(path);
	this->filepicker->SetExtraStyle(wxFLP_FILE_MUST_EXIST | wxFLP_OPEN);

	vbox->Add(new wxStaticText(this, -1, "Arquivo do objeto"), 0, wxTOP | wxLEFT, 10);
	vbox->Add(filepicker, 0, wxLEFT | wxRIGHT | wxEXPAND, 10);
	vbox->Add(obs_label, 0, wxLEFT | wxRIGHT | wxBOTTOM, 10);

	this->InstTree = new wxTreeCtrl(this, ID_INSTTREE);

	wxBoxSizer *hbox_tree = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer *hbox_tree_1 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *hbox_tree_2 = new wxBoxSizer(wxVERTICAL);

	this->button_novo = new wxButton(this, ID_ADDINST, "Novo");
	this->button_rem = new wxButton(this, ID_REMINST, "Remover");

	hbox_tree_1->Add(this->button_novo);
	hbox_tree_1->Add(this->button_rem);

	hbox_tree_2->Add(this->InstTree, 1, wxEXPAND);

	hbox_tree->Add(hbox_tree_1, 0.2, wxRIGHT, 5);
	hbox_tree->Add(hbox_tree_2, 1, wxLEFT, 5);

	vbox->Add(hbox_tree, 0, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 10);

	this->initPanelTransform(this->tabs);
	this->initPanelFisica(this->tabs);

	vbox->Add(this->tabs, 1, wxEXPAND);

	this->tabs->Disable();
	this->tabs->SetSelection(0);
	this->okButton = new wxButton(this, ID_OK, wxT("Importar"), wxDefaultPosition, wxSize(70, 30));
	this->okButton->Disable();

	wxButton *closeButton = new wxButton(this, ID_CANCEL, wxT("Cancelar"), wxDefaultPosition, wxSize(70, 30));

	this->Connect(ID_FILE, wxEVT_COMMAND_FILEPICKER_CHANGED, wxFileDirPickerEventHandler(ImportDialog::OnFilePickerChanged));
	this->Connect(ID_OK, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ImportDialog::OnConfirm));
	this->Connect(ID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ImportDialog::OnCancel));
	this->Connect(ID_ADDINST, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ImportDialog::OnAddInst));
	this->Connect(ID_REMINST, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ImportDialog::OnRemoveInst));
	this->Connect(ID_INSTTREE, wxEVT_TREE_SEL_CHANGED, wxTreeEventHandler(ImportDialog::clickInst));
	this->Connect(ID_INSTSPIN, wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler(ImportDialog::choiceInst));

	wxTreeItemId RootId = this->InstTree->AddRoot("Inst�ncias");

	wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

	this->InstTree->Disable();
	this->instChoice->Disable();
	this->button_novo->Disable();
	this->button_rem->Disable();

	hbox->Add(this->okButton, 0);
	hbox->Add(closeButton, 0, wxLEFT, 10);
	vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 20);

	vbox->SetSizeHints(this);

	this->SetSizer(vbox);
	this->Centre();
}

void ImportDialog::initPanelTransform(wxNotebook * parent) {

	wxPanel *panel = new wxPanel(parent);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	wxFlexGridSizer *fgs_matrix = new wxFlexGridSizer(4, 4, 3, 3);

	wxArrayString instChoices;

	this->instChoice = new wxChoice(panel, ID_INSTSPIN, wxDefaultPosition, wxSize(50, 20), instChoices);

	wxBoxSizer *hbox_spin = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *text = new wxStaticText(panel, -1, "Visualizando inst�ncia : ", wxDefaultPosition, wxSize(-1, 20));
	text->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	hbox_spin->Add(text, 0, wxRIGHT, 10);
	hbox_spin->Add(this->instChoice);

	vbox->Add(hbox_spin, 0, wxALL, 10);

	wxStaticText* title_label = new wxStaticText(panel, -1, wxT(""));

	wxStaticText* translate_label = new wxStaticText(panel, -1, wxT("Translate (Unit)"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	wxStaticText* scale_label = new wxStaticText(panel, -1, wxT("Scale (Absolute)"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	wxStaticText* rotate_label = new wxStaticText(panel, -1, wxT("Rotate (Degrees)"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);

	wxStaticText* x_label = new wxStaticText(panel, -1, wxT("X (Left/Right)"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	wxStaticText* y_label = new wxStaticText(panel, -1, wxT("Y (Up/Down)"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	wxStaticText* z_label = new wxStaticText(panel, -1, wxT("Z (Front/Back)"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

	wxSize sizeCell = wxSize(90, 20);

	//TRANSLATE
	this->translate_x = new wxSpinCtrlDouble(panel, -1, "0.0", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL, -400, 400);
	this->translate_y = new wxSpinCtrlDouble(panel, -1, "0.0", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL, -400, 400);
	this->translate_z = new wxSpinCtrlDouble(panel, -1, "0.0", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL, -400, 400);

	//SCALE
	this->scale_y = new wxSpinCtrlDouble(panel, -1, "1.00", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL);
	this->scale_x = new wxSpinCtrlDouble(panel, -1, "1.00", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL);
	this->scale_z = new wxSpinCtrlDouble(panel, -1, "1.00", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL);

	//ROTATE
	this->rotate_x = new wxSpinCtrlDouble(panel, -1, "0.0", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL, -360, 360);
	this->rotate_y = new wxSpinCtrlDouble(panel, -1, "0.0", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL, -360, 360);
	this->rotate_z = new wxSpinCtrlDouble(panel, -1, "0.0", wxDefaultPosition, sizeCell, wxALIGN_CENTRE_HORIZONTAL, -360, 360);

	fgs_matrix->Add(title_label, 0, wxEXPAND);
	fgs_matrix->Add(x_label, 0, wxEXPAND);
	fgs_matrix->Add(y_label, 0, wxEXPAND);
	fgs_matrix->Add(z_label, 0, wxEXPAND);

	fgs_matrix->Add(translate_label, 0, wxEXPAND);
	fgs_matrix->Add(translate_x, 0, wxEXPAND);
	fgs_matrix->Add(translate_y, 0, wxEXPAND);
	fgs_matrix->Add(translate_z, 0, wxEXPAND);

	fgs_matrix->Add(scale_label, 0, wxEXPAND);
	fgs_matrix->Add(scale_x, 0, wxEXPAND);
	fgs_matrix->Add(scale_y, 0, wxEXPAND);
	fgs_matrix->Add(scale_z, 0, wxEXPAND);

	fgs_matrix->Add(rotate_label, 0, wxEXPAND);
	fgs_matrix->Add(rotate_x, 0, wxEXPAND);
	fgs_matrix->Add(rotate_y, 0, wxEXPAND);
	fgs_matrix->Add(rotate_z, 0, wxEXPAND);
	vbox->Add(fgs_matrix, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
	vbox->AddSpacer(10);
	panel->SetSizer(vbox);

	parent->AddPage(panel, "Inst�ncias", true);

}

void ImportDialog::initPanelFisica(wxNotebook* parent) {

	wxPanel *panel = new wxPanel(parent);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	this->gravity = new wxCheckBox(panel, -1, wxT("Aplicar f�sica"));

	this->bounding_box = new wxChoice(panel, -1);
	wxArrayString bb_types_string;

	bb_dt *bb_datatype = new bb_dt;

	for (physic_type *bb : *bb_datatype->tipos_bounding_box) {
		bb_types_string.Add(bb->nome);
	}
	this->bounding_box->Insert(bb_types_string, 0);
	this->bounding_box->SetSelection(0);

	wxString path = wxGetHomeDir() + wxT("\\");
	this->filepicker_boundingbox_physics = new wxFilePickerCtrl(panel, ID_FILE, path, "Selecione o arquivo 3D", "FBX files (*.fbx)|*.fbx|OBJ Wavefront files (*.obj)|*.obj");

	this->filepicker_boundingbox_physics->SetInitialDirectory(path);
	this->filepicker_boundingbox_physics->SetPath(path);
	this->filepicker_boundingbox_physics->SetExtraStyle(wxFLP_FILE_MUST_EXIST | wxFLP_OPEN);

	this->mass_physics = new wxSpinCtrlDouble(panel, -1, "1.0", wxDefaultPosition, wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL, 0.1, 10);

	vbox->AddSpacer(5);
	vbox->Add(this->gravity, 0, wxRIGHT, 10);
	vbox->AddSpacer(5);
	vbox->Add(new wxStaticText(panel, -1, "Tipo de Bounding Box"));
	vbox->Add(this->bounding_box, 0, wxRIGHT, 10);
	vbox->AddSpacer(5);
	vbox->Add(new wxStaticText(panel, -1, "Usar objeto simples para o bounding box em vez do objeto"));
	vbox->Add(this->filepicker_boundingbox_physics, 0, wxRIGHT | wxEXPAND, 10);
	vbox->AddSpacer(5);
	vbox->Add(new wxStaticText(panel, -1, "Massa (Kg)"));
	vbox->Add(this->mass_physics, 0, wxRIGHT, 10);
	vbox->AddSpacer(5);

	panel->SetSizer(vbox);
	parent->AddPage(panel, "F�sica", true);
}

void ImportDialog::OnConfirm(wxCommandEvent & WXUNUSED(event)) {
	if (!this->filepicker->GetFileName().FileExists()) {
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("Arquivo n�o existe! O campo do arquivo est� informado corretamete ?!"), wxT("Opss"),
			wxOK | wxICON_EXCLAMATION);
		dial->SetMaxSize(wxSize(200, 200));

		dial->ShowModal();
	} else {
		this->saveInst();
		this->confirmAction = true;
		this->Hide();
	}
}

void ImportDialog::OnCancel(wxCommandEvent & WXUNUSED(event)) {
	this->saveInst();
	this->confirmAction = false;
	this->Hide();
}

void ImportDialog::OnFilePickerChanged(wxFileDirPickerEvent & event) {
	this->listItem.clear();
	this->listInst.clear();
	this->novaInst();
	this->EnableMatriz();
	this->InstTree->Expand(this->InstTree->GetRootItem());
}

void ImportDialog::OnClose(wxCloseEvent & event) {
	this->confirmAction = false;
	this->Destroy();
}

void ImportDialog::OnAddInst(wxCommandEvent & WXUNUSED(event)) {
	if (this->atualInst != nullptr) {
		this->saveInst();
	}
	this->novaInst();
}

void ImportDialog::OnRemoveInst(wxCommandEvent & WXUNUSED(event)) {
	this->removeInst();
}

void ImportDialog::clickInst(wxTreeEvent & event) {
	this->saveInst();
	instDataImport *inst = this->getInst(event.GetItem());
	if (inst != nullptr) {
		this->selectInst(inst);
	}
}

void ImportDialog::choiceInst(wxCommandEvent& event) {
	int id = event.GetSelection();
	instDataImport* inst = this->listInst.at(id);
	this->InstTree->SelectItem(this->listItem.at(id),true);
	/*if (inst != NULL) {
		this->selectInst(inst);
	}*/
}

void ImportDialog::EnableMatriz() {

	this->tabs->Enable();
	this->okButton->Enable();
	this->InstTree->Enable();
	this->instChoice->Enable();
	this->button_novo->Enable();
	this->button_rem->Enable();

}

int ImportDialog::indexOf(instDataImport * inst) {
	int pos = 0;
	for (instDataImport *search : this->listInst) {
		if (search == inst) {
			return pos;
		}
		pos++;
	}
	return -1;
}

int ImportDialog::indexOf(wxTreeItemId item) {
	int pos = 0;
	for (wxTreeItemId search : this->listItem) {
		if (search == item) {
			return pos;
		}
		pos++;
	}
	return -1;
}

instDataImport * ImportDialog::getInst(wxTreeItemId item) {
	int pos = this->indexOf(item);
	if (pos != -1) {
		return this->listInst.at(pos);
	}
	return nullptr;
}

wxTreeItemId ImportDialog::getItem(instDataImport * inst) {
	int pos = this->indexOf(inst);
	if (pos != -1) {
		return this->listItem.at(pos);
	}
	return wxTreeItemId(nullptr);
}

void ImportDialog::saveInst() {

	this->atualInst->rotate[0] = this->rotate_x->GetValue();
	this->atualInst->rotate[1] = this->rotate_y->GetValue();
	this->atualInst->rotate[2] = this->rotate_z->GetValue();

	this->atualInst->translate[0] = this->translate_x->GetValue();
	this->atualInst->translate[1] = this->translate_y->GetValue();
	this->atualInst->translate[2] = this->translate_z->GetValue();

	this->atualInst->scale[0] = this->scale_x->GetValue();
	this->atualInst->scale[1] = this->scale_y->GetValue();
	this->atualInst->scale[2] = this->scale_z->GetValue();

}

void ImportDialog::novaInst() {
	instDataImport *inst = new instDataImport;

	inst->rotate[0] = 0;
	inst->rotate[1] = 0;
	inst->rotate[2] = 0;
	inst->translate[0] = 0;
	inst->translate[1] = 0;
	inst->translate[2] = 0;
	inst->scale[0] = 1;
	inst->scale[1] = 1;
	inst->scale[2] = 1;
	
	int max = this->countInst;
	this->countInst++;
	this->instChoice->Insert(std::string("#") + std::to_string(max), this->listItem.size());
	wxTreeItemId item = this->InstTree->AppendItem(this->InstTree->GetRootItem(), std::string("Inst�ncia #") + std::to_string(max));
	this->listItem.push_back(item);
	this->listInst.push_back(inst);
	this->selectInst(inst);

}

void ImportDialog::removeInst() {

	if (this->listInst.size() == 1) {
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("N�o � possivel remover a �nica instancia do objeto"), wxT("Opss"),
			wxOK | wxICON_EXCLAMATION);
		dial->SetMaxSize(wxSize(200, 200));
		dial->ShowModal();
		return;
	}

	int pos = this->indexOf(this->atualInst);

	if (pos != -1) {

		if (pos + 1 != this->listInst.size()) {
			this->selectInst(pos + 1);
		} else {
			this->selectInst(pos - 1);
		}
		
		instDataImport *inst = this->listInst.at(pos);
		this->InstTree->Delete(this->listItem.at(pos));
		this->instChoice->Delete(pos);
		this->listInst.erase(this->listInst.begin() + pos);
		this->listItem.erase(this->listItem.begin() + pos);
		
		free(inst);
		inst = nullptr;
	}
}

void ImportDialog::selectInst(instDataImport *inst) {
	this->atualInst = inst;

	int pos = this->indexOf(inst);
	this->instChoice->SetSelection(pos);

	this->rotate_x->SetValue(this->atualInst->rotate[0]);
	this->rotate_y->SetValue(this->atualInst->rotate[1]);
	this->rotate_z->SetValue(this->atualInst->rotate[2]);

	this->translate_x->SetValue(this->atualInst->translate[0]);
	this->translate_y->SetValue(this->atualInst->translate[1]);
	this->translate_z->SetValue(this->atualInst->translate[2]);

	this->scale_x->SetValue(this->atualInst->scale[0]);
	this->scale_y->SetValue(this->atualInst->scale[1]);
	this->scale_z->SetValue(this->atualInst->scale[2]);

}

void ImportDialog::selectInst(int pos) {
	this->selectInst(this->listInst.at(pos));
}

void ImportDialog::setData(dataImport data) {

	double taxa = 1.0;
	if (wxString(data.filename).EndsWith(".fbx") || wxString(data.filename).EndsWith(".FBX")) {
		taxa = 0.01;
	}

	this->listInst.clear();
	this->listItem.clear();

	wxTreeItemId root = this->InstTree->GetRootItem();

	instDataImport *first = nullptr;
	this->countInst = 0;
	for (instDataImport inst : data.insts) {

		instDataImport *revertInst = new instDataImport;
		revertInst->rotate[0] = 360.0 * (inst.rotate[0] / (2 * M_PI));
		revertInst->rotate[1] = 360.0 * (inst.rotate[1] / (2 * M_PI));
		revertInst->rotate[2] = 360.0 * (inst.rotate[2] / (2 * M_PI));

		revertInst->translate[0] = inst.translate[0];
		revertInst->translate[1] = inst.translate[1];
		revertInst->translate[2] = inst.translate[2];

		revertInst->scale[0] = (inst.scale[0] / taxa);
		revertInst->scale[1] = (inst.scale[1] / taxa);
		revertInst->scale[2] = (inst.scale[2] / taxa);

		if (first == nullptr) {
			first = revertInst;
		}

		wxTreeItemId item = this->InstTree->AppendItem(root, std::string("Inst�ncia #") + std::to_string(this->countInst));
		this->listInst.push_back(revertInst);
		this->listItem.push_back(item);
		this->instChoice->Insert(std::string("#") + std::to_string(this->countInst), this->countInst);
		this->countInst++;
	}
	this->selectInst(first);

	this->filepicker->SetPath(data.path + wxString("\\") + data.filename);
	this->filepicker->Layout();

	this->EnableMatriz();

	this->okButton->SetLabelText("Alterar");

	this->gravity->SetValue(data.physic->gravity);
	this->bounding_box->SetSelection(data.physic->index);
	this->InstTree->Expand(this->InstTree->GetRootItem());
}

dataImport ImportDialog::getData() {
	dataImport data;

	wxFileName filename = this->filepicker->GetFileName();
	double taxa = 1.0;

	if (filename.GetExt() == "fbx") {
		taxa = 0.01;
	}

	for (instDataImport *inst: this->listInst) {
		instDataImport processInst;
		processInst.rotate[0] = 2 * M_PI * (inst->rotate[0] / 360.0);
		processInst.rotate[1] = 2 * M_PI * (inst->rotate[1] / 360.0);
		processInst.rotate[2] = 2 * M_PI * (inst->rotate[2] / 360.0);

		processInst.translate[0] = inst->translate[0];
		processInst.translate[1] = inst->translate[1];
		processInst.translate[2] = inst->translate[2];

		processInst.scale[0] = (inst->scale[0] * taxa);
		processInst.scale[1] = (inst->scale[1] * taxa);
		processInst.scale[2] = (inst->scale[2] * taxa);

		data.insts.push_back(processInst);
	}

	data.filename = filename.GetFullName();
	data.path = filename.GetPath();

	bb_dt *bb_datatype = new bb_dt;

	data.physic = bb_datatype->tipos_bounding_box->at(this->bounding_box->GetSelection());
	data.physic->gravity = this->gravity->IsChecked();
	data.physic->mass = this->mass_physics->GetValue();

	return data;
}


