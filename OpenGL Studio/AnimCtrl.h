#pragma once

class AnimCtrl {
private:
	void(*function)(double);
	bool repeat;
	double mod;
	double lockedTime;
	double initial;
	bool locked;
	bool running;
public:
	AnimCtrl(void(*function)(double), bool repeat, double mod);
	void setLockedTime(double max);
	void setMod(double mod);
	void start(double initial);
	void trigger(double time);
	bool isLocked();
	bool isRunning();
	void end();
	~AnimCtrl();
};

