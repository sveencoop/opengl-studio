#include "File.h"
#include <fstream>
#include <cstdlib>
#include "InterfaceIO.h"

using namespace GlobalInterfaceIO;

namespace File {
	std::string read2string(const char * path) {
		std::string temp = "";
		std::string txt;
		std::ifstream file(path);

		if (file.is_open()) {
			while (file.good()) {
				std::getline(file, txt);
				temp += txt + "\n";
			}

		} else {
			log(StringBuilder::concat("File '",path,"' not found"));
		}
		file.close();
		return temp;
	}

}