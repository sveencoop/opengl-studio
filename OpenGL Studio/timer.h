#pragma once

class timer
{
private:
	static double lastTime;
	static double deltaTime;
public:
	static double getLastTime();
	static void clock();
};

