#pragma once
class Fog {
private:
	unsigned int attr_enabled;
	unsigned int attr_color;
	unsigned int attr_max;
	unsigned int attr_delta;
	unsigned int attr_gap;

public:
	float *color;
	float max;
	float delta;
	float gap;
	int enabled;
	Fog();
	void shaderingData(unsigned int shader);
	void apply();
};

