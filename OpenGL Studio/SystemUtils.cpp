#include "SystemUtils.h"

void SystemUtils::dumpheader() {
	wxTextFile file;

	bool isnew = file.Create(wxT("../dump.txt"));
	if (isnew) {
		file.Open("../dump.txt");

		if (!file.IsOpened()) {
			wxPuts(wxT("the dump cannot be opened"));
			return;
		}

		wxDateTime now = wxDateTime::Now();
		file.AddLine(wxT("Dump: ") + now.Format());
		file.AddLine(wxGetHomeDir());
		file.AddLine(wxGetOsDescription());
		file.AddLine(wxGetUserName());
		file.AddLine(wxGetFullHostName());
		file.AddLine(wxT(""));
		file.AddLine(wxT("----------------------------------"));
		file.AddLine(wxT(""));
		file.Write();
		file.Close();

		if (!file.IsOpened())
			wxPuts(wxT("the dump has been writted"));
	}
}

void SystemUtils::writeFile() {
	wxString str = wxT("You make me want to be a better man.\n");

	wxFile file;
	file.Create(wxT("../quote.txt"), true);

	if (file.IsOpened())
		wxPuts(wxT("the file is opened"));

	file.Write(str);
	file.Close();

	if (!file.IsOpened())
		wxPuts(wxT("the file is not opened"));
}

void SystemUtils::getDateTime() {
	wxDateTime now = wxDateTime::Now();

	wxString date1 = now.Format();
	wxString date2 = now.Format(wxT("%X"));
	wxString date3 = now.Format(wxT("%x"));

	wxPuts(date1);
	wxPuts(date2);
	wxPuts(date3);
}

void SystemUtils::getComputerInfo() {
	wxPuts(wxGetHomeDir());
	wxPuts(wxGetOsDescription());
	wxPuts(wxGetUserName());
	wxPuts(wxGetFullHostName());

	long mem = wxGetFreeMemory().ToLong();

	//wxPrintf(wxT("Memory: %ld\n"), mem);
}
