#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Light.h"

class Spotlight : public Light {
	private:

		unsigned int attr_ambient;
		unsigned int attr_diffuse;
		unsigned int attr_specular;
		unsigned int attr_intensity;
		unsigned int attr_constant;
		unsigned int attr_linear;
		unsigned int attr_quadratic;
		unsigned int attr_position;
		unsigned int attr_direction;
		unsigned int attr_outerCutOff;
		unsigned int attr_cutOff;
		unsigned int attr_epsilon;
		unsigned int attr_radius;

		float intensity;

		float outerCutOff;
		float innerCutOff;
		float epsilon;
		float targetIntensity;
		double lastTime;
		float* direction;
		int pos;

	public:
		Spotlight();
		void init();
		void initAttrs(unsigned int pos);
		void init(glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular);
		void toggleIntensity();
		void setAttenuation(float constant, float linear, float quadratic);
		void setPosition(float x, float y, float z);
		void setDirection(float x, float y, float z);

		void scale(float *scale);
		void rotate(float *rotate);
		void translate(float *translate);

		void setOuterCutOff(float value);
		void setInnerCutOff(float value);
		void updateEpsilon();
		void shaderingDataFlashlight(unsigned int shader);
		void shaderingData(unsigned int shader);
		void unset();
		void apply(float *position, float *direction);
		void apply();
		const char* getType();
};

