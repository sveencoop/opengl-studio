#include "Obj3D.h"

Mesh::Mesh(const char* name, std::vector<Vertex>& vertices, std::vector<unsigned int> indices) {
	this->vao = 0;
	this->indices = indices;
	this->vertices = vertices;
	this->name = name;

	this->material = new Material;
	this->material->shininess = 16;
	this->material->IOR = 0;
	this->material->reflection = 0;

	this->material->textureDiffuse = -1;
	this->material->textureSpecular = -1;
	this->material->textureReflection = -1;
	this->material->textureNormal = -1;
	this->material->textureEmission = -1;

	this->dataImgDiffuseToLoad = nullptr;
	this->dataImgSpecularToLoad = nullptr;
	this->dataImgReflectionToLoad = nullptr;
	this->dataImgNormalToLoad = nullptr;
	this->dataImgEmissionToLoad = nullptr;

	this->parent = nullptr;
}

Mesh::~Mesh() {
	this->vertices.clear();
	this->indices.clear();
}

void Mesh::setDiffuseColor(unsigned int id, float r, float g, float b) {
	this->diffuseColor = glm::vec3(r, g, b);
}

void Mesh::setTextureImageDiffuse(TextureImage * image) {
	this->dataImgDiffuseToLoad = image;
}

void Mesh::setTextureImageSpecular(TextureImage * image) {
	this->dataImgSpecularToLoad = image;
}

void Mesh::setTextureImageReflection(TextureImage * image) {
	this->dataImgReflectionToLoad = image;
}

void Mesh::setTextureImageNormal(TextureImage * image) {
	this->dataImgNormalToLoad = image;
}

void Mesh::setTextureImageEmission(TextureImage * image) {
	this->dataImgEmissionToLoad = image;
}

void Mesh::flushTextures() {

	if (this->dataImgDiffuseToLoad != nullptr) {
		this->material->textureDiffuse = this->dataImgDiffuseToLoad->id;
	}
	if (this->dataImgSpecularToLoad != nullptr) {
		this->material->textureSpecular = this->dataImgSpecularToLoad->id;
	}
	if (this->dataImgReflectionToLoad != nullptr) {
		this->material->textureReflection = this->dataImgReflectionToLoad->id;
	}
	if (this->dataImgNormalToLoad != nullptr) {
		this->material->textureNormal = this->dataImgNormalToLoad->id;
	}
	if (this->dataImgEmissionToLoad != nullptr) {
		this->material->textureEmission = this->dataImgEmissionToLoad->id;
	}
}

void Mesh::freeLoad() {
	this->dataImgDiffuseToLoad = nullptr;
	this->dataImgSpecularToLoad = nullptr;
	this->dataImgReflectionToLoad = nullptr;
	this->dataImgNormalToLoad = nullptr;
}

void Mesh::compileVertexData() {
	//pode se usar tamb�m o conceito de glBufferSubdata
	unsigned int VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*this->vertices.size(), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*this->indices.size(), &this->indices[0], GL_STATIC_DRAW);

	// position attribute
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

	// normal attribute
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

	// Texture attribute
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

	// Tangent attribute
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangents));

	// Bitangent attribute
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, BiTangents));

	// Shadow attribute
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, ShMCoords));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	this->vao = VAO;
}

void Mesh::setShininess(float newshininess) {
	this->material->shininess = newshininess;
}

void Mesh::setReflection(float value) {
	this->material->reflection = value;
}

void Mesh::setIOR(float value) {
	this->material->IOR = value;
}

/*
	OBJECT 3D
*/
Object3D::Object3D(const char* name) {
	int max = strlen(name);
	char *copy = (char*)malloc(sizeof(const char)*(strlen(name) + 1));
	for (int i = 0; i < max; i++) {
		copy[i] = name[i];
	}
	copy[max] = '\0';
	this->name = copy;

	this->limits = new Limits;
	this->isOutlined = false;
}

void Object3D::setCompileOutline(bool value) {
	this->isOutlined = value;
}

void Object3D::setShaderProgramme(unsigned int id) {
	this->shader_programme = id;
}

void Object3D::translateTransform(glm::vec3 translate) {
	this->oriTranf[3][0] = translate.x;
	this->oriTranf[3][1] = translate.y;
	this->oriTranf[3][2] = translate.z;
}

void Object3D::addMesh(Mesh *m) {
	this->meshes.push_back(m);
	m->parent = this;
}

Replica *Object3D::addReplica(instDataImport data) {
	
	Replica *novo = new Replica(this, data);
	novo->setTransformation(this->oriTranf);
	novo->compileTransform();

	novo->limits = new Limits(limits);
	novo->limits->resize(novo->scale);
	this->replicas.push_back(novo);
	
	return novo;
}

void Object3D::setTransform(glm::mat4 trans) {
	this->oriTranf = trans;
}

void Object3D::setName(const char * name) {
	this->name = name;
}

const char *Object3D::getName() {
	return name;
}

glm::vec3 Limits::get_center() {
	return glm::vec3((this->x.max + this->x.min)/2.0, (this->y.max + this->y.min) / 2.0, (this->z.max + this->z.min) / 2.0);
}

Limits::Limits(Limits * copy) {
	this->x = copy->x;
	this->y = copy->y;
	this->z = copy->z;
}

Limits::Limits() {
}

void Limits::test(glm::vec3 v) {
	if (v.x > this->x.max) { this->x.max = v.x; }
	if (v.x < this->x.min) { this->x.min = v.x; }

	if (v.y > this->y.max) { this->y.max = v.y; }
	if (v.y < this->y.min) { this->y.min = v.y; }

	if (v.z > this->z.max) { this->z.max = v.z; }
	if (v.z < this->z.min) { this->z.min = v.z; }
}

void Limits::resize(glm::vec3 resize) {
	this->x.min *= resize.x;
	this->x.max *= resize.x;
	
	this->y.min *= resize.y;
	this->y.max *= resize.y;

	this->z.min *= resize.z;
	this->z.max *= resize.z;
}

glm::vec3 * Limits::bounding_box() {
	glm::vec3 *vertex = (glm::vec3*)malloc(sizeof(glm::vec3) * 8);

	vertex[0] = glm::vec3(this->x.min, this->y.min, this->z.min);
	vertex[1] = glm::vec3(this->x.min, this->y.min, this->z.max);
	vertex[2] = glm::vec3(this->x.min, this->y.max, this->z.min);
	vertex[3] = glm::vec3(this->x.min, this->y.max, this->z.max);

	vertex[4] = glm::vec3(this->x.max, this->y.min, this->z.min);
	vertex[5] = glm::vec3(this->x.max, this->y.min, this->z.max);
	vertex[6] = glm::vec3(this->x.max, this->y.max, this->z.min);
	vertex[7] = glm::vec3(this->x.max, this->y.max, this->z.max);

	return vertex;
}

double max(double a, double b) {
	return (a > b ? a : b);
}

double Limits::maxRadius() {

	glm::vec3 *vertices = this->bounding_box();

	double atual_raio = -1;

	for (int i = 0; i < 8; i++) {
		glm::vec3 v = glm::abs(vertices[i]);
		double length = max(max(v.x,v.y), v.z);

		if (length > atual_raio) {
			atual_raio = length;
		}

	}

	return atual_raio;
}

double Limits::lx() {
	return this->x.max - this->x.min;
}

double Limits::ly() {
	return  this->y.max - this->y.min;
}

double Limits::lz() {
	return this->z.max - this->z.min;
}

Replica::Replica(Object3D* parent, instDataImport inst) {
	this->rotateMatriz = glm::mat4(1.0);
	this->idColor = glm::vec3(0.0, 0.0, 0.0);
	this->rigid_body = NULL;
	this->adictionalMatriz = glm::mat4(1.0);
	this->parent = parent;
	this->inst = inst;
	this->center = parent->limits->get_center();
	this->position = glm::vec3(inst.translate[0], inst.translate[1], inst.translate[2]);
	this->setRotation(glm::vec3(inst.rotate[0], inst.rotate[1], inst.rotate[2]));
	this->scale = glm::vec3(inst.scale[0], inst.scale[1], inst.scale[2]);
}

glm::mat4 Replica::getTransformationMatrix() {
	return this->transf;
}

void Replica::setIdColor(float r, float g, float b) {
	this->idColor = glm::vec3(r, g, b);
}

void Replica::setTransformation(glm::mat4 trans) {
	this->oriTranf = trans;
}

void Replica::compileTransform() {
	glm::mat4 trans = this->oriTranf;
	trans = glm::translate(trans, this->position);
	trans = trans * this->rotateMatriz;
	trans = glm::scale(trans, this->scale);
	trans *=  this->adictionalMatriz;
	this->transf = trans;
}

void Replica::setAdictionalMatriz(glm::mat4 matriz) {
	this->adictionalMatriz = matriz;
}

void Replica::setCenter(glm::vec3 center) {
	this->center = center;
}

void Replica::setPosition(glm::vec3 position) {
	this->position = position;
}

void Replica::setRotation(glm::vec3 rotation) {
	this->rotateMatriz = glm::mat4(1.0);

	this->rotateMatriz = glm::rotate(this->rotateMatriz, rotation.x, glm::vec3(1, 0, 0));
	this->rotateMatriz = glm::rotate(this->rotateMatriz, rotation.y, glm::vec3(0, 1, 0));
	this->rotateMatriz = glm::rotate(this->rotateMatriz, rotation.z, glm::vec3(0, 0, 1));
}

void Replica::setRotationOnCenter(glm::vec3 rotationOnCenter) {
	this->rotateCenterMatriz = glm::mat4(1.0);
	
	this->rotateCenterMatriz = glm::translate(this->rotateCenterMatriz, -this->center);
	this->rotateCenterMatriz = glm::rotate(this->rotateCenterMatriz, rotationOnCenter.x, glm::vec3(1, 0, 0));
	this->rotateCenterMatriz = glm::rotate(this->rotateCenterMatriz, rotationOnCenter.y, glm::vec3(0, 1, 0));
	this->rotateCenterMatriz = glm::rotate(this->rotateCenterMatriz, rotationOnCenter.z, glm::vec3(0, 0, 1));
	this->rotateCenterMatriz = glm::translate(this->rotateCenterMatriz, this->center);

}

void Replica::setMatrizRotation(glm::mat4 matriz) {
	this->rotateMatriz = glm::transpose(matriz);
}

instDataImport Replica::getInst() {
	return this->inst;
}

void Replica::setRigidBody(dBodyID body) {
	this->rigid_body = body;
}

void Replica::updateFromRigidBody() {
	if (this->rigid_body != nullptr) {
		const dReal *pos = dBodyGetPosition(this->rigid_body);
		this->position.x = pos[0];
		this->position.y = pos[1];
		this->position.z = pos[2];
		const dReal *rot = dBodyGetRotation(this->rigid_body);
		this->rotateMatriz = glm::mat4(
			rot[0], rot[4], rot[8], 0,
			rot[1], rot[5], rot[9], 0,
			rot[2], rot[6], rot[10], 0,
			rot[3], rot[7], rot[11], 1);
		this->compileTransform();
	}
}

Replica::~Replica() {

}
