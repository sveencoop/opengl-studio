#include "timer.h"
#include <GLFW\glfw3.h>

double timer::lastTime;
double timer::deltaTime;

double timer::getLastTime() {
	return timer::deltaTime;
}

void timer::clock() {
	double currentTime = glfwGetTime();
	timer::deltaTime = currentTime - timer::lastTime;
	timer::lastTime = currentTime;
}
