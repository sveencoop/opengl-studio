#pragma once
#include <glad\glad.h>

namespace shader
{
	unsigned int compileShader(const char *vertex_shader_path, const char *geometry_shader_path, const char *fragment_shader_path);
	unsigned int compileShader(const char *vertex_shader, const char *fragment_shader);
	void freeShader(unsigned int shader);
};

struct material {

	unsigned int specmap;
	unsigned int reflexmap;
	unsigned int diffmap;
	unsigned int normalmap;
	unsigned int shadowMap;

	unsigned int withTexture;
	unsigned int hasNormal;
	unsigned int hasEmission;
	unsigned int reflect;
	unsigned int IOR;
	unsigned int shininess;
};

class DeferredShader {
public:
	unsigned int id_gbuffer_program;//shader
	unsigned int id_deffered_program;//shader

	//gbuffering attr
	unsigned int view_projection;
	unsigned int transform;
	material material;

	//deferred attr
	unsigned int gbuffer_positions;
	unsigned int gbuffer_normals;
	unsigned int gbuffer_albedospec;


	DeferredShader();
	void shadering_data_gbuffer();
	void shadering_data_deffered();

};

class GameShader {
public:
	unsigned int id;

	unsigned int view_projection;
	unsigned int viewPos;
	unsigned int shadowMap;
	unsigned int lightSpaceMatrix;
	unsigned int NR_POINT_LIGHTS;
	unsigned int NR_SPOT_LIGHTS;
	unsigned int time;
	unsigned int transform;
	unsigned int cubemap;

	material material;

	GameShader();
	void shadering_data();
};

