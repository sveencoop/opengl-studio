#include "PhysicsEngine.h"
#include <thread>

dWorldID PhysicsEngine::world;
dSpaceID PhysicsEngine::space;
dGeomID  PhysicsEngine::ground;
dJointGroupID PhysicsEngine::contactgroup;
dJointFeedback *PhysicsEngine::feedback;
std::vector<dBodyID> PhysicsEngine::bodies;
float PhysicsEngine::max_vel = 100;

void PhysicsEngine::enableGravity() {
	std::this_thread::sleep_for(std::chrono::seconds(1));
	dWorldSetGravity(PhysicsEngine::world, 0, -9.81, 0);
}

void PhysicsEngine::init() {
	dInitODE();
	PhysicsEngine::world = dWorldCreate(); //inicializa mundo
	PhysicsEngine::space = dHashSpaceCreate(0); //incializa espa�o de detec��o de colis�es
	PhysicsEngine::ground = dCreatePlane(PhysicsEngine::space, 0, 1, 0, 0); //cria objeto de ch�o
	PhysicsEngine::contactgroup = dJointGroupCreate(0); //grupo de contato

	dWorldSetGravity(PhysicsEngine::world, 0, 0, 0);
}

void PhysicsEngine::collide(void * data, dGeomID o1, dGeomID o2) {
	const int N = 100;
	dContact contact[N];

	int n = dCollide(o1, o2, N, &contact[0].geom, sizeof(dContact));

	for (int i = 0; i < n; i++) {
		contact[i].surface.mode = dContactSoftERP | dContactSoftCFM;
		contact[i].surface.mu = dInfinity;
		contact[i].surface.soft_cfm = 1e-8;
		contact[i].surface.soft_erp = 0.1;

		dJointID c = dJointCreateContact(PhysicsEngine::world, PhysicsEngine::contactgroup, &contact[i]);
		dJointAttach(c, dGeomGetBody(contact[i].geom.g1), dGeomGetBody(contact[i].geom.g2));
	}
}

void PhysicsEngine::bodyUpdateEvent(dBodyID body) {
	Replica *replica = (Replica*)dBodyGetData(body);
	replica->updateFromRigidBody();
}

void PhysicsEngine::appendObject(Replica * replica, dBodyID body) {
	PhysicsEngine::addBody(body);
	dBodySetData(body, replica);
	dBodySetMovedCallback(body, PhysicsEngine::bodyUpdateEvent);
	replica->setRigidBody(body);
	dBodySetLinearVel(body, 0, 0, 0);
	dBodySetAngularVel(body, 0, 0, 0);
}

void PhysicsEngine::loop(double stepsize) {

	if (!PhysicsEngine::bodies.empty()) {

		for (dBodyID body : PhysicsEngine::bodies) {
			const dReal *linear_vel = dBodyGetLinearVel(body);
			double amount = 1 - stepsize * 0.1;
			if (amount < 0) {
				amount = 0;
			}

			PhysicsEngine::clampMaxVelocity(body, linear_vel[0] * amount, linear_vel[1] * amount, linear_vel[2] * amount);
		}

		dSpaceCollide(PhysicsEngine::space, 0, &PhysicsEngine::collide);
		dWorldQuickStep(PhysicsEngine::world, stepsize);
		dJointGroupEmpty(PhysicsEngine::contactgroup);
	}
}

void PhysicsEngine::clampMaxVelocity(dBodyID body, float x, float y, float z) {
	float p0 = (x > PhysicsEngine::max_vel ? PhysicsEngine::max_vel : (x > -PhysicsEngine::max_vel ? x : -PhysicsEngine::max_vel));
	float p1 = (y > PhysicsEngine::max_vel ? PhysicsEngine::max_vel : (y > -PhysicsEngine::max_vel ? y : -PhysicsEngine::max_vel));
	float p2 = (z > PhysicsEngine::max_vel ? PhysicsEngine::max_vel : (z > -PhysicsEngine::max_vel ? z : -PhysicsEngine::max_vel));

	dBodySetLinearVel(body, p0, p1, p2);
}

void PhysicsEngine::createBall(Replica * obj) {

	dBodyID body = dBodyCreate(PhysicsEngine::world);
	const dReal radius = obj->limits->maxRadius();
	const dReal mass = obj->parent->dataImported.physic->mass;

	instDataImport inst = obj->getInst();

	dReal x0 = inst.translate[0], y0 = inst.translate[1], z0 = inst.translate[2];

	dMass m1;
	dMassSetZero(&m1);
	dMassSetSphereTotal(&m1, mass, radius);
	dBodySetMass(body, &m1);
	dBodySetPosition(body, x0, y0, z0);
	dMatrix3 R;
	dRFromAxisAndAngle(R, 1.0, 0.0, 0.0, inst.rotate[0]);
	if (inst.rotate[1] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, inst.rotate[1]);
	}
	if (inst.rotate[2] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, inst.rotate[2]);
	}
	dBodySetRotation(body, R);


	dGeomID geom = dCreateSphere(PhysicsEngine::space, radius);
	dGeomSetBody(geom, body);
	PhysicsEngine::appendObject(obj, body);
}

void PhysicsEngine::createCrate(Replica * obj) {
	dBodyID body = dBodyCreate(PhysicsEngine::world);
	const dReal lx = obj->limits->lx();
	const dReal ly = obj->limits->ly();
	const dReal lz = obj->limits->lz();
	const dReal mass = obj->parent->dataImported.physic->mass;

	instDataImport inst = obj->getInst();

	dReal x0 = inst.translate[0], y0 = inst.translate[1], z0 = inst.translate[2];

	dMass m1;
	dMassSetZero(&m1);
	dMassSetBoxTotal(&m1, mass, lx, ly, lz);
	dBodySetMass(body, &m1);
	dBodySetPosition(body, x0, y0, z0);
	dMatrix3 R;
	dRFromAxisAndAngle(R, 1.0, 0.0, 0.0, inst.rotate[0]);
	if (inst.rotate[1] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, inst.rotate[1]);
	}
	if (inst.rotate[2] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, inst.rotate[2]);
	}
	dBodySetRotation(body, R);

	dGeomID geom = dCreateBox(PhysicsEngine::space, lx, ly, lz);
	dGeomSetBody(geom, body);
	PhysicsEngine::appendObject(obj, body);
}

void PhysicsEngine::createCapsule(Replica * obj) {
	dBodyID body = dBodyCreate(PhysicsEngine::world);
	const dReal lx = obj->limits->lx();
	const dReal ly = obj->limits->ly();
	const dReal lz = obj->limits->lz();
	const dReal mass = obj->parent->dataImported.physic->mass;

	instDataImport inst = obj->getInst();

	dReal x0 = inst.translate[0], y0 = inst.translate[1], z0 = inst.translate[2];

	dMass m1;
	dMassSetZero(&m1);
	dMassSetCapsuleTotal(&m1, mass, 2, lx / 2.0, lz - lx / 2.0);
	dBodySetMass(body, &m1);
	dBodySetPosition(body, x0, y0, z0);

	dMatrix3 R;
	dRFromAxisAndAngle(R, 1.0, 0.0, 0.0, inst.rotate[0]);
	if (inst.rotate[1] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, inst.rotate[1]);
	}
	if (inst.rotate[2] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, inst.rotate[2]);
	}
	dBodySetRotation(body, R);

	//dBodySetRotation(body, R);

	dGeomID geom = dCreateCapsule(PhysicsEngine::space, lx / 2.0, lz - lx / 2.0);
	dGeomSetBody(geom, body);

	PhysicsEngine::appendObject(obj, body);
}

void PhysicsEngine::createCilindro(Replica * obj) {
	dBodyID body = dBodyCreate(PhysicsEngine::world);
	const dReal lx = obj->limits->lx();
	const dReal ly = obj->limits->ly();
	const dReal lz = obj->limits->lz();
	const dReal mass = obj->parent->dataImported.physic->mass;

	instDataImport inst = obj->getInst();

	dReal x0 = inst.translate[0], y0 = inst.translate[1], z0 = inst.translate[2];

	dMass m1;
	dMassSetZero(&m1);
	dMassSetCappedCylinderTotal(&m1, mass, 2, lx / 2.0, lz);
	dBodySetMass(body, &m1);
	dBodySetPosition(body, x0, y0, z0);

	dMatrix3 R;
	dRFromAxisAndAngle(R, 1.0, 0.0, 0.0, inst.rotate[0]);
	if (inst.rotate[1] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, inst.rotate[1]);
	}
	if (inst.rotate[2] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, inst.rotate[2]);
	}
	dBodySetRotation(body, R);

	//dBodySetRotation(body, R);

	dGeomID geom = dCreateCylinder(PhysicsEngine::space, lx / 2.0, lz);
	dGeomSetBody(geom, body);

	PhysicsEngine::appendObject(obj, body);

}

void PhysicsEngine::createObject(Replica * obj) {

	dBodyID body = dBodyCreate(PhysicsEngine::world);

	dTriMeshDataID dataMeshID = dGeomTriMeshDataCreate();

	const dReal mass = obj->parent->dataImported.physic->mass;

	instDataImport inst = obj->getInst();

	dReal x0 = inst.translate[0], y0 = inst.translate[1], z0 = inst.translate[2];

	int vertex_count = 0;
	int indices_count = 0;

	dReal* mapVertices = new dReal[vertex_count];
	dTriIndex *mapIndices = new dTriIndex[indices_count];

	//popular map

	dGeomTriMeshDataBuildSimple(dataMeshID, mapVertices, vertex_count * 3, mapIndices, indices_count * 3);
	dGeomID geom = dCreateTriMesh(PhysicsEngine::space, dataMeshID, 0, 0, 0);

	dMass m1;
	dMassSetZero(&m1);
	dMassSetTrimeshTotal(&m1, mass, geom);
	dBodySetMass(body, &m1);
	dBodySetPosition(body, x0, y0, z0);

	dMatrix3 R;
	dRFromAxisAndAngle(R, 1.0, 0.0, 0.0, inst.rotate[0]);
	if (inst.rotate[1] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, inst.rotate[1]);
	}
	if (inst.rotate[2] != 0.0) {
		dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, inst.rotate[2]);
	}
	dBodySetRotation(body, R);

	dGeomSetBody(geom, body);

	PhysicsEngine::appendObject(obj, body);
}

void PhysicsEngine::registerObject(Object3D* obj) {
	if (obj->dataImported.physic->gravity) {
		for (Replica *r : obj->replicas) {
			bb_dt *physicsDataType = new bb_dt();

			unsigned int index = obj->dataImported.physic->index;

			if (index == physicsDataType->esfera) {
				PhysicsEngine::createBall(r);
			} else if (index == physicsDataType->cubo) {
				PhysicsEngine::createCrate(r);
			} else if (index == physicsDataType->cilindro) {
				PhysicsEngine::createCilindro(r);
			} else if (index == physicsDataType->capsula) {
				PhysicsEngine::createCapsule(r);
			} else if (index == physicsDataType->objeto) {
				
			}
		}
	}
}

void PhysicsEngine::resetObjects() {

	for (dBodyID body : PhysicsEngine::bodies) {

		Replica *r = (Replica*)dBodyGetData(body);
		instDataImport inst = r->getInst();

		dReal x0 = inst.translate[0],
			y0 = inst.translate[1],
			z0 = inst.translate[2];

		dBodySetPosition(body, x0, y0, z0);
		dBodySetLinearVel(body, x0, y0, z0);

		dMatrix3 R;
		dRFromAxisAndAngle(R, 1.0, 0.0, 0.0, inst.rotate[0]);
		if (inst.rotate[1] != 0.0) {
			dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, inst.rotate[1]);
		}
		if (inst.rotate[2] != 0.0) {
			dRFromAxisAndAngle(R, 0.0, 0.0, 1.0, inst.rotate[2]);
		}
		dBodySetRotation(body, R);

	}


}

void PhysicsEngine::addBody(dBodyID body) {
	PhysicsEngine::bodies.push_back(body);
}

void PhysicsEngine::finish() {
	PhysicsEngine::bodies.clear();

	dJointGroupDestroy(PhysicsEngine::contactgroup);
	dSpaceDestroy(PhysicsEngine::space);
	dWorldDestroy(PhysicsEngine::world);
	dCloseODE();
}
