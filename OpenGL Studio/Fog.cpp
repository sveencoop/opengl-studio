#include "Fog.h"
#include <glad\glad.h>
#include <stdlib.h>

Fog::Fog() {
	this->enabled = 0;
	this->color = (float*)malloc(sizeof(float)*3);
	this->color[0] = 1.0f;
	this->color[1] = 1.0f;
	this->color[2] = 1.0f;

	this->max = 0.4f;
	this->delta = 40;
	this->gap = 0;
}

void Fog::shaderingData(unsigned int shader) {
	this->attr_enabled = glGetUniformLocation(shader, "fog.enabled");
	this->attr_color = glGetUniformLocation(shader, "fog.color");
	this->attr_max = glGetUniformLocation(shader, "fog.max");
	this->attr_delta = glGetUniformLocation(shader, "fog.delta");
	this->attr_gap = glGetUniformLocation(shader, "fog.gap");
}

void Fog::apply() {
	glUniform1i(this->attr_enabled, this->enabled);
	glUniform3fv(this->attr_color, 1, this->color);
	glUniform1f(this->attr_max, this->max);
	glUniform1f(this->attr_delta, this->delta);
	glUniform1f(this->attr_gap, this->gap);
}

