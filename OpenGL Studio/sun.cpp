#include "sun.h"
#include <glad\glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void Sun::init() {
	this->diffuse = (float*)malloc(sizeof(float) * 3);
	this->ambient = (float*)malloc(sizeof(float) * 3);
	this->direction = (float*)malloc(sizeof(float) * 3);
	this->diffuse[0] = 0.3f;
	this->diffuse[1] = 0.3f;
	this->diffuse[2] = 0.3f;

	this->ambient[0] = 0.3f;
	this->ambient[1] = 0.3f;
	this->ambient[2] = 0.3f;

	glm::vec3 direction = glm::normalize(glm::vec3(5.0f,-20.0f,20.0f));

	this->direction[0] = direction.x;
	this->direction[1] = direction.y;
	this->direction[2] = direction.z;
}

void Sun::apply() {
	glUniform3fv(this->attr_ambient, 1, this->ambient);
	glUniform3fv(this->attr_diffuse, 1, this->diffuse);
	glUniform3fv(this->attr_direction, 1, this->direction);
}

void Sun::setPosition(float x, float y, float z) {
	this->direction[0] = x;
	this->direction[1] = y;
	this->direction[2] = z;
}

void Sun::shaderingData(unsigned int shader) {
	this->attr_ambient = glGetUniformLocation(shader, "sun.ambient");
	this->attr_diffuse = glGetUniformLocation(shader, "sun.diffuse");
	this->attr_direction = glGetUniformLocation(shader, "sun.direction");
}
