#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "InterfaceIO.h"

enum Modulo_Key { fly_mode, normal_mode, deg90 };

class Camera {
	protected:
		glm::mat4 view;
		glm::mat4 projection;
		glm::vec3 posCam;
		glm::vec3 tarCam;
		glm::vec3 camFront;
		glm::vec3 camUp;
		glm::vec3 camRight;

		float accCamY;
		float yaw; //x
		float pitch; //y

		unsigned int w_width;
		unsigned int w_height;
		float angle;
		float max;
		float min;
		float w_ratio;

	public:
		void init(float x, float y, float z, float angle, float min, float max);
		void compile();
		void calcFinalCam();
		void setOrientation(unsigned int DIR);
		void changePosCam(float x, float y, float z);
		void changeTargetCam(float x, float y, float z);
		void setWindow(int SCR_WIDTH, int SCR_HEIGHT);
		void setAngle(float angle);
		glm::vec3 getPosition();
		glm::vec3 getFront();
		glm::mat4 getView();
		glm::mat4 getProjection();
		float getYaw();
		float getPitch();
		void setFront(glm::vec3 front);
		void setYaw(float yaw);
		void setPitch(float pitch);

		virtual void accPitch(double acc) = 0;
		virtual void accYaw(double acc) = 0;
		virtual void foward(double speed) = 0;
		virtual void backward(double speed) = 0;
		virtual void left(double speed) = 0;
		virtual void right(double speed) = 0;
		virtual void changeY(float y) = 0;
		virtual Modulo_Key getMode() = 0;
};

class NoWalk : public Camera {
	public:
		void accPitch(double acc);
		void accYaw(double acc);
		void foward(double speed);
		void backward(double speed);
		void left(double speed);
		void right(double speed);
		void changeY(float y);
		Modulo_Key getMode();
};

class FPSWalk : public Camera {
	public:
		void accPitch(double acc);
		void accYaw(double acc);
		void foward(double speed);
		void backward(double speed);
		void left(double speed);
		void right(double speed);
		void changeY(float y);
		Modulo_Key getMode();
};

class FPSNoclipWalk : public Camera {
	public:
		void accPitch(double acc);
		void accYaw(double acc);
		void foward(double speed);
		void backward(double speed);
		void left(double speed);
		void right(double speed);
		void changeY(float y);
		Modulo_Key getMode();
};

