#version 400

struct Material {
    vec3 ambient;
    vec3 diff;
    vec3 spec;
    float shininess;
}; 

struct Light {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

out vec4 frag_colour;

in vec2 TexCoord;
in vec3 Normal; 
in vec3 FragPos;  

uniform Light light;  
uniform Material material;
uniform sampler2D texture0;
uniform vec3 viewPos;

void main() {

	vec3 norm = normalize(Normal);

	vec3 lightDir = normalize(light.position - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * material.diff;
	
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm); 
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec3 specular = spec * material.spec;

	vec3 result = material.ambient * light.ambient + diffuse * light.diffuse + specular * light.specular;

	frag_colour = vec4(result, 1.0)*texture(texture0, TexCoord);
  
}