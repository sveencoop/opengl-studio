#include "ShaderControl.h"
#include <fstream>
#include <cstdlib>
#include "StringBuilder.h"
#include "InterfaceIO.h"

using namespace GlobalInterfaceIO;

namespace File{
	std::string read2string(const char * path) {
		std::string temp = "";
		std::string txt;
		std::ifstream file(path);

		if (file.is_open()) {
			while (file.good()) {
				std::getline(file, txt);
				temp += txt + "\n";
			}

		} else {
			log(StringBuilder::concat("File '", path, "' not found"));
		}
		file.close();
		return temp;
	}

}

namespace shader {

	//privado
	unsigned int compileCode(const char * path, unsigned int shader_type) {
		std::string shader;
		shader = File::read2string(path);
		const char* code = shader.c_str();

		GLuint id = glCreateShader(shader_type); //criou
		glShaderSource(id, 1, &code, NULL); //setou
		glCompileShader(id); //compilou
		int  success;
		char infoLog[512];
		glGetShaderiv(id, GL_COMPILE_STATUS, &success);
		if (!success) {

			glGetShaderInfoLog(id, 512, NULL, infoLog);
			log(std::string(std::string(infoLog) + std::string(" on ") + std::string(path)).c_str());
		}

		return id;
	}

	//publico
	unsigned int compileShader(const char *vertex_shader_path, const char *geometry_shader_path, const char *fragment_shader_path) {
		GLuint vs = compileCode(vertex_shader_path, GL_VERTEX_SHADER);
		GLuint gs = compileCode(geometry_shader_path, GL_GEOMETRY_SHADER);
		GLuint fs = compileCode(fragment_shader_path, GL_FRAGMENT_SHADER);

		GLuint programme = glCreateProgram();
		glAttachShader(programme, fs);
		glAttachShader(programme, gs);
		glAttachShader(programme, vs);
		glLinkProgram(programme);

		glDeleteShader(vs);
		glDeleteShader(gs);
		glDeleteShader(fs);
		return programme;

	}

	unsigned int compileShader(const char *vertex_shader_path, const char *fragment_shader_path) {

		GLuint vs = compileCode(vertex_shader_path, GL_VERTEX_SHADER);
		GLuint fs = compileCode(fragment_shader_path, GL_FRAGMENT_SHADER);

		GLuint programme = glCreateProgram();
		glAttachShader(programme, fs);
		glAttachShader(programme, vs);
		glLinkProgram(programme);

		glDeleteShader(vs);
		glDeleteShader(fs);
		return programme;
	}

	void freeShader(unsigned int shader) {
		glDeleteProgram(shader);
	}

}

GameShader::GameShader() {
	this->id = shader::compileShader("../OpenGL Studio/Shaders/withTextureAlpha/test.vsh", "../OpenGL Studio/Shaders/withTextureAlpha/test.fsh");
}

void GameShader::shadering_data() {
	this->view_projection = glGetUniformLocation(this->id, "view_projection");
	this->viewPos = glGetUniformLocation(this->id, "viewPos");

	this->shadowMap = glGetUniformLocation(this->id, "shadowMap");

	this->lightSpaceMatrix = glGetUniformLocation(this->id, "lightSpaceMatrix");

	this->NR_POINT_LIGHTS = glGetUniformLocation(this->id, "NR_POINT_LIGHTS");
	this->NR_SPOT_LIGHTS = glGetUniformLocation(this->id, "NR_SPOT_LIGHTS");

	this->time = glGetUniformLocation(this->id, "time");

	this->transform = glGetUniformLocation(this->id, "transform");
	this->cubemap = glGetUniformLocation(this->id, "cubemap");

	this->material.hasEmission = glGetUniformLocation(this->id, "material.hasEmission");
	this->material.hasNormal = glGetUniformLocation(this->id, "material.hasNormal");
	this->material.withTexture = glGetUniformLocation(this->id, "material.withTexture");

	this->material.reflect = glGetUniformLocation(this->id, "material.reflect");
	this->material.IOR = glGetUniformLocation(this->id, "material.IOR");
	this->material.shininess = glGetUniformLocation(this->id, "material.shininess");

	this->material.diffmap = glGetUniformLocation(this->id, "material.diffmap");
	this->material.specmap = glGetUniformLocation(this->id, "material.specmap");
	this->material.reflexmap = glGetUniformLocation(this->id, "material.reflexmap");
	this->material.normalmap = glGetUniformLocation(this->id, "material.normalmap");
	this->material.shadowMap = glGetUniformLocation(this->id, "material.shadowMap");

	glUniform1i(this->material.diffmap, 0);
	glUniform1i(this->material.specmap, 1);
	glUniform1i(this->material.reflexmap, 2);
	glUniform1i(this->material.normalmap, 3);
	glUniform1i(this->material.shadowMap, 4);
	glUniform1i(this->cubemap, 5);
	glUniform1i(this->shadowMap, 6);
}

DeferredShader::DeferredShader() {
	this->id_gbuffer_program = shader::compileShader("../OpenGL Studio/Shaders/deffering/gbuffering.vsh", "../OpenGL Studio/Shaders/deffering/gbuffering.fsh");
	this->id_deffered_program = shader::compileShader("../OpenGL Studio/Shaders/deffering/render.vsh", "../OpenGL Studio/Shaders/deffering/render.fsh");
}

void DeferredShader::shadering_data_gbuffer() {
	this->view_projection = glGetUniformLocation(this->id_gbuffer_program, "view_projection");
	this->transform = glGetUniformLocation(this->id_gbuffer_program, "transform");

	this->material.hasNormal = glGetUniformLocation(this->id_gbuffer_program, "material.hasNormal");
	this->material.withTexture = glGetUniformLocation(this->id_gbuffer_program, "material.withTexture");

	this->material.diffmap = glGetUniformLocation(this->id_gbuffer_program, "material.diffmap");
	this->material.specmap = glGetUniformLocation(this->id_gbuffer_program, "material.specmap");
	this->material.normalmap = glGetUniformLocation(this->id_gbuffer_program, "material.normalmap");
	glUniform1i(this->material.diffmap, 0);
	glUniform1i(this->material.specmap, 1);
	glUniform1i(this->material.normalmap, 2);
}

void DeferredShader::shadering_data_deffered() {

	this->gbuffer_positions = glGetUniformLocation(this->id_deffered_program, "gbuffer_position");
	this->gbuffer_normals = glGetUniformLocation(this->id_deffered_program, "gbuffer_normals");
	this->gbuffer_albedospec = glGetUniformLocation(this->id_deffered_program, "gbuffer_albedospec");

	glUniform1i(this->gbuffer_positions, 0);
	glUniform1i(this->gbuffer_normals, 1);
	glUniform1i(this->gbuffer_albedospec, 2);

}
