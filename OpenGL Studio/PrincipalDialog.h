#pragma once

#include "Core.h"
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/icon.h>
#include <wx/panel.h>
#include <wx/wxprec.h>
#include <wx/stattext.h>
#include <wx/colour.h>
#include <wx/menu.h>
#include <vector>
#include "ImportDialog.h"
#include "EditorManagerWindows.h"
#include <thread>

class app : public wxApp {
public:
	virtual bool OnInit();
};

const int ID_DEFAULT = 0;
const int ID_EDITOR = 1;
const int ID_CONFIG = 2;
const int ID_OPEN = 3;
const int ID_SAVE = 4;
const int ID_NOVO = 5;
const int ID_FECHAR = 6;
const int ID_DIALOG_IMPORT = 7;
const int ID_LIST = 8;
const int ID_REMOVE_ITEM = 9;
const int ID_EDIT_ITEM = 10;
const int ID_CLEAR_ITENS = 11;
const int ID_W_WIDTH = 12;
const int ID_W_HEIGHT = 13;
const int ID_W_FULLSCREEN = 14;

const int ID_SAIR = 99;

const int INDEX_COLUMN_FILENAME = 0;
const int INDEX_COLUMN_NAME = 1;
const int INDEX_COLUMN_LOAD = 2;
const int INDEX_COLUMN_SHADER = 3;


class MenuInicial;

class SidebarApps : public wxPanel {
private:
	wxStaticText * title_label;
	wxButton *editor_btn;
	wxButton *config_btn;
	MenuInicial *m_parent;

	void OpenDefaultPanel(wxCommandEvent &event);
	void OpenEditorPanel(wxCommandEvent &event);
	void OpenConfigPanel(wxCommandEvent &event);

public:
	SidebarApps(wxPanel *parent);

	void setMenuInicial(MenuInicial *parent);

};

class DefaultAboutPanel : public wxPanel {
private:
	MenuInicial * m_parent;
public:
	DefaultAboutPanel(wxPanel *parent);
	void setMenuInicial(MenuInicial *parent);
};

class EditorAboutPanel : public wxPanel {
private:
	MenuInicial *m_parent;
	wxListCtrl *list_control;

	wxSpinCtrl *w_width;
	wxSpinCtrl *w_height;
	wxCheckBox *w_fullscreen;

	wxButton *buttonRemove;
	wxButton *buttonEdit;

	std::vector<dataImport> dataImported;

	void OpenEditor(wxCommandEvent &event);
	void OnRemoveItem(wxCommandEvent &event);
	void OnEditItem(wxCommandEvent &event);
	void OpenImportDialog(wxCommandEvent &event);
	void OnClearItem(wxCommandEvent &event);

	void setItemList(long index, dataImport data);
	void NewItem();
	void EditSelectedItem();
	void ClearItens();
	wxString *filter(dataImport data, bool notDiscard);

	void OnListItemSelected(wxListEvent& event);
	void OnListItemDeselected(wxListEvent &event);
	void OnActiveItem(wxListEvent &event);
public:
	EditorAboutPanel(wxPanel *parent);

	void ImportNewItem(dataImport data);
	void setMenuInicial(MenuInicial *parent);

};

class ConfigAboutPanel : public wxPanel {
private:
	MenuInicial * m_parent;
public:
	ConfigAboutPanel(wxPanel *parent);
	void setMenuInicial(MenuInicial *parent);
};

class MenuInicial : public wxFrame,Trigger {
private:
	int atualPanelOpened_id;
	void OnQuit(wxCommandEvent &event);
	void OnClose(wxCloseEvent& event);
public:
	MenuInicial(const wxString &title);
	void switchPanel(int panel);
	void trigger();

	wxMenuBar *menubar;
	wxMenu *file;
	wxBoxSizer *hbox;

	DefaultAboutPanel *defaultAboutPanel;
	EditorAboutPanel *editorAboutPanel;
	ConfigAboutPanel *configAboutPanel;

	SidebarApps *appspanel;
	wxPanel *m_parent;
};

bool initEditorMenu(MenuInicial* m, std::vector<dataImport> data);
bool initEditor(MenuInicial* m, std::vector<dataImport> data, int width, int height, bool fullscreen);
static EditorManagerWindows *manager;