#pragma once

#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/stattext.h>
#include <wx/filepicker.h>
#include <wx/filedlg.h>
#include <wx/spinctrl.h>
#include <wx/string.h>
#include <wx/utils.h>
#include <wx/textctrl.h>
#include <wx/choice.h>
#include <wx/notebook.h>
#include "DataType.h"
#include <wx/treectrl.h>

const int ID_CANCEL = 0;
const int ID_OK = 1;
const int ID_FILE = 2;
const int ID_INSTTREE = 3;
const int ID_ADDINST = 4;
const int ID_REMINST = 5;
const int ID_INSTSPIN = 6;

class ImportDialog : public wxDialog{

private:
	std::vector<wxTreeItemId> listItem;
	std::vector<instDataImport*> listInst;

	wxNotebook *tabs;

	wxSpinCtrlDouble *translate_x;
	wxSpinCtrlDouble *translate_y;
	wxSpinCtrlDouble *translate_z;

	wxSpinCtrlDouble *scale_x;
	wxSpinCtrlDouble *scale_y;
	wxSpinCtrlDouble *scale_z;

	wxSpinCtrlDouble *rotate_x;
	wxSpinCtrlDouble *rotate_y;
	wxSpinCtrlDouble *rotate_z;

	wxChoice *instChoice;
	wxTreeCtrl *InstTree;

	wxCheckBox *gravity;
	wxChoice *bounding_box;

	wxFilePickerCtrl *filepicker;
	wxButton *okButton;

	wxFilePickerCtrl *filepicker_boundingbox_physics;
	wxSpinCtrlDouble *mass_physics;

	wxButton *button_novo;
	wxButton *button_rem;

	void OnConfirm(wxCommandEvent &event);
	void OnCancel(wxCommandEvent &event);
	void OnFilePickerChanged(wxFileDirPickerEvent &event);
	void OnClose(wxCloseEvent& event);
	void OnAddInst(wxCommandEvent &event);
	void OnRemoveInst(wxCommandEvent &event);
	void clickInst(wxTreeEvent& event);
	void choiceInst(wxCommandEvent& event);

	void EnableMatriz();
	
	instDataImport *atualInst;
	int countInst;
	int indexOf(instDataImport *inst);
	int indexOf(wxTreeItemId item);
	instDataImport *getInst(wxTreeItemId item);
	wxTreeItemId getItem(instDataImport *inst);
	void saveInst();
	void novaInst();
	void removeInst();
	void selectInst(instDataImport *inst);
	void selectInst(int pos);

public:

	ImportDialog(const wxString &title);

	void initPanelTransform(wxNotebook* parent);
	void initPanelFisica(wxNotebook* parent);

	bool confirmAction;

	void setData(dataImport);
	dataImport getData();

};

